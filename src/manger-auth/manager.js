import { GoogleSignin } from '@react-native-community/google-signin';

const currentProvider = 'google';

export default class ManagerAuth {
    static getToken = async () => {
        if (currentProvider == 'google') {
            const idToken = (await GoogleSignin.signInSilently()).idToken
            return idToken.toString()
        }
        return null;
    };

    static logout = async (props) => {
        if (currentProvider == 'google') {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            console.log('User signed out!')
        }
        props.navigation.navigate('Splash');
    }
}

