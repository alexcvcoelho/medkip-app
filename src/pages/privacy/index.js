import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';

import IconBack from "~/assets/images/arrow-back.svg";


export default class Privacty extends Component {
    state = {

    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={this.onPressBack}>
                        <IconBack width="32" height="32"></IconBack>
                    </TouchableOpacity>
                    <Text style={[styles.text_title]}>Política de privacidade</Text>
                    <View style={{ width: 30, height: 30 }}></View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
});