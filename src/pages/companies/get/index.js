import React, { Component } from 'react';

import IconBack from "~/assets/images/arrow-back.svg";
import { View, StyleSheet, TouchableOpacity, Text, ScrollView, ActivityIndicator } from 'react-native';

import IconEmpty from "~/assets/images/no_companies.svg";
import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';
import { Card, Button, Icon } from 'react-native-elements';

import crashlytics from '@react-native-firebase/crashlytics';


import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import IconMenu from "~/assets/images/icon-ellipsis-v.svg";
import IconTrash from "~/assets/images/icon-trash.svg";

import { theme, variables, hexToRGB } from '~/theme';
import { BottomSheetFailure, SimpleBottomSheet } from '~/components/bottomSheets';
import IconPathSuccess from "~/assets/images/icon-path-profile.svg";


export default class CompanyList extends Component {
    state = {
        isLoading: false,
        token: '',
        /*companies: [
            { "id": 1, "name": "Empresa Itu", "cnpj": "123123132132" },
            { "id": 2, "name": "Empresa Sorocaba", "cnpj": "41241231313213" },
            { "id": 3, "name": "Empresa Campinas", "cnpj": "4124141315612" }],*/
        companies: [],
        showModalDelete: false,
        modalSuccess: false,
        itemToDelete: null,
        modalInfo: false,
        messageInfo: '',
    }

    onPressDelete = () => {
        this.setState({ showModalDelete: false, isLoading: true })

        // chamada de api para apagar registro
        if (this.state.itemToDelete != null) {
            ServiceApi.deleteCompany(this.state.token, this.state.itemToDelete.id, () => {
                this.refreshlist()
                this.setState({ isLoading: false, modalSuccess: true })
            }, (error) => {
                this.setState({
                    isLoading: false, modalInfo: true, messageInfo: error, itemToDelete: null
                })
            }, this.props);
        }
    }

    refreshlist = () => {
        if (this.state.itemToDelete != null) {
            const filteredData = this.state.companies.filter(item => item.id !== this.state.itemToDelete.id);
            this.setState({ companies: filteredData, itemToDelete: null, modalSuccess: false });
        }
    }

    onPressOptionDelete = (item) => {
        this.setState({ showModalDelete: true, itemToDelete: item })
    }

    onPressCancelDelete = () => {
        this.setState({ showModalDelete: false, itemToDelete: null })
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }

    getCompanies = () => {
        ServiceApi.getCompanies(this.state.token, (response) => {
            this.setState({ companies: response, isLoading: false })
        }, (error) => {
            this.setState({
                isLoading: false, modalInfo: true, messageInfo: error
            })
        }, this.props);
    }

    getToken = async () => {
        this.setState({ isLoading: true })

        const idToken = await ManagerAuth.getToken();

        this.setState({ token: idToken.toString() })

        this.getCompanies();
    }

    formatCNPJ = (cnpj) => {
        cnpj = cnpj.replace(/^(\d{2})(\d)/, "$1.$2")

        //Coloca ponto entre o quinto e o sexto dígitos
        cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3")

        //Coloca uma barra entre o oitavo e o nono dígitos
        cnpj = cnpj.replace(/\.(\d{3})(\d)/, ".$1/$2")

        //Coloca um hífen depois do bloco de quatro dígitos
        cnpj = cnpj.replace(/(\d{4})(\d)/, "$1-$2")

        return cnpj
    }

    componentDidMount() {
        crashlytics().log('Updating user count.');

        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    render() {
        return (
            <MenuProvider>
                <View style={styles.container}>
                    <View style={styles.navigation_bar}>
                        <TouchableOpacity onPress={this.onPressBack}>
                            <IconBack width="32" height="32"></IconBack>
                        </TouchableOpacity>
                        <Text style={[styles.text_title]}>Empresa</Text>
                        <View style={{ width: 30, height: 30 }}>
                        </View>
                    </View>

                    {this.state.isLoading ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size="large" color="#4493FF" />
                        </View>

                        :
                        <View style={{ flex: 1 }}>
                            {this.state.companies.length == 0 ?
                                <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                    <IconEmpty width="59" height="41"></IconEmpty>
                                    <Text style={{ color: '#8189A1', width: 213, textAlign: 'center', marginTop: 20 }}>Você ainda não cadastrou nenhuma empresa</Text>

                                    <TouchableOpacity style={styles.button_continue} onPress={() => this.props.navigation.navigate('CompanySave', { company: null })}>
                                        <Text style={styles.text_style_bold}>Cadastrar</Text>
                                    </TouchableOpacity>
                                </View>
                                : <View style={{ flex: 1 }}>
                                    <ScrollView style={{ backgroundColor: '#E5E5E5', flex: 1 }}>
                                        <Text style={{ marginLeft: 16, marginTop: 16, color: '#364574', fontFamily: 'Montserrat-Bold', fontSize: 14 }}>Empresas Adicionadas</Text>
                                        {this.state.companies.map((item, index) => (
                                            <TouchableOpacity key={item.id} style={{ minHeight: 66, marginLeft: 20, marginRight: 20 }}>
                                                <Card containerStyle={{ borderWidth: 0, borderRadius: 6, elevation: 0, marginLeft: 0, marginRight: 0, paddingVertical: 0 }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>

                                                        <IconEmpty width="38" height="38"></IconEmpty>
                                                        <View style={{ flexDirection: 'column', marginLeft: 20 }}>
                                                            <Text style={{ color: '#364574', fontFamily: 'Montserrat-Medium', fontSize: 14, marginTop: 15 }}>{item.name}</Text>
                                                            <Text style={{ color: '#626B8A', fontFamily: 'Montserrat-Medium', fontSize: 13, marginTop: 4, marginBottom: 15 }}>{this.formatCNPJ(item.cnpj)}</Text>
                                                        </View>

                                                        <View style={{ alignItems: 'center', flexDirection: 'row', position: 'absolute', right: 0 }}>
                                                            <Menu onSelect={(func) => func()}>
                                                                <MenuTrigger>
                                                                    <View style={{ width: 30, height: 30, alignItems: 'flex-end', justifyContent: 'center' }}>
                                                                        <IconMenu />
                                                                    </View>
                                                                </MenuTrigger>
                                                                <MenuOptions optionsContainerStyle={{ marginTop: 30 }} customStyles={menu2}>
                                                                    <MenuOption value={() => this.onPressOptionDelete(item)}>
                                                                        <View style={styles.dropdown_item}>
                                                                            <Text style={styles.dropdown_item_txt}>Excluir</Text>
                                                                        </View>
                                                                    </MenuOption>
                                                                    <MenuOption value={() => this.props.navigation.navigate('CompanySave', { company: item })}>
                                                                        <View style={styles.dropdown_item}>
                                                                            <Text style={styles.dropdown_item_txt}>Editar</Text>
                                                                        </View>
                                                                    </MenuOption>
                                                                </MenuOptions>
                                                            </Menu>
                                                        </View>
                                                    </View>
                                                </Card>
                                            </TouchableOpacity>
                                        ))}
                                    </ScrollView>
                                    <View style={[theme.fab_button_container, { backgroundColor: 'transparent' }]}>
                                        <TouchableOpacity style={theme.fab_button} onPress={() => this.props.navigation.navigate('CompanySave', { company: null })}>
                                            <Icon name="plus" type="feather" color="#ffffff" />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            }
                        </View>
                    }
                    <SimpleBottomSheet visible={this.state.showModalDelete} onClose={() => this.onPressCancelDelete()}>
                        <View style={{ alignItems: 'center', marginHorizontal: variables.paddingHorizontal }}>
                            <IconTrash />
                            <Text style={{ fontSize: 16, color: variables.gray2, marginVertical: variables.paddingHorizontal, textAlign: 'center' }}>Você tem certeza que desenha exluir este local da sua lista?</Text>
                        </View>
                        <Button
                            title="Sim, excluir"
                            disabled={false}
                            buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                            titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                            disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                            disabledTitleStyle={{ color: variables.light }}
                            onPress={() => this.onPressDelete()}
                        />
                        <View style={{ height: variables.paddingHorizontal }} />
                        <Button
                            title="Não, voltar"
                            disabled={false}
                            buttonStyle={{ backgroundColor: variables.light, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight, borderWidth: 1 }}
                            titleStyle={{ color: variables.primary, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                            disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                            disabledTitleStyle={{ color: variables.light }}
                            onPress={() => this.onPressCancelDelete()}
                        />
                    </SimpleBottomSheet>


                    <SimpleBottomSheet
                        visible={this.state.modalSuccess}
                        onClose={() => this.setState({ modalSuccess: false })}
                    >
                        <View style={{ alignItems: 'center' }}>
                            <IconPathSuccess />
                            <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Empresa excluída com sucesso!'}</Text>
                        </View>
                        <Button
                            title="Ok"
                            disabled={false}
                            onPress={() => this.setState({ modalSuccess: false })}
                            buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                            titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                            disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                            disabledTitleStyle={{ color: variables.light }}
                        />
                    </SimpleBottomSheet>


                    <BottomSheetFailure
                        visible={this.state.modalInfo}
                        text={this.state.messageInfo}
                        onClose={() => { this.setState({ modalInfo: false }); }}
                    />
                </View>
            </MenuProvider>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
    button_continue: {
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: '#006CFF',
        height: 48,
        width: 223,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text_style_bold: {
        color: '#ffffff',
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold'
    },
    dropdown_item: {
        padding: variables.paddingHorizontal,
        backgroundColor: variables.light,
    },
    dropdown_item_txt: {
        fontSize: variables.fontSize,
        color: variables.secondary,
    },
});

const menu2 = {
    optionsContainer: {
        padding: 0,
    },
    optionWrapper: {
        margin: 0,
        padding: 0,
    },
}