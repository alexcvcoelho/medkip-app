import React, { Component } from 'react';

import IconBack from "~/assets/images/arrow-back.svg";
import IconInfo from "~/assets/images/info.svg";

import { View, StyleSheet, TouchableOpacity, Text, ScrollView, ActivityIndicator, Dimensions, TextInput } from 'react-native';
import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';

import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import { Icon } from 'react-native-elements';

import { variables, hexToRGB } from '~/theme';
import FloatingInput from '~/components/floatingInput';
import { BottomSheetFailure, SimpleBottomSheet, SimpleModal } from '~/components/bottomSheets';
import IconPathSuccess from "~/assets/images/icon-path-profile.svg";

import { Button } from 'react-native-elements';

export default class CompanySave extends Component {
    state = {
        token: '',
        isLoading: false,
        nameFocus: false,
        cnpjFocus: false,
        id: null,
        textFieldName: '',
        textFieldCNPJ: '',
        selected_partner: '',
        selected_tax: '',
        aliquotValue: 0,

        modalSuccess: false,
        modalInfo: false,
        messageInfo: '',
        modalVisible: false,
        itemEdit: null,
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }

    fillForm = () => {
        if (this.state.itemEdit) {
            console.log('dados')

            console.log(this.state.itemEdit)
            const {
                aliquot,
                cnpj,
                id,
                name,
                tax,
                members,
            } = this.state.itemEdit;

            this.setState({
                id: id,
                textFieldName: name,
                textFieldCNPJ: cnpj,
                aliquotValue: aliquot.toString(),
                selected_tax: tax,
                selected_partner: members
            });
        }
    }

    validarCNPJ = (cnpj) => {

        cnpj = cnpj.replace(/[^\d]+/g, '');

        if (cnpj == '') return false;

        if (cnpj.length != 14)
            return false;

        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" ||
            cnpj == "11111111111111" ||
            cnpj == "22222222222222" ||
            cnpj == "33333333333333" ||
            cnpj == "44444444444444" ||
            cnpj == "55555555555555" ||
            cnpj == "66666666666666" ||
            cnpj == "77777777777777" ||
            cnpj == "88888888888888" ||
            cnpj == "99999999999999")
            return false;

        // Valida DVs
        var tamanho = cnpj.length - 2
        var numeros = cnpj.substring(0, tamanho);
        const digitos = cnpj.substring(tamanho);
        var soma = 0;
        var pos = tamanho - 7;
        for (var i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;

        return true;
    }

    validateFields = () => {
        if (this.state.textFieldName.length == 0) {
            this.setState({ messageInfo: 'Campo nome da empresa é obrigatório!', modalInfo: true })
            return false
        } else if (!this.validarCNPJ(this.state.textFieldCNPJ)) {
            this.setState({ messageInfo: 'Campo CNPJ inválido!', modalInfo: true })
            return false
        } else if (this.state.selected_partner == '') {
            this.setState({ messageInfo: 'Selecione a quantidade de sócios.', modalInfo: true })
            return false
        } else if (this.state.selected_tax == '') {
            this.setState({ messageInfo: 'Selecione um regime de imposto.', modalInfo: true })
            return false
        }

        return true
    }

    printSamples = () => {
        //console.log(this.state.selected_partner)
        //console.log(this.state.selected_tax)
        //console.log(this.state.aliquotValue)
        //console.log(this.state.id)
    }

    handleInputAliquot = (text) => {
        if (/[+-]?([0-9]*[.])?[0-9]+/.test(text)) {
            this.setState({
                aliquotValue: text
            });
        }
    }
    onPressSave = () => {
        const result = this.validateFields()
        if (!result) {
            return
        }

        this.setState({ isLoading: true })

        this.printSamples()

        ServiceApi.saveCompany(this.state.token,
            this.state.id,
            this.state.textFieldName.toString(),
            this.state.textFieldCNPJ.replace(/[^\d]+/g, '').toString(),
            this.state.selected_partner.toString(),
            this.state.selected_tax.toString(),
            parseFloat(this.state.aliquotValue.toString()), () => {
                this.setState({ isLoading: false, modalSuccess: true })
            }, (error) => {
                this.setState({
                    isLoading: false, modalInfo: true, messageInfo: error
                })
            }, this.props);
    }

    getToken = async () => {
        this.setState({ isLoading: true })

        const { route } = this.props;
        const itemEdit = route.params.company
        if (itemEdit) {
            console.log(itemEdit.id);
        }
        const idToken = await ManagerAuth.getToken();

        this.setState({ token: idToken.toString(), isLoading: false, itemEdit: itemEdit })

        this.fillForm()
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    render() {
        return (
            <MenuProvider>
                <View style={styles.container}>
                    <View style={styles.navigation_bar}>
                        <TouchableOpacity onPress={this.onPressBack}>
                            <IconBack width="32" height="32"></IconBack>
                        </TouchableOpacity>
                        <Text style={[styles.text_title]}>Cadastrar Empresa</Text>
                        <View style={{ width: 30, height: 30 }}>
                        </View>
                    </View>

                    <ScrollView style={styles.sub_container}>

                        <View style={{ marginTop: 22 }}>
                            <FloatingInput
                                label="Nome da Empresa"
                                isMask={false}
                                value={this.state.textFieldName}
                                onChangeText={(text) => this.setState({ textFieldName: text })}
                            />
                        </View>

                        <FloatingInput
                            label="CNPJ"
                            isMask={true}
                            maskType={'cnpj'}
                            value={this.state.textFieldCNPJ}
                            onChangeText={(text) => this.setState({ textFieldCNPJ: text })}
                        />

                        <Menu onSelect={value => this.setState({ selected_partner: value })}>
                            <MenuTrigger>
                                <View style={styles.textfield}>
                                    <TextInput
                                        placeholder="Quantidade de Sócios"
                                        placeholderTextColor={variables.gray3}
                                        editable={false}
                                        value={this.state.selected_partner}
                                        style={{ color: variables.secondary, height: variables.inputHeight }}
                                    />
                                    <Icon name="chevron-down" type="feather" color={variables.accent} />
                                </View>
                            </MenuTrigger>
                            <MenuOptions optionsContainerStyle={{ marginTop: 50 }} customStyles={menu}>
                                <MenuOption value="Até 5">
                                    <View style={styles.dropdown_item}>
                                        <Text style={styles.dropdown_item_txt}>Até 5</Text>
                                    </View>
                                </MenuOption>
                                <MenuOption value="5 a 10">
                                    <View style={styles.dropdown_item}>
                                        <Text style={styles.dropdown_item_txt}>5 a 10</Text>
                                    </View>
                                </MenuOption>
                                <MenuOption value="10 a 20">
                                    <View style={styles.dropdown_item}>
                                        <Text style={styles.dropdown_item_txt}>10 a 20</Text>
                                    </View>
                                </MenuOption>
                                <MenuOption value="+ 20">
                                    <View style={styles.dropdown_item}>
                                        <Text style={styles.dropdown_item_txt}>+ 20</Text>
                                    </View>
                                </MenuOption>
                            </MenuOptions>
                        </Menu>

                        <TouchableOpacity style={{ marginTop: 22, flexDirection: 'row' }}>
                            <Text style={{ color: '#364574', fontFamily: 'Montserrat-SemiBold', fontSize: 14, marginRight: 10 }}>
                                Regime de Imposto</Text>
                            <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                                <IconInfo width="20" height="20"></IconInfo>
                            </TouchableOpacity>
                        </TouchableOpacity>


                        <View style={{ marginTop: 22 }}>
                            <Menu onSelect={value => this.setState({ selected_tax: value, aliquotValue: value == 'Simples Nacional' ? "12" : "14.33", })}>
                                <MenuTrigger>
                                    <View style={styles.textfield}>
                                        <TextInput
                                            placeholder="Selecione o Regime de Imposto"
                                            placeholderTextColor={variables.gray3}
                                            editable={false}
                                            value={this.state.selected_tax}
                                            style={{ color: variables.secondary, height: variables.inputHeight }}
                                        />
                                        <Icon name="chevron-down" type="feather" color={variables.accent} />
                                    </View>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={{ marginTop: 50 }} customStyles={menu}>
                                    <MenuOption value="Simples Nacional">
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>Simples Nacional</Text>
                                        </View>
                                    </MenuOption>
                                    <MenuOption value="Lucro Presumido">
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>Lucro Presumido</Text>
                                        </View>
                                    </MenuOption>

                                </MenuOptions>
                            </Menu>
                        </View>

                        {this.state.selected_tax ? <FloatingInput
                            label="ISS"
                            editable={true}
                            isMask={false}
                            keyboardType='numeric'
                            value={this.state.aliquotValue}
                            onChangeText={(text) => this.handleInputAliquot(text)}
                        /> : null}

                        <TouchableOpacity style={styles.button_continue} onPress={this.onPressSave}>
                            {this.state.isLoading ?
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator size="large" color="#4493FF" />
                                </View>
                                :
                                <Text style={styles.text_style_bold}>Salvar</Text>
                            }
                        </TouchableOpacity>
                        <View style={styles.margin_end}></View>
                    </ScrollView>
                </View>

                <SimpleBottomSheet
                    visible={this.state.modalSuccess}
                    onClose={() => this.setState({ modalSuccess: false })}
                >
                    <View style={{ alignItems: 'center' }}>
                        <IconPathSuccess />
                        <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Empresa salva com sucesso!'}</Text>
                    </View>
                    <Button
                        title="Ver Empresa"
                        disabled={false}
                        onPress={() => this.onPressBack()}
                        buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                        titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                    />

                    <View style={{ height: variables.paddingHorizontal }} />
                </SimpleBottomSheet>


                <BottomSheetFailure
                    visible={this.state.modalInfo}
                    text={this.state.messageInfo}
                    onClose={() => { this.setState({ modalInfo: false }); }}
                />

                <SimpleModal
                    visible={this.state.modalVisible}
                    onClose={() => this.setState({ modalVisible: false })}>
                    <Text style={{ fontFamily: 'Montserrat-Bold', color: '#364574', fontSize: 14 }}>{`Impostos pagos por\nnota fiscal`}</Text>
                    <Text style={{ fontFamily: 'Montserrat-Regular', color: '#626B8A', fontSize: 14, marginTop: 24 }}>{`Imposto Municipal:\nISS de 2%`}</Text>
                    <Text style={{ fontFamily: 'Montserrat-Regular', color: '#626B8A', fontSize: 14, marginTop: 12 }}>{`Impostos Federais:\nPIS (0,65%)\nCOFINS (3%)\nIRPJ (1,5%)\nCSLL (1%)`}</Text>
                </SimpleModal>
            </MenuProvider>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    sub_container: {
        marginLeft: 16,
        marginRight: 16
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Bold',
        justifyContent: 'center'
    },
    view_image: {
        alignItems: 'center',
        marginTop: 24
    },
    button_continue: {
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: '#006CFF',
        height: 48,
        width: '100%',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text_style_bold: {
        color: '#ffffff',
        fontSize: 14,
        marginLeft: 24,
        fontFamily: 'Montserrat-SemiBold'
    },
    margin_end: {
        marginTop: 40
    },
    dropdown_item: {
        padding: variables.paddingHorizontal,
        backgroundColor: variables.light,
    },
    dropdown_item_txt: {
        fontSize: variables.fontSize,
        color: variables.secondary,
    },
    textfield: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: variables.paddingHorizontal,
        borderRadius: variables.borderRadius,
        backgroundColor: variables.light,
        borderWidth: 1,
        borderColor: '#C0C4D0',
        marginBottom: variables.paddingHorizontal,
        position: 'relative',
    },
});

const windowWidth = Dimensions.get('window').width - variables.paddingHorizontal * 2;

const menu = {
    optionsContainer: {
        padding: 0,
        width: windowWidth,
    },
    optionWrapper: {
        margin: 0,
        padding: 0,
    },
}