import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, ActivityIndicator, Text } from 'react-native';

import { Button } from 'react-native-elements';

import { RNCamera } from 'react-native-camera';
import ServiceApi from '~/api/api-caller';


import IconPicture from "~/assets/images/camera-picture.svg";
import IconSwitchCamera from "~/assets/images/switch-camera.svg";
import { BottomSheetFailure, SimpleBottomSheet } from '~/components/bottomSheets';

import ManagerAuth from '~/manger-auth/manager';
import IconPathSuccess from "~/assets/images/icon-path-profile.svg";
import { variables, hexToRGB } from '~/theme';

export default class UploadImage extends Component {
    state = {
        token: '',
        isFront: true,
        isLoading: false,
        modalSuccess: false,
        modalInfo: false,
        messageInfo: '',
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    onCloseSuccess = () => {
        this.setState({ modalSuccess: false });
        this.onPressBack();
    }

    getToken = async () => {
        this.setState({ isLoading: true })
        const idToken = await ManagerAuth.getToken();
        this.setState({ token: idToken.toString(), isLoading: false })
    }

    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true, width: 300, fixOrientation: true };
            const data = await this.camera.takePictureAsync(options)
            //alert(data.base64);

            this.setState({ isLoading: true })

            ServiceApi.uploadAvatar(this.state.token, data, () => {
                // apresenta modal de sucesso
                this.setState({ modalSuccess: true, isLoading: false })
            }, (error) => {
                // tratar o erro ou disparar erro default
                this.setState({
                    isLoading: false, modalInfo: true, messageInfo: error
                })
            }, this.props);
        }
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }


    render() {
        return (
            <View style={styles.container}>
                {this.state.isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#4493FF" />
                    </View>
                    :
                    <View style={styles.container}>
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            style={styles.preview}
                            type={this.state.isFront ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back}
                            flashMode={RNCamera.Constants.FlashMode.on}
                            androidCameraPermissionOptions={{
                                title: 'Permission to use camera',
                                message: 'We need your permission to use your camera',
                                buttonPositive: 'Ok',
                                buttonNegative: 'Cancel',
                            }}
                            androidRecordAudioPermissionOptions={{
                                title: 'Permission to use audio recording',
                                message: 'We need your permission to use your audio',
                                buttonPositive: 'Ok',
                                buttonNegative: 'Cancel',
                            }}
                            onGoogleVisionBarcodesDetected={({ barcodes }) => {
                                console.log(barcodes);
                            }}
                        />
                        <View style={{ position: 'absolute', bottom: 30, width: '100%' }}>
                            <TouchableOpacity onPress={this.takePicture.bind(this)} style={{ alignItems: 'center', width: 80, height: 80, alignSelf: 'center' }}>
                                <IconPicture width="80" height="80"></IconPicture>
                            </TouchableOpacity>
                        </View>

                        <View style={{ position: 'absolute', bottom: 30, right: 15 }}>
                            <TouchableOpacity onPress={() => { this.setState({ isFront: this.state.isFront ? false : true }) }} style={{ alignItems: 'center' }}>
                                <IconSwitchCamera width="24" height="24"></IconSwitchCamera>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
                <SimpleBottomSheet
                    visible={this.state.modalSuccess}
                    onClose={() => this.setState({ modalSuccess: false })}
                >
                    <View style={{ alignItems: 'center' }}>
                        <IconPathSuccess />
                        <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Foto alterada com sucesso!'}</Text>
                    </View>
                    <Button
                        title="Ver Perfil"
                        disabled={false}
                        onPress={() => this.onCloseSuccess()}
                        buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                        titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                    />

                    <View style={{ height: variables.paddingHorizontal }} />
                </SimpleBottomSheet>


                <BottomSheetFailure
                    visible={this.state.modalInfo}
                    text={this.state.messageInfo}
                    onClose={() => { this.setState({ modalInfo: false }); }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

});