import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, ScrollView } from 'react-native';

import { Button, Icon } from 'react-native-elements';

import moment from 'moment';
import 'moment/locale/pt-br';

import { theme, variables, hexToRGB } from '~/theme';
import { SimpleModal } from '~/components/bottomSheets';

import IconBack from "~/assets/images/arrow-back.svg";
import IconBell from "~/assets/images/icon-bell.svg";
import IconCalendar from "~/assets/images/icon-calendar-danger.svg";


export default class Notifications extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMessage: false,
            selectedMessage: [],
            messages: [{
                type: 'shift',
                date: new Date(),
                title: 'O seu próximo plantão',
                description: 'O seu próximo plantão começará em menos de 1h. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porttitor nibh id sapien consectetur fringilla. Cras lacinia ipsum nec arcu tincidunt, id elementum massa suscipit. Integer scelerisque lobortis sapien, sit amet laoreet enim aliquam ac',
                list: ['Etiam porttitor nibh id sapien.', 'Mauris porta elit sit amet iaculis.', 'Vestibulum arcu arcu, imperdiet.'],
                read: false,
            },
            {
                type: 'default',
                date: moment(new Date()).subtract(1, 'days'),
                title: 'Lembrete de autorização',
                description: 'Não se esqueça de pegar a autorização com o seu supervisor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porttitor nibh id sapien consectetur fringilla. Cras lacinia ipsum nec arcu tincidunt, id elementum massa suscipit. Integer scelerisque lobortis sapien, sit amet laoreet enim aliquam ac',
                list: [],
                read: false,
            },
            {
                type: 'default',
                date: moment(new Date()).subtract(1, 'month'),
                title: 'Imposto de renda',
                description: 'Faltam 3 semanas para a declaração do importo de renda. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porttitor nibh id sapien consectetur fringilla. Cras lacinia ipsum nec arcu tincidunt, id elementum massa suscipit. Integer scelerisque lobortis sapien, sit amet laoreet enim aliquam ac',
                list: [],
                read: true,
            }]
        }

        moment.locale('pt-BR');

        var meses = 'Janeiro_Fevereiro_Março_Abril_Maio_Junho_Julho_Agosto_Setembro_Outubro_Novembro_Dezembro'.split('_');
        var semanas = 'Domingo_Segunda-feira_Terça-feira_Quarta-feira_Quinta-feira_Sexta-feira_Sábado'.split('_');
        var monthsShort = 'jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez'.split('_');

        moment.updateLocale('pt-BR', { months: meses, weekdays: semanas, monthsShort });
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }

    readMessage = (item, index) => {
        this.setState({ selectedMessage: item, showMessage: true });
        this.changeValue('read', true, index);
    }

    changeValue = (key, value, index) => {
        const data = [...this.state.messages];
        data[index][key] = value;
        this.setState({ messages: data });
    };

    closeMessage = () => {
        this.setState({ selectedMessage: [], showMessage: false })
    }

    render() {
        return (
            <View style={[theme.container, { backgroundColor: '#F5F6FA' }]}>
                <View style={theme.navigation_bar}>
                    <TouchableOpacity onPress={this.onPressBack}>
                        <IconBack width="32" height="32"></IconBack>
                    </TouchableOpacity>
                    <Text style={[theme.text_title]}>Mensagens</Text>
                    <View style={{ width: 30, height: 30 }}></View>
                </View>
                <ScrollView>
                    <View style={styles.page}>
                        <Text style={theme.title}>Novas Notificações</Text>

                        {/* {this.state.messages.map((item, index) => (
                            <TouchableOpacity key={index} onPress={() => this.readMessage(item, index)} style={styles.card}>
                                <View style={styles.row}>
                                    <View style={styles.card_col}>
                                        <View style={[styles.icon, item.type == 'shift' && styles.icon_danger]}>
                                            {item.type == 'shift' && <IconCalendar />}
                                            {item.type == 'default' && <IconBell />}
                                        </View>
                                        <Text style={[styles.text, !item.read && styles.bold]} numberOfLines={2}>{item.description}</Text>
                                    </View>
                                    <Text style={styles.date}>{moment(item.date).format('DD MMM')}</Text>
                                </View>
                            </TouchableOpacity>
                        ))} */}

                        {/* {Array(3).fill(null).map(() => (
                            <TouchableOpacity style={styles.card}>
                                <View style={styles.row}>
                                    <View style={styles.card_col}>
                                        <View style={[styles.icon]}>
                                            <IconBell />
                                        </View>
                                        <Text style={styles.text} numberOfLines={2}>Próximo plantão inicia em 3h</Text>
                                    </View>
                                    <Text style={styles.date}>23 set</Text>
                                </View>
                            </TouchableOpacity>
                        ))} */}

                    </View>
                </ScrollView>
                <SimpleModal visible={this.state.showMessage}>
                    <View style={[styles.row, { paddingHorizontal: 0, alignItems: 'flex-start' }]}>
                        <View style={[styles.card_col, { flex: 0.95 }]}>
                            <View style={[styles.icon, this.state.selectedMessage.type == 'shift' && styles.icon_danger]}>
                                {this.state.selectedMessage.type == 'shift' && <IconCalendar />}
                                {this.state.selectedMessage.type == 'default' && <IconBell />}
                            </View>
                            <Text style={[styles.text]} numberOfLines={2}>{moment(this.state.selectedMessage.date).format(`dddd, DD [de] MMMM [\n]HH:ss`)}</Text>
                        </View>
                        <View style={{ right: -10 }}>
                            <Icon name="close" color={variables.accent} size={24} onPress={() => this.closeMessage()} />
                        </View>
                    </View>
                    <View style={{ paddingBottom: variables.paddingHorizontal }}>
                        <Text style={theme.title}>{this.state.selectedMessage.title}</Text>
                        <Text style={styles.paragraph}>{this.state.selectedMessage.description}</Text>
                        {this.state.selectedMessage.list && this.state.selectedMessage.list.map((text) => (
                            <View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ backgroundColor: '#22CFEE', width: 6, height: 6, borderRadius: 3, marginRight: 19 }} />
                                    <Text style={styles.paragraph}>{text}</Text>
                                </View>
                            </View>
                        ))}
                        {this.state.selectedMessage.type == 'shift' && <>
                            <View style={{ height: variables.paddingHorizontal }} />
                            <Button
                                title="Ver Plantão"
                                disabled={false}
                                buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                                titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                disabledTitleStyle={{ color: variables.light }}
                                onPress={() => { this.closeMessage(); this.props.navigation.navigate("Shift") }}
                            />
                        </>}
                    </View>
                </SimpleModal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    //meus estilos
    page: {
        padding: variables.paddingHorizontal,
        flex: 1,
    },
    spacing: {
        padding: variables.paddingHorizontal,
    },
    card: {
        backgroundColor: variables.light,
        borderRadius: variables.borderRadius,
        // padding: variables.paddingHorizontal,
        marginVertical: variables.paddingHorizontal / 2,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'nowrap',
        padding: variables.paddingHorizontal,
    },
    icon: {
        width: 43,
        height: 43,
        borderRadius: 43 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: hexToRGB(variables.accent, 0.1),
        marginRight: variables.paddingHorizontal,
    },
    icon_danger: {
        backgroundColor: hexToRGB(variables.danger, 0.1),
    },
    card_col: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 0.8,
        paddingRight: variables.paddingHorizontal,
    },
    text: {
        color: variables.gray2,
        fontSize: variables.fontSize,
    },
    bold: {
        fontWeight: 'bold',
    },
    date: {
        color: variables.gray2,
        flex: 0.4,
        textAlign: 'right',
        alignSelf: 'flex-start',
        fontSize: 12,
        textTransform: 'uppercase',
    },
    paragraph: {
        fontSize: variables.fontSize,
        color: variables.gray2,
        marginVertical: 8,
    },
})