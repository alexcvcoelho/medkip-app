import React, { Component } from 'react';

import IconBack from "~/assets/images/arrow-back.svg";
import { View, StyleSheet, TouchableOpacity, Text, Switch } from 'react-native';


export default class AlertConfigurations extends Component {
    state = {
        isEnabledNotifications: false,
        isEnableAlerts: false
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }

    onValueNotificationsChange = (value) => {
        this.setState({ isEnabledNotifications: value })
    }


    onValueAlertChange = (value) => {
        this.setState({ isEnableAlerts: value })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={this.onPressBack}>
                        <IconBack width="32" height="32"></IconBack>
                    </TouchableOpacity>
                    <Text style={[styles.text_title]}>Configurações</Text>
                    <View style={{ width: 30, height: 30 }}>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 50, marginLeft: 16, marginRight: 16 }}>
                    <View style={{ flexDirection: 'column', width: '90%' }}>
                        <Text style={styles.text_sub_title}>Receber notificações</Text>
                        <Text style={styles.text_sub_description}>Ao ativar essa função, você estará permitindo que enviemos notificações</Text>
                    </View>
                    <Switch
                        trackColor={{ false: "#d3d3d3", true: "#006CFF" }}
                        thumbColor={this.state.isEnabledNotifications ? "#FFFFFF" : "#C0C3D0"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={this.onValueNotificationsChange}
                        value={this.state.isEnabledNotifications}
                    />
                </View>


                <View style={{ flexDirection: 'row', marginTop: 30, marginLeft: 16, marginRight: 16 }}>
                    <View style={{ flexDirection: 'column', width: '90%' }}>
                        <Text style={styles.text_sub_title}>Receber alerta de plantões</Text>
                        <Text style={styles.text_sub_description}>Ao ativar essa função, você estará permitindo que enviemos alertas de plantões.</Text>
                    </View>
                    <Switch
                        trackColor={{ false: "#d3d3d3", true: "#006CFF" }}
                        thumbColor={this.state.isEnableAlerts ? "#FFFFFF" : "#C0C3D0"}
                        ios_backgroundColor="#FFFFFF"
                        onValueChange={this.onValueAlertChange}
                        value={this.state.isEnableAlerts}
                    />
                </View >
            </View >
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
    switch_style: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center',
    },
    text_sub_title: {
        color: '#202945', fontSize: 16, fontFamily: 'Montserrat-Regular'
    },
    text_sub_description: {
        color: '#626B8A', fontSize: 14, fontFamily: 'Montserrat-Regular', marginTop: 2
    },
});