import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import BottomTabNavigator from "~/pages/tab";
import { AlertConfigStackNavigator, PrivacyStackNavigator, HelpStackNavigator, FilesStackNavigator } from "~/pages/stack-navigation";

import Logout from '~/pages/logout';

//import Sidebar from '~/components/customdrawer';

import IconProfile from "~/assets/images/profile-active.svg";
import IconConfig from "~/assets/images/icon-config.svg";
import IconHelp from "~/assets/images/icon-help.svg";
import IconPrivacy from "~/assets/images/icon-privacy.svg";
import IconFiles from "~/assets/images/icon-files.svg";
import IconLogout from "~/assets/images/icon-logout.svg";

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
    return (
        <>
            <Drawer.Navigator
                drawerStyle={{ width: '90%' }}>
                <Drawer.Screen name="Profile" component={BottomTabNavigator}
                    options={{
                        title: 'Conta',
                        drawerIcon: ({ focused, size }) => (
                            <IconProfile width="24" height="24"></IconProfile>
                        ),
                    }} />
                <Drawer.Screen name="AlertConfigurations" component={AlertConfigStackNavigator}
                    options={{
                        title: 'Configuração da conta',
                        drawerIcon: ({ focused, size }) => (
                            <IconHelp width="24" height="24"></IconHelp>
                        ),
                    }}
                />
                <Drawer.Screen name="Help" component={HelpStackNavigator}
                    options={{
                        title: 'Ajuda',
                        drawerIcon: ({ focused, size }) => (
                            <IconConfig width="24" height="24"></IconConfig>

                        ),
                    }}
                />
                <Drawer.Screen name="Privacy" component={PrivacyStackNavigator}
                    options={{
                        title: 'Política de privacidade',
                        drawerIcon: ({ focused, size }) => (
                            <IconPrivacy width="24" height="24"></IconPrivacy>

                        ),
                    }}
                />
                <Drawer.Screen name="Files" component={FilesStackNavigator}
                    options={{
                        title: 'Arquivos',
                        drawerIcon: ({ focused, size }) => (
                            <IconFiles width="24" height="24"></IconFiles>

                        ),
                    }}
                />
                <Drawer.Screen name="Logout" component={Logout}
                    options={{
                        title: 'Logout',
                        drawerIcon: ({ focused, size }) => (
                            <IconLogout width="24" height="24"></IconLogout>
                        ),
                    }}
                />
            </Drawer.Navigator>
        </>
    );
}

export default DrawerNavigator;
