import React, { Component } from 'react';
import { View, StyleSheet, Animated, BackHandler } from 'react-native';

import Logo from "~/assets/images/splash.svg";

import { GoogleSignin } from '@react-native-community/google-signin';

export default class Splash extends Component {
    state = {
        backgroundColor: "#ffffff",
        animation: new Animated.Value(1)
    }

    /*componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }*/

    goToOnboarding = async () => {
        const isSignedIn = await GoogleSignin.isSignedIn();
        //const user = await GoogleSignin.getCurrentUser();
        //console.log('user: ' + user.toString());
        //console.log('isSignedIn: ' + isSignedIn);

        if (isSignedIn) {
            this.props.navigation.navigate('DrawerNavigator');
        } else {
            this.props.navigation.navigate('Onboarding');
        }
    }

    fadeOut() {
        Animated.timing(this.state.animation, {
            toValue: 0,
            timing: 400,
        }).start()
    }

    loadScreen() {
        this.timeoutHandle = setTimeout(() => {
            // after 4s fade out
            this.fadeOut()
            this.timeoutHandle = setTimeout(() => {
                // after 1,5s change color
                this.setState({ backgroundColor: '#202945' })
                this.timeoutHandle = setTimeout(() => {
                    // after 1s change screen
                    this.goToOnboarding()
                }, 1500);
            }, 1500)
        }, 4000);
    }

    setInitialState() {
        this.setState(
            {
                backgroundColor: "#ffffff",
                animation: new Animated.Value(1)
            }
        );
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.setInitialState();
            this.loadScreen();
        });
    }

    render() {
        //const { navigation } = this.props;
        const animatedStyle = {
            opacity: this.state.animation
        }
        return (
            <View style={[styles.container, { backgroundColor: this.state.backgroundColor }]}>
                <Animated.View style={animatedStyle}>
                    <Logo width="300" height="300" />
                </Animated.View>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});