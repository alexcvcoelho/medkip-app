import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';

import IconBack from "~/assets/images/arrow-back.svg";
import { Avatar } from 'react-native-elements'

import IconCamera from "~/assets/images/camera.svg";

import ServiceApi from '~/api/api-caller';
import ManagerAuth from '~/manger-auth/manager';

import FloatingInput from '~/components/floatingInput';
import { BottomSheetFailure, SimpleBottomSheet } from '~/components/bottomSheets';
import IconPathSuccess from "~/assets/images/icon-path-profile.svg";
import { variables, hexToRGB } from '~/theme';


export default class PersonUpdate extends Component {
    state = {
        isLoading: true,
        isSaving: false,
        token: '',
        name: '',
        isPicture: false,
        avatar: '',
        initials: '',

        textFieldName: '',
        textFieldUniversity: '',
        textFieldBio: '',
        textfieldSpeciality: '',
        textFieldCity: '',
        textfieldState: '',
        textfieldCrm: '',

        modalSuccess: false,
        modalInfo: false,
        messageInfo: '',
    }

    onPressBack = () => {
        this.setState({ modalSuccess: false })
        this.props.navigation.goBack();
    }

    getToken = async () => {
        const idToken = await ManagerAuth.getToken();
        this.setState({ token: idToken.toString(), isLoading: true })
        this.getUserData();
    }

    onUploadImage = async () => {
        this.props.navigation.navigate('UploadImage')
    }

    onPressSave = () => {
        this.setState({ isSaving: true })

        ServiceApi.saveProfile(this.state.token,
            this.state.textFieldBio,
            this.state.textFieldCity,
            this.state.textFieldUniversity,
            this.state.textfieldCrm,
            this.state.textfieldSpeciality,
            this.state.textFieldName,
            this.state.textfieldState,
            () => {
                this.setState({ isSaving: false, modalSuccess: true })
            }, (error) => {
                // tratar o erro ou disparar erro default
                this.setState({ isSaving: false, modalInfo: true, messageInfo: error })
            }, this.props);
    }

    setUserInfos(profile) {
        this.setState({
            name: profile.name ? profile.name : "",
            textFieldName: profile.name ? profile.name : "",
            textFieldCity: profile.city ? profile.city : "",
            textfieldState: profile.state ? profile.state : "",
            textFieldBio: profile.bio ? profile.bio : "",
            textFieldUniversity: profile.college ? profile.college : "",
            textfieldCrm: profile.crm ? profile.crm : "",
            textfieldSpeciality: profile.expertise ? profile.expertise : "",
            isPicture: profile.avatar ? true : false,
            avatar: `${profile.avatar}?v1.${new Date()}`,
            initials: profile.name ? profile.name.split(" ").map((n) => n[0]).join(".") : "",
            isLoading: false
        })
    }
    getUserData() {
        ServiceApi.getProfile(this.state.token, (data) => {
            this.setUserInfos(data.profile);
        }, (error) => {
            // tratar o erro ou disparar erro default
            this.setState({
                isLoading: false, modalInfo: true, messageInfo: error
            })
        }, this.props);
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={this.onPressBack}>
                        <IconBack width="32" height="32"></IconBack>
                    </TouchableOpacity>
                    <Text style={[styles.text_title]}>Editar Dados Pessoais</Text>
                    <View style={{ width: 30, height: 30 }}>
                    </View>
                </View>

                {this.state.isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#4493FF" />
                    </View>
                    :
                    <ScrollView style={styles.sub_container}>
                        {this.state.name == '' ? null
                            : (
                                this.state.isPicture ?
                                    <TouchableOpacity style={styles.view_image} onPress={this.onUploadImage}>
                                        <View style={{
                                            width: 80,
                                        }}>
                                            < Avatar
                                                rounded
                                                size="large"
                                                source={{
                                                    uri:
                                                        this.state.avatar,
                                                }}
                                            />
                                            <View style={{ position: 'absolute', bottom: 0, right: 0, justifyContent: 'center', alignItems: 'center', width: 30, height: 30, borderRadius: 30 / 2, backgroundColor: '#006CFF' }}>
                                                <IconCamera width="18" height="16"></IconCamera>
                                            </View>
                                        </View>
                                    </TouchableOpacity>

                                    :
                                    <TouchableOpacity style={styles.view_image} onPress={this.onUploadImage}>
                                        <View style={{
                                            width: 80,
                                        }}>
                                            <Avatar
                                                size="large"
                                                rounded
                                                title={this.state.initials}
                                                avatarStyle={{ backgroundColor: '#d3d3d3', opacity: 0.7 }}
                                                titleStyle={{ color: 'black', padding: 5, fontSize: 13 }}
                                            />
                                            <View style={{ position: 'absolute', bottom: 0, right: 0, justifyContent: 'center', alignItems: 'center', width: 30, height: 30, borderRadius: 30 / 2, backgroundColor: '#006CFF' }}>
                                                <IconCamera width="18" height="16"></IconCamera>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            )
                        }

                        <View style={{ marginTop: 38 }}>
                            <FloatingInput
                                label="Nome"
                                isMask={false}
                                value={this.state.textFieldName}
                                onChangeText={(text) => this.setState({ textFieldName: text })}
                            />
                        </View>

                        <View style={{ marginTop: 22 }}>
                            <FloatingInput
                                label="Universidade"
                                isMask={false}
                                value={this.state.textFieldUniversity}
                                onChangeText={(text) => this.setState({ textFieldUniversity: text })}
                            />
                        </View>


                        <View style={{ marginTop: 22 }}>
                            <FloatingInput
                                label="Residência"
                                isMask={false}
                                value={this.state.textFieldBio}
                                onChangeText={(text) => this.setState({ textFieldBio: text })}
                            />
                        </View>

                        <View style={{ marginTop: 22 }}>
                            <FloatingInput
                                label="Especialidade"
                                isMask={false}
                                value={this.state.textfieldSpeciality}
                                onChangeText={(text) => this.setState({ textfieldSpeciality: text })}
                            />
                        </View>

                        <View style={{ marginTop: 22 }}>
                            <FloatingInput
                                label="CRM"
                                isMask={false}
                                value={this.state.textfieldCrm}
                                onChangeText={(text) => this.setState({ textfieldCrm: text })}
                            />
                        </View>

                        <View style={{ marginTop: 22 }}>
                            <FloatingInput
                                label="Cidade"
                                isMask={false}
                                value={this.state.textFieldCity}
                                onChangeText={(text) => this.setState({ textFieldCity: text })}
                            />
                        </View>

                        <View style={{ marginTop: 22 }}>
                            <FloatingInput
                                label="Estado"
                                isMask={false}
                                value={this.state.textfieldState}
                                onChangeText={(text) => this.setState({ textfieldState: text })}
                            />
                        </View>

                        <TouchableOpacity style={styles.button_continue} onPress={this.onPressSave}>
                            {this.state.isSaving ?
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator size="large" color="#4493FF" />
                                </View>
                                :
                                <Text style={styles.text_style_bold}>Salvar</Text>
                            }
                        </TouchableOpacity>

                        <View style={styles.margin_end}></View>
                    </ScrollView>
                }
                <SimpleBottomSheet
                    visible={this.state.modalSuccess}
                    onClose={() => this.setState({ modalSuccess: false })}
                >
                    <View style={{ alignItems: 'center' }}>
                        <IconPathSuccess />
                        <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Perfil salvo com sucesso!'}</Text>
                    </View>
                    <Button
                        title="Ver Perfil"
                        disabled={false}
                        onPress={() => this.onPressBack()}
                        buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                        titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                    />

                    <View style={{ height: variables.paddingHorizontal }} />

                    <Button
                        title="Fechar"
                        disabled={false}
                        buttonStyle={{ backgroundColor: variables.light, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight, borderWidth: 1 }}
                        titleStyle={{ color: variables.primary, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                        onPress={() => this.setState({ modalSuccess: false })}
                    />
                </SimpleBottomSheet>


                <BottomSheetFailure
                    visible={this.state.modalInfo}
                    text={this.state.messageInfo}
                    onClose={() => { this.setState({ modalInfo: false }); }}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    sub_container: {
        marginLeft: 16,
        marginRight: 16
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Bold',
        justifyContent: 'center'
    },
    view_image: {
        alignItems: 'center',
        marginTop: 24
    },
    button_continue: {
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: '#006CFF',
        height: 48,
        width: '100%',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text_style_bold: {
        color: '#ffffff',
        fontSize: 14,
        marginLeft: 24,
        fontFamily: 'Montserrat-SemiBold'
    },
    text_input_view: {
        height: 56,
        marginTop: 10,
        borderWidth: 1,
        borderRadius: 6,
    },
    text_input_view_disable: {
        borderColor: '#364574',
        opacity: 0.3
    },
    text_input_view_enable: {
        borderColor: '#006CFF',
        opacity: 1.0
    },
    float_text: {
        paddingLeft: 12,
        paddingRight: 12,
        position: 'absolute',
        top: 0,
        marginLeft: 10,
        backgroundColor: 'white',
    },

    field_disactivated: {
        color: '#364574',
    },
    field_activated: {
        color: '#006CFF',
    },
    margin_end: {
        marginTop: 40
    }
});