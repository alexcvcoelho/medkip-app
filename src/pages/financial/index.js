import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, Dimensions } from 'react-native';

import { Divider, Icon } from 'react-native-elements';

import { MaskService } from 'react-native-masked-text';

import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import {
    StackedBarChart
} from 'react-native-chart-kit'

import { theme, variables, hexToRGB } from '~/theme';
import { SimpleModal } from '~/components/bottomSheets';

import IconMenu from "~/assets/images/menu.svg";
import IconBell from "~/assets/images/bell.svg";

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';

import moment from 'moment';


export default class Financial extends Component {
    state = {
        token: '',
        selectedTab: 1,
        filtredItems: [],
        total: 0,
        tax: 0,
        filter: 'Últimos 3 meses',
        showValues: false,
        showValues2: false,
        showChartInfo: false,
        plantoes: [],
        currentYear: '',
        data: {
            labels: [],//["Jan", "Fev", "Mar", "Abr", "Mai", "Jun"],
            legend: [],
            data: [],/*[
                [0, 100],
                [300 - 80, 80],
                [440 - 80, 80],
                [320 - 80, 80],
                [380 - 80, 80],
                [500 - 80, 80],
            ],*/
            barColors: [variables.secondary, '#707B9F']
        },

        valores: [],
    }

    convertMoney = (value) => {
        let money = MaskService.toMask('money', value, {
            unit: 'R$ ',
            separator: ',',
            delimiter: '.'
        })

        return money;
    }

    onShowValues = (value, show) => {
        if (show) {
            return value;
        } else {
            let v = value.split('R$ ');
            let points = '';
            for (let i = 0; i < v[1].length; i++) {
                points += '. ';
            }
            return 'R$ ' + points;
        }
    }

    reloadFinancalGridPeriod = (value) => {
        this.setState({ filter: value })

        var period = ''

        if (value != null) {
            if (value == 'Últimos 3 meses') {
                period = "3"
            } else if (value == 'Últimos 6 meses') {
                period = "6"
            } else if (value == 'Últimos 12 meses') {
                period = "12"
            }
        }
        this.showGrid(period)
    }

    showGrid = (value) => {
        ServiceApi.financialGetDashDataGrid(this.state.token, value.toString(), (response) => {
            if (response != null) {
                /*
                 {
                    type: 1,
                    title: 'Janeiro 2020',
                    value: 20000
                },
                {
                    type: 1,
                    title: 'Fevereiro 2020',
                    value: 15000
                },
                {
                    type: 1,
                    title: 'Março 2020',
                    value: 20000
                },
                {
                    type: 2,
                    title: 'Janeiro 2021',
                    value: 20000
                },
                {
                    type: 2,
                    title: 'Fevereito 2021',
                    value: 10000
                } 
                 */

                var items = []

                response.map((item) => {
                    items.push({ title: item.month, type: 1, value: item.liquidAmount })
                    items.push({ title: item.month, type: 2, value: item.amount })
                });

                this.setState({ valores: items })
                this.changeTab(this.state.selectedTab)
            }
        }, (error) => {

        })
    }

    fillData = () => {

        this.setState({ currentYear: moment().format('YYYY') })

        ServiceApi.financialGetDashNextOrdeal(this.state.token, (response) => {
            if (response != null) {
                const {
                    nextOrderlies,
                    amount,
                    tax,
                } = response;

                this.setState({ total: amount, tax: tax })

                if (nextOrderlies != null) {
                    this.setState({ plantoes: nextOrderlies })
                    /*const {
                        title,
                        amount,
                        tax,
                    } = nextOrderlies; */
                }
            }
        }, (errr) => {

        })

        ServiceApi.financialGetDashDataChart(this.state.token, (response) => {
            if (response != null) {

                var months = []
                var graph = []

                var count = 0
                response.map((item) => {
                    if (item.liquidAmount == 0 && item.amount == 0) {
                        count++
                    }
                    months.push(item.month)
                    graph.push([item.liquidAmount, item.amount])
                });


                this.setState(prevState => {
                    let data = Object.assign({}, prevState.data);
                    data.labels = count == response.length ? [] : months
                    data.data = count == response.length ? [] : graph
                    return { data };
                })

                //console.log('teste')
                //console.log(this.state.data.data.length)
            }
        }, (error) => {

        })

        this.showGrid("3")
    }

    getToken = async () => {
        const idToken = await ManagerAuth.getToken();

        this.setState({ token: idToken.toString() })
        this.fillData();
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    changeTab = (tab) => {
        const data = [...this.state.valores];
        this.setState({
            filtredItems: data.filter(e => e.type === tab),
            selectedTab: tab,
        });
    }

    render() {
        const navigation = this.props.navigation;

        const chartConfigs = {
            backgroundColor: 'white',
            backgroundGradientFrom: 'white',
            backgroundGradientTo: 'white',
            color: () => hexToRGB(variables.secondary, 0.1),
            decimalPlaces: 2,
            style: {
                paddingHorizontal: variables.paddingHorizontal,
                borderRadius: 0,
            },
            labelColor: () => variables.gray3,
        };

        const width = Dimensions.get('window').width;
        const height = 220;
        const graphStyle = {
            marginVertical: 8,
            ...chartConfigs.style
        }

        const menu = {
            optionsContainer: {
                padding: 0,
                width: 130,
            },
            optionWrapper: {
                margin: 0,
                padding: 0,
            },
        }

        const active_line = {
            height: 3,
            backgroundColor: variables.accent,
            bottom: -1,
            borderRadius: variables.borderRadius,
            width: '50%',
            position: 'absolute',
            left: this.state.selectedTab === 1 ? 0 : '50%',
        }

        return (
            <MenuProvider>
                <View style={[theme.container, { backgroundColor: '#F5F6FA' }]}>
                    <View style={theme.navigation_bar}>
                        <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <IconMenu width="32" height="32"></IconMenu>
                        </TouchableOpacity>
                        <Text style={[theme.text_title]}>Financeiro</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Notifications')} style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <IconBell width="32" height="32"></IconBell>
                        </TouchableOpacity>
                    </View>
                    <ScrollView>
                        <View style={styles.page}>
                            <View style={styles.card}>
                                <View style={styles.row}>
                                    <Text style={theme.title}>Valor Líquido nos Próximos 30 dias</Text>
                                    <TouchableOpacity onPress={() => { }}>
                                        <Icon name="info" type="feather" color={variables.gray3} size={20} />
                                    </TouchableOpacity>
                                </View>
                                <Divider style={styles.divider} />
                                <View style={styles.row}>
                                    <Text style={styles.value}>{this.onShowValues(this.convertMoney(this.state.total), this.state.showValues)}</Text>
                                    <TouchableOpacity onPress={() => this.setState({ showValues: !this.state.showValues })}>
                                        <Icon name={this.state.showValues ? 'eye' : 'eye-off'} type="feather" color={variables.primary} size={20} />
                                    </TouchableOpacity>
                                </View>
                                <Divider style={styles.divider} />
                                <View style={styles.row}>
                                    <View>
                                        <Text style={styles.text}>Imposto</Text>
                                        <Text style={styles.text}>Previsão de valor/h</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.text}>Valor Liq.</Text>
                                        <Text style={[styles.text, { fontWeight: "bold" }]}>{this.convertMoney(this.state.tax)}</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.card}>
                                <View style={styles.row}>
                                    <Text style={theme.title}>Previsão dos próximos plantões</Text>
                                    <TouchableOpacity onPress={() => this.setState({ showValues2: !this.state.showValues2 })}>
                                        <Icon name={this.state.showValues2 ? 'eye' : 'eye-off'} type="feather" color={variables.primary} size={20} />
                                    </TouchableOpacity>
                                </View>

                                {this.state.plantoes.length > 0 ?
                                    this.state.plantoes.map((item, index) => (<View key={index}>
                                        <View style={[styles.row, { alignItems: 'flex-end' }]} key={index}>
                                            <View>
                                                <Text style={styles.subtitle}>{item.title}</Text>
                                                <Text style={styles.text}>Previsão de valor/h</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.text}>{this.onShowValues(this.convertMoney(item.amount), this.state.showValues2)}</Text>
                                            </View>
                                        </View>
                                        { index < this.state.plantoes.length - 1 && <Divider style={styles.divider} />}
                                    </View>
                                    )) : null}
                            </View>

                            <View style={styles.card}>
                                <View style={styles.row}>
                                    <Text style={theme.title}>Gráfico de Rendimento - {this.state.currentYear}</Text>
                                    <TouchableOpacity onPress={() => this.setState({ showChartInfo: true })}>
                                        <Icon name="info" type="feather" color={variables.gray3} size={20} />
                                    </TouchableOpacity>
                                </View>
                                {this.state.data.data.length > 0 ?
                                    <StackedBarChart
                                        width={width - variables.paddingHorizontal * 4}
                                        height={height}
                                        data={this.state.data}
                                        chartConfig={chartConfigs}
                                        style={graphStyle}
                                    />
                                    : null
                                }

                                <View style={[styles.spacing, { flexDirection: 'row' }]}>
                                    <View style={{ flexDirection: 'row', flex: 1 / 2 }}>
                                        <View style={styles.rect}></View>
                                        <Text style={styles.text3}>Valor Líquido</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', flex: 1 / 2 }}>
                                        <View style={[styles.rect, { backgroundColor: '#707B9F' }]}></View>
                                        <Text style={styles.text3}>Valor Bruto</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.card}>
                                <View>
                                    <View style={styles.row}>
                                        <TouchableOpacity style={{ flex: 1 / 2, alignItems: 'center' }} onPress={() => this.changeTab(1)}>
                                            <Text style={{ color: this.state.selectedTab === 1 ? variables.primary : variables.gray3 }}>Valor Líquido</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flex: 1 / 2, alignItems: 'center' }} onPress={() => this.changeTab(2)}>
                                            <Text style={{ color: this.state.selectedTab === 2 ? variables.primary : variables.gray3 }}>Valor Bruto</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Divider style={styles.divider} />
                                    <View style={active_line} />
                                </View>
                                <View style={styles.row}>
                                    <Text style={{ color: '#A1A6B9' }}>Filtar por:</Text>
                                    <Menu onSelect={value => this.reloadFinancalGridPeriod(value)}>
                                        <MenuTrigger>
                                            <View style={styles.textfield}>
                                                <Text style={styles.input} numberOfLines={1}>{this.state.filter}</Text>
                                                <View style={{ width: 20 }}>
                                                    <Icon name="chevron-down" type="feather" color="#A1A6B9" size={16} />
                                                </View>
                                            </View>
                                        </MenuTrigger>
                                        <MenuOptions optionsContainerStyle={{ marginTop: 29 }} customStyles={menu}>
                                            <MenuOption value="Últimos 3 meses">
                                                <View style={theme.dropdown_item}>
                                                    <Text style={[theme.dropdown_item_txt, { fontSize: 12 }]}>Últimos 3 meses</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption value="Últimos 6 meses">
                                                <View style={theme.dropdown_item}>
                                                    <Text style={[theme.dropdown_item_txt, { fontSize: 12 }]}>Últimos 6 meses</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption value="Últimos 12 meses">
                                                <View style={theme.dropdown_item}>
                                                    <Text style={[theme.dropdown_item_txt, { fontSize: 12 }]}>Últimos 12 meses</Text>
                                                </View>
                                            </MenuOption>
                                        </MenuOptions>
                                    </Menu>
                                </View>
                                <View style={styles.table}>
                                    {this.state.filtredItems.map((item, index) => (
                                        <View style={[styles.tr, index & 1 ? {} : styles.tr_color]} key={`tr_${index}`}>
                                            <Text style={styles.text3}>{item.title}</Text>
                                            <Text style={styles.text3}>{this.convertMoney(item.value)}</Text>
                                        </View>
                                    ))}
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <SimpleModal visible={this.state.showChartInfo} onClose={() => this.setState({ showChartInfo: false })} title="Texto sobre o infográfico">
                    <Text style={styles.paragraph}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pitor nibh id sapien condsectetur fringilla. Cras lacinia ipsum nec arcu tincidunt, id elementum massa suscipit. </Text>
                </SimpleModal>
            </MenuProvider>
        );
    }
}

const styles = StyleSheet.create({
    //meus estilos
    page: {
        padding: variables.paddingHorizontal,
        flex: 1,
    },
    spacing: {
        padding: variables.paddingHorizontal,
    },
    card: {
        backgroundColor: variables.light,
        borderRadius: variables.borderRadius,
        // padding: variables.paddingHorizontal,
        marginVertical: variables.paddingHorizontal / 2,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: variables.paddingHorizontal,
    },
    divider: {
        backgroundColor: hexToRGB(variables.secondary_light, 0.1),
    },
    value: {
        color: variables.secondary,
        fontSize: 28,
        fontFamily: variables.fontBold,
    },
    subtitle: {
        color: variables.secondary,
        fontSize: variables.fontSize,
        fontWeight: "bold",
    },
    text: {
        color: variables.gray2,
        fontSize: variables.fontSize,
        marginTop: 5,
    },
    text3: {
        color: variables.gray3,
    },
    rect: {
        width: 12,
        height: 12,
        backgroundColor: variables.secondary,
        marginRight: 8,
        alignSelf: 'center',
        borderRadius: 2,
    },
    textfield: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: variables.paddingHorizontal / 2,
        borderRadius: variables.borderRadius,
        backgroundColor: variables.light,
        borderWidth: 1,
        borderColor: '#C0C4D0',
        position: 'relative',
        height: 29,
        width: 130,
    },
    input: {
        flexGrow: 1,
        color: variables.gray2,
        fontSize: 12,
    },
    table: {
        paddingHorizontal: variables.paddingHorizontal,
        paddingBottom: variables.paddingHorizontal,
    },
    tr: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: variables.paddingHorizontal / 2,
    },
    tr_color: {
        backgroundColor: '#E8F2FF',
    },
    paragraph: {
        fontSize: variables.fontSize,
        color: variables.gray2,
        marginVertical: 8,
    },
});