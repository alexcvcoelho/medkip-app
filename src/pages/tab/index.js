import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { HomeStackNavigator, CasesStackNavigator, FinancialStackNavigator, ShiftsStackNavigator, ProfileStackNavigator } from "~/pages/stack-navigation";

import Filter from "~/components/bottomSheets/filter";

import HomeActive from "~/assets/images/home-active.svg";
import HomeInactive from "~/assets/images/home-inactive.svg";

import CasesActive from "~/assets/images/cases-active.svg";
import CasesInactive from "~/assets/images/cases-inactive.svg";

import FinancialActive from "~/assets/images/financial-active.svg";
import FinancialInactive from "~/assets/images/financial-inactive.svg";

import ShiftsActive from "~/assets/images/calendar-active.svg";
import ShiftsInactive from "~/assets/images/calendar-inactive.svg";

import ProfileActive from "~/assets/images/profile-active.svg";
import ProfileInactive from "~/assets/images/profile-inactive.svg";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
    return (
        <>
            <Tab.Navigator
                initialRouteName="Home"
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {


                        if ((route.name == 'Home') && (focused)) {
                            return <HomeActive width={20} height={20}></HomeActive>

                        } else if ((route.name == 'Home') && (!focused)) {
                            return <HomeInactive width={20} height={20}></HomeInactive>
                        }

                        else if ((route.name == 'Cases') && (focused)) {
                            return <CasesActive width={20} height={size}></CasesActive>

                        } else if ((route.name == 'Cases') && (!focused)) {
                            return <CasesInactive width={20} height={size}></CasesInactive>
                        }

                        else if ((route.name == 'Financial') && (focused)) {
                            return <FinancialActive width={20} height={size}></FinancialActive>

                        } else if ((route.name == 'Financial') && (!focused)) {
                            return <FinancialInactive width={20} height={size}></FinancialInactive>
                        }

                        else if ((route.name == 'Shifts') && (focused)) {
                            return <ShiftsActive width={24} height={size}></ShiftsActive>

                        } else if ((route.name == 'Shifts') && (!focused)) {
                            return <ShiftsInactive width={24} height={size}></ShiftsInactive>
                        }

                        else if ((route.name == 'Profile') && (focused)) {
                            return <ProfileActive width={20} height={size}></ProfileActive>

                        } else if ((route.name == 'Profile') && (!focused)) {
                            return <ProfileInactive width={20} height={size}></ProfileInactive>
                        }
                    },
                })}
                tabBarOptions={{
                    showLabel: true,
                    labelStyle: {
                        fontSize: 10,
                        fontFamily: 'Montserrat-Regular'
                    },
                    activeTintColor: '#006CFF',
                    inactiveTintColor: '#afb5c7',
                    style: { height: 90, borderTopLeftRadius: 15, borderTopRightRadius: 15, paddingBottom: 20, paddingTop: 20 }
                }}>
                <Tab.Screen name="Home" component={HomeStackNavigator} options={{
                    tabBarLabel: 'Home',
                }} />
                <Tab.Screen name="Cases" component={CasesStackNavigator} options={{
                    tabBarLabel: 'Casos',
                }} />
                <Tab.Screen name="Financial" component={FinancialStackNavigator} options={{
                    tabBarLabel: 'Financeiro',
                }} />
                <Tab.Screen name="Shifts" component={ShiftsStackNavigator} options={{
                    tabBarLabel: 'Plantões',
                }} />
                <Tab.Screen name="Profile" component={ProfileStackNavigator} options={{
                    tabBarLabel: 'Conta',
                }} />
            </Tab.Navigator>
        </>
    );
};

export default BottomTabNavigator;
