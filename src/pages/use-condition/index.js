import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import IconBack from "~/assets/images/arrow-back.svg";

import { CheckBox } from 'react-native-elements';
import IconCheckboxActive from "~/assets/images/icon-checkbox-active.svg";
import IconCheckboxInactive from "~/assets/images/icon-checkbox-inactive.svg";

import { variables, hexToRGB } from '~/theme';
import { Button } from 'react-native-elements';


export default class UseConditionTerms extends Component {
    state = {
        dontShow: false,
        description: "6.4.      O Usuário é o responsável por incluir no cadastro informações exatas referentes a empresa que faz parte (PJ) e hospital que presta serviços médicos (Plantão) para que seja possível a MedKip informar com precisão os valores que são pagos de maneira bruta e líquida pelo Usuário pela prestação dos seus serviços (Plantão).\n\n6.4.1.   O Usuário que faz parte de uma empresa (PJ) para prestar serviços de plantão em hospitais, necessariamente precisa emitir Nota Fiscal ao local que prestou serviço médico (plantão). Essa nota poderá ser emitida para cada plantão ou de uma única vez de maneira mensal.\n\n6.4.2. A empresa (PJ) que o Usuário faz parte pode estar enquadrada no regime tributário chamado \"Lucro Presumido\" ou \"Simples Nacional\".\n\n6.4.3. A empresa que está enquadrada no \"Lucro Presumido\", o Usuário precisa pagar por Nota Fiscal emitida os seguintes impostos: (i) Impostos Federais: PIS (0,65%), COFINS (3%), IRPJ (1,5%) e CSLL (1%); e (ii) Imposto Municipal: Imposto Sobre Serviço – ISS. Este imposto é Municipal e é devido na cidade em que a empresa (PJ) / Usuário presta o serviço, independentemente da sede da sua empresa. De acordo com o IBGE (https://cidades.ibge.gov.br/brasil/panorama), o Brasil possui 5.570 cidades e o ISS pode variar de cidade para cidade de 2% a 5%. Em razão disso, a MedKip utilizará como padrão no Aplicativo a alíquota de 2% que é a aplicada na cidade de São Paulo. Assim, caso o Usuário preste serviço por meio da sua empresa (PJ) em uma cidade que a alíquota é outra, fica responsável por alterar no cadastro a alíquota. Essa informação pode ser obtida por meio de uma ligação a sua Contabilidade ou verificando no Informe de Rendimentos que a Contabilidade envia mensalmente.\n\n6.4.3.1. A empresa que está enquadrada no \"Lucro Presumido\" também paga impostos de maneira trimestral (IRPJ alíquota de 15% e CSLL alíquota de 9%) que terão como base de cálculo a quantia de 32% (alíquota) sobre o valor total bruto recebido pela empresa no último trimestre. Como a MedKip não tem acesso ao valor faturado pela empresa (PJ) que o Usuário faz parte não é possível mensurar estes valores, mas que a depender do faturamento e da quantidade de sócios que integram a empresa (PJ) este valor é irrisório quando comparado aos impostos pagos descritos no Item 6.4.3.\n\n6.4.4.   A empresa que está enquadrada no \"Simples Nacional\" paga por Nota Fiscal emitida uma única alíquota que engloba todos os impostos. Serviços Médicos estão previstos na Tabela do Anexo III do Simples Nacional que pode variar de 6% a 33% a depender da receita bruta em 12 meses. A maioria dos médicos que se associam em uma empresa para prestar serviço de plantão acabam optando pelo Lucro Presumido, pois em razão do faturamento bruto desses médicos nos últimos 12 meses acaba atingindo uma faixa de receita em que a alíquota mínima é de 16%. Como a MedKip não tem acesso ao valor faturado pela empresa (PJ) que o Usuário faz parte não é possível precisar o imposto que é recolhido. Em razão disso, a MedKip utilizará como padrão no Aplicativo a alíquota de 16%, ficando o Usuário responsável por alterar no cadastro da empresa a alíquota exata que é paga. Essa informação pode ser obtida por meio de uma ligação a sua Contabilidade ou verificando no Informe de Rendimentos que a Contabilidade envia mensalmente.",
    }

    onPress = () => {
        this.props.navigation.navigate('DrawerNavigator');
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={this.onPressBack}>
                        <IconBack width="32" height="32"></IconBack>
                    </TouchableOpacity>
                    <Text style={[styles.text_title]}>Termos de Uso</Text>
                    <View style={{ width: 30, height: 30 }}>
                    </View>
                </View>
                <ScrollView style={{ padding: 30 }}>
                    <Text style={{ color: '#626B8A', fontFamily: 'Montserrat-Regular', fontSize: 14 }}>
                        {this.state.description}
                    </Text>

                    <View style={{ marginBottom: 20 }}></View>

                    <View>
                        <CheckBox
                            title='Li e aceito os termos de uso'
                            containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, paddingHorizontal: 0, marginLeft: 0, paddingVertical: variables.paddingHorizontal }}
                            textStyle={{ color: variables.gray2, fontWeight: this.state.dontShow ? 'bold' : 'normal' }}
                            checked={this.state.dontShow}
                            onPress={() => this.setState({ dontShow: this.state.dontShow ? false : true })}
                            checkedIcon={<IconCheckboxActive />}
                            uncheckedIcon={<IconCheckboxInactive />}
                        />
                    </View>

                    <View style={{ marginBottom: 20 }}></View>

                    <Button
                        title="Continuar"
                        disabled={!this.state.dontShow}
                        onPress={() => this.onPress()}
                        buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                        titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                    />
                    <View style={{ marginBottom: 100 }}></View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10,
        width: '100%'
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
});