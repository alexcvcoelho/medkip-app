import React, { Component } from 'react';

import IconBack from "~/assets/images/arrow-back.svg";
import { View, StyleSheet, TouchableOpacity, Text, ActivityIndicator } from 'react-native';

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';
import { Button } from 'react-native-elements';
import IconPathSuccess from "~/assets/images/icon-path-profile.svg";
import { BottomSheetFailure, SimpleBottomSheet, SimpleModal } from '~/components/bottomSheets';

import { theme, variables, hexToRGB } from '~/theme';
import FloatingInput from '~/components/floatingInput';
import { ScrollView } from 'react-native';

export default class HospitalSave extends Component {
    state = {
        isLoading: false,
        token: '',
        name: '',
        city: '',
        aliquot: '',
        id: null,
        modalSuccess: false,
        modalVisible: false,
        modalInfo: false,
        messageInfo: '',
        itemEdit: null,
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }

    validateFields = () => {

        if (this.state.name.length == 0) {
            this.setState({ messageInfo: 'Campo nome do hospital é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.city.length == 0) {
            this.setState({ messageInfo: 'Campo cidade é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.aliquot.length == 0) {
            this.setState({ messageInfo: 'Campo iss é obrigatório!', modalInfo: true })
            return false
        }
        return true
    }

    fillForm = () => {
        if (this.state.itemEdit) {

            const {
                id,
                city,
                name,
                iss,
            } = this.state.itemEdit;

            this.setState({
                id: id,
                name: name,
                city: city,
                aliquot: iss.toString(),
            });
        }
    }

    getToken = async () => {
        this.setState({ isLoading: true })

        const idToken = await ManagerAuth.getToken();

        const { route } = this.props;
        const itemEdit = route.params.hospital
        if (itemEdit) {
            console.log(itemEdit.id);
        }

        this.setState({ token: idToken.toString(), isLoading: false, itemEdit: itemEdit })

        this.fillForm()
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    handleISSNumber = (text) => {
        if (/^\d+$/.test(text) || text === '') {
            this.setState({ aliquot: text })
        }
    }

    onPressSave = () => {
        const result = this.validateFields()

        if (!result) {
            return
        }

        this.setState({ isLoading: true })

        ServiceApi.saveHospital(this.state.token,
            this.state.id,
            this.state.name.toString(),
            this.state.city.toString(),
            parseInt(this.state.aliquot.toString()), () => {
                this.setState({ isLoading: false, modalSuccess: true })
            }, (error) => {
                this.setState({
                    isLoading: false, modalInfo: true, messageInfo: error
                })
            }, this.props);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={this.onPressBack}>
                        <IconBack width="32" height="32"></IconBack>
                    </TouchableOpacity>
                    <Text style={[styles.text_title]}>Adicionar Hospital</Text>
                    <View style={{ width: 30, height: 30 }}>
                    </View>
                </View>

                <ScrollView>
                    <View style={styles.sub_container}>

                        <View style={{ marginTop: 22 }}>
                            <FloatingInput
                                label="Digite o nome do Hospital"
                                isMask={false}
                                value={this.state.name}
                                onChangeText={(text) => this.setState({ name: text })}
                            />
                        </View>

                        <View style={{ marginTop: 16 }}>
                            <FloatingInput
                                label="Cidade"
                                isMask={false}
                                value={this.state.city}
                                onChangeText={(text) => this.setState({ city: text })}
                            />
                        </View>


                        <View style={{ marginTop: 16 }}>
                            <FloatingInput
                                label="Alíquota de ISS"
                                isMask={false}
                                keyboardType="numeric"
                                value={this.state.aliquot}
                                onChangeText={(text) => this.handleISSNumber(text)}
                            />
                        </View>

                        <View style={{ marginTop: 16 }}>
                            <TouchableOpacity style={styles.button_continue} onPress={this.onPressSave}>
                                {this.state.isLoading ?
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                        <ActivityIndicator size="large" color="#4493FF" />
                                    </View>
                                    :
                                    <Text style={styles.text_style_bold}>Salvar</Text>
                                }
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>

                <SimpleBottomSheet
                    visible={this.state.modalSuccess}
                    onClose={() => this.setState({ modalSuccess: false })}>
                    <View style={{ alignItems: 'center' }}>
                        <IconPathSuccess />
                        <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Hospital salvo com sucesso!'}</Text>
                    </View>
                    <Button
                        title="Ver Hospital"
                        disabled={false}
                        onPress={() => this.onPressBack()}
                        buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                        titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                    />

                    <View style={{ height: variables.paddingHorizontal }} />
                </SimpleBottomSheet>

                <BottomSheetFailure
                    visible={this.state.modalInfo}
                    text={this.state.messageInfo}
                    onClose={() => { this.setState({ modalInfo: false }); }}
                />

                <SimpleModal
                    visible={this.state.modalVisible}
                    onClose={() => this.setState({ modalVisible: false })}>
                    <Text style={{ fontFamily: 'Montserrat-Bold', color: '#364574', fontSize: 14 }}>{`Impostos pagos por\nnota fiscal`}</Text>
                    <Text style={{ fontFamily: 'Montserrat-Regular', color: '#626B8A', fontSize: 14, marginTop: 24 }}>{`Imposto Municipal:\nISS de 2%`}</Text>
                    <Text style={{ fontFamily: 'Montserrat-Regular', color: '#626B8A', fontSize: 14, marginTop: 12 }}>{`Impostos Federais:\nPIS (0,65%)\nCOFINS (3%)\nIRPJ (1,5%)\nCSLL (1%)`}</Text>
                </SimpleModal>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
    button_continue: {
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: '#006CFF',
        height: 48,
        width: '100%',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text_style_bold: {
        color: '#ffffff',
        fontSize: 14,
        marginLeft: 24,
        fontFamily: 'Montserrat-SemiBold'
    },
    sub_container: {
        marginLeft: 16,
        marginRight: 16
    },
});