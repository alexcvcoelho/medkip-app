import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Modal } from 'react-native';

import { Divider, Icon } from 'react-native-elements';

import { theme, variables, hexToRGB } from '~/theme';
import { SimpleBottomSheet } from '~/components/bottomSheets';

import IconBack from "~/assets/images/close-white.svg";
import IconBack2 from "~/assets/images/arrow-back.svg";
import IconNotFound from "~/assets/images/icon-notfound.svg";


export default class Help extends Component {
    state = {
        showHelp: false,
        showSearch: false,
        query: '',
        results: [
            {
                "title": "Lorem ipsum dolor sit am, consectetur?",
                "description": "Consectetur porttitor elit proin elit et."
            },
            {
                "title": "Lorem ipsum dolor sit am, consectetur?",
                "description": "Consectetur porttitor elit proin elit et."
            },
            {
                "title": "Lorem ipsum dolor sit am, consectetur?",
                "description": "Consectetur porttitor elit proin elit et."
            },
            {
                "title": "Lorem ipsum dolor sit am, consectetur?",
                "description": "Consectetur porttitor elit proin elit et."
            },
            {
                "title": "Lorem ipsum dolor sit am, consectetur?",
                "description": "Consectetur porttitor elit proin elit et."
            },
        ],
        filterResults: [],
    }

    onChangeTextBar = (text) => {
        let filteredData = this.state.results.filter(x => String(x.title).includes(text) || String(x.description).includes(text));
        this.setState({ filterResults: [...this.state.filterResults, ...filteredData], query: text })
        this.setState({ query: text })
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    }

    render() {

        const inp = React.createRef();

        return (
            <View style={theme.container}>
                <View style={theme.navigation_bar}>
                    <TouchableOpacity onPress={this.onPressBack}>
                        <IconBack width="32" height="32"></IconBack>
                    </TouchableOpacity>
                    <Text style={[theme.text_title]}>Ajuda</Text>
                    <View style={{ width: 35, height: 35 }}></View>
                </View>
                <ScrollView>
                    <View style={styles.page}>
                        <TouchableOpacity style={styles.search_bar} onPress={() => this.setState({ showSearch: true })}>
                            <TextInput
                                style={styles.search_bar_input}
                                placeholder="Qual é a sua dúvida?"
                                placeholderTextColor="#8189A1"
                                editable={false}
                            />
                            <Icon name="search" color={variables.primary} style={styles.search_bar_icon} />
                        </TouchableOpacity>
                        {this.state.results.map((item, index) => (
                            <TouchableOpacity onPress={() => this.setState({ showHelp: true })}>
                                <View style={styles.row}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={theme.title} numberOfLines={1}>{item.title}</Text>
                                        <Text style={{ fontSize: 13, color: variables.gray2 }} numberOfLines={1}>{item.description}</Text>
                                    </View>
                                    <View style={{ alignSelf: 'center', width: 32 }}>
                                        <Icon name="chevron-right" color={variables.accent} size={24} />
                                    </View>
                                </View>
                                <Divider style={styles.divider} />
                            </TouchableOpacity>
                        ))}
                    </View>
                </ScrollView>
                <SimpleBottomSheet visible={this.state.showHelp} onClose={() => this.setState({ showHelp: false })}>
                    <View>
                        <Text style={theme.title}>Lorem ipsum dolor sit amet, consectetur?</Text>
                        <Text style={styles.paragraph}>Lobortis sem faucibus et eget hendrerit nunc, aliquam iaculis. Ut magna dui massa congue interdum ligula. Tempus dignissim sit ultrices ultrices. Cum semper elit consequat est. Iaculis pulvinar facilisis vel enim. </Text>
                        <Text style={styles.paragraph}>Sodales morbi semper est dui diam eu morbi at nulla. Varius arcu, nunc aliquam elit urna, vestibulum arcu justo, tellus. Scelerisque suspendisse neque est metus, quam libero, amet. Bibendum ridiculus ullamcorper orci libero dolor placerat feugiat est. Gravida nulla tellus, in ultricies accumsan quis. A diam ullamcorper ultrices interdum ipsum quis.</Text>
                    </View>
                </SimpleBottomSheet>
                <Modal visible={this.state.showSearch} onShow={() => {
                    inp.current.blur()
                    inp.current.focus()
                }}>
                    <View style={theme.navigation_bar}>
                        <TouchableOpacity onPress={() => this.setState({ showSearch: false })}>
                            <IconBack2 width="32" height="32" />
                        </TouchableOpacity>
                        <TextInput
                            style={{ flexGrow: 1, height: 40, top: 3, position: 'relative', color: variables.light }}
                            placeholder="Qual é a sua dúvida?"
                            placeholderTextColor="#E0E1E8"

                            onChangeText={(text) => this.onChangeTextBar(text)}
                            value={this.state.query}
                            autoFocus
                            ref={inp}
                        />
                        {this.state.query.length > 0 && <Icon name="close" color={variables.light} onPress={() => this.setState({ query: '' })} />}
                    </View>
                    {this.state.query.length > 0 && this.state.filterResults.length > 0 && <ScrollView>
                        <View style={styles.page}>
                            {this.state.filterResults.map((item, index) => (
                                <TouchableOpacity onPress={() => this.setState({ showHelp: true })}>
                                    <View style={styles.row}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={theme.title} numberOfLines={1}>{item.title}</Text>
                                            <Text style={{ fontSize: 13, color: variables.gray2 }} numberOfLines={1}>{item.description}</Text>
                                        </View>
                                        <View style={{ alignSelf: 'center', width: 32 }}>
                                            <Icon name="chevron-right" color={variables.accent} size={24} />
                                        </View>
                                    </View>
                                    <Divider style={styles.divider} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </ScrollView>}
                    {this.state.filterResults.length == 0 && <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View style={{ alignItems: 'center' }}>
                            <IconNotFound style={{ marginBottom: 19 }} />
                            <Text style={{ color: variables.gray3, fontSize: 14 }}>Nenhum resultado encontrado</Text>
                        </View>
                    </View>}
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    //meus estilos
    page: {
        padding: variables.paddingHorizontal,
        flex: 1,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: variables.paddingHorizontal,
    },
    search_bar: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 12,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: hexToRGB(variables.secondary_light, 0.2),
    },
    search_bar_input: {
        height: 48,
        flexGrow: 1,
    },
    search_bar_icon: {
        flexShrink: 0,
    },
    divider: {
        backgroundColor: hexToRGB(variables.secondary_light, 0.1),
    },
    paragraph: {
        fontSize: 16,
        color: variables.gray2,
        marginVertical: 8,
    },
});