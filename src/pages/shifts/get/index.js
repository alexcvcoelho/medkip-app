import React, { useState, useEffect, useRef } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text } from 'react-native';

import { Divider } from 'react-native-elements';

import { theme, variables, hexToRGB } from '~/theme';

import Accordian from '~/components/accordian';

import IconBack from "~/assets/images/arrow-back.svg";
import IconCalendar from "~/assets/images/icon-calendar2.svg";

import IconEdit from "~/assets/images/edit.svg";


const index = ({ navigation, route }) => {
    return (
        <View style={theme.container}>
            <View style={theme.navigation_bar}>
                <IconBack width="32" height="32" onPress={() => navigation.goBack()}></IconBack>
                <Text style={[theme.text_title]}>Plantão A. Einstein</Text>
                <IconEdit onPress={() => navigation.push('ShiftSave', { id: 1 })} />
            </View>
            <ScrollView>
                <View style={styles.page}>
                    <Text style={theme.title}>Dados Gerais</Text>
                    <View style={styles.card}>
                        <View>
                            <Text style={styles.textfield_title}>Hospital</Text>
                            <View style={styles.textfield}>
                                <Text style={styles.textfield_input} numberOfLines={1}>Hospital Israelita Albert Einstein - São Paulo, SP</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Data de Entrada</Text>
                            <View style={styles.textfield}>
                                <Text style={styles.textfield_input} numberOfLines={1}>28/08/2020</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Horário de Entrada</Text>
                            <View style={styles.textfield}>
                                <Text style={styles.textfield_input} numberOfLines={1}>16:00</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Quantidade de Horas do Plantão</Text>
                            <View style={styles.textfield}>
                                <Text style={styles.textfield_input} numberOfLines={1}>12h</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Recorrência</Text>
                            <View style={[styles.textfield, { borderBottomWidth: 0 }]}>
                                <Text style={styles.textfield_input} numberOfLines={1}>Semanal | Ter, Qui</Text>
                            </View>
                        </View>
                        <Accordian
                            title={'Ver datas dos próximos plantões'}
                            headerStyles={{ height: null, }}
                        >
                            <View style={{ paddingTop: variables.paddingHorizontal }}>
                                <View style={{ flexDirection: 'row', marginBottom: variables.paddingHorizontal }}>
                                    <IconCalendar />
                                    <Text style={{ color: variables.gray2, marginLeft: 8 }}>Ter, 13/10/20</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginBottom: variables.paddingHorizontal }}>
                                    <IconCalendar />
                                    <Text style={{ color: variables.gray2, marginLeft: 8 }}>Ter, 13/10/20</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginBottom: variables.paddingHorizontal }}>
                                    <IconCalendar />
                                    <Text style={{ color: variables.gray2, marginLeft: 8 }}>Ter, 13/10/20</Text>
                                </View>
                            </View>
                        </Accordian>
                    </View>
                    <Text style={theme.title}>Dados Financeiros</Text>
                    <View style={styles.card}>
                        <View>
                            <Text style={styles.textfield_title}>Valor Bruto ou Total</Text>
                            <View style={styles.textfield}>
                                <Text style={[styles.textfield_input, { color: variables.gray2 }]} numberOfLines={1}>R$ 5.000,00 - R$ 350,00/h</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Regime de Pagamento</Text>
                            <View style={styles.textfield}>
                                <Text style={[styles.textfield_input, { color: variables.gray2 }]} numberOfLines={1}>Pessoa Física</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Data do Recebimento</Text>
                            <View style={[styles.textfield, { borderBottomWidth: 0 }]}>
                                <Text style={[styles.textfield_input, { color: variables.gray2 }]} numberOfLines={1}>Daqui 30 dias</Text>
                            </View>
                        </View>
                    </View>
                    <View style={[styles.card, { paddingHorizontal: 0 }]}>
                        <View style={{ paddingHorizontal: variables.paddingHorizontal }}>
                            <View style={[styles.textfield, { borderBottomWidth: 0 }]}>
                                <IconCalendar style={styles.textfield_icon} />
                                <Text style={theme.title}>Veja mais plantões realizados</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <Text style={{ color: variables.gray2, fontWeight: "500" }}>Plantão A</Text>
                            <Text style={{ color: variables.gray2 }}>10/08/20  | 18h - 20h</Text>
                        </View>
                        <Divider style={{ backgroundColor: hexToRGB(variables.secondary_light, 0.1) }} />
                        <View style={styles.row}>
                            <Text style={{ color: variables.gray2, fontWeight: "500" }}>Plantão B</Text>
                            <Text style={{ color: variables.gray2 }}>10/08/20  | 18h - 20h</Text>
                        </View>
                        <Divider style={{ backgroundColor: hexToRGB(variables.secondary_light, 0.1) }} />
                        <View style={styles.row}>
                            <Text style={{ color: variables.gray2, fontWeight: "500" }}>Plantão C</Text>
                            <Text style={{ color: variables.gray2 }}>10/08/20  | 18h - 20h</Text>
                        </View>
                        <Divider style={{ backgroundColor: hexToRGB(variables.secondary_light, 0.1) }} />
                        <View style={styles.row}>
                            <Text style={{ color: variables.gray2, fontWeight: "500" }}>Plantão D</Text>
                            <Text style={{ color: variables.gray2 }}>10/08/20  | 18h - 20h</Text>
                        </View>
                        <Divider style={{ backgroundColor: hexToRGB(variables.secondary_light, 0.1) }} />
                        <View style={styles.row}>
                            <View>
                                <Text style={{ color: variables.gray2 }}>Valor médio a receber</Text>
                                <Text style={{ color: variables.gray2 }}>30 dias</Text>
                            </View>
                            <Text style={{ color: variables.primary, fontWeight: "bold", fontSize: 16 }}>R$ 3.000,00</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    //meus estilos
    page: {
        padding: variables.paddingHorizontal,
        backgroundColor: '#F5F6FA',
        flex: 1,
    },
    card: {
        backgroundColor: variables.light,
        borderRadius: variables.borderRadius,
        padding: variables.paddingHorizontal,
        marginVertical: variables.paddingHorizontal,
    },
    textfield_title: {
        color: hexToRGB(variables.secondary, 0.5),
        fontSize: variables.fontSize,
        marginVertical: 5,
    },
    textfield: {
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: variables.light,
        borderBottomWidth: 1,
        borderColor: hexToRGB(variables.secondary_light, 0.1),
        // marginBottom: variables.paddingHorizontal,
        // height: 48,
    },
    textfield_input: {
        flexGrow: 1,
        color: variables.secondary,
        fontSize: 14,
        fontWeight: "bold",
        paddingBottom: 16,
    },
    textfield_icon: {
        flexShrink: 0,
        marginRight: 12,
    },
    textfield_paragraph: {
        marginTop: 8,
        borderRightWidth: 2,
        borderColor: variables.primary,
        paddingRight: 8,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: variables.paddingHorizontal,
    }
});

export default index
