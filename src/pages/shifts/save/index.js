import React, { Component, useState } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Animated, Image, Modal, Dimensions, ActivityIndicator } from 'react-native';

import { Button, Icon, Divider, CheckBox } from 'react-native-elements';

import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import CalendarPicker from 'react-native-calendar-picker';

import moment from 'moment';

import { theme, variables, hexToRGB } from '~/theme';

import FloatingInput from '~/components/floatingInput';
import Accordian from '~/components/accordian';
import { SimpleBottomSheet, SimpleModal, BottomSheetConfirm, BottomSheetSuccess } from '~/components/bottomSheets';

import Search from '~/components/search-hospital';

import IconBack from "~/assets/images/arrow-back.svg";
import IconCalendar from "~/assets/images/icon-calendar.svg";


import IconRadioActive from "~/assets/images/icon-radio-active.svg";
import IconRadioInactive from "~/assets/images/icon-radio-inactive.svg";

import IconShiftSuccess from "~/assets/images/icon-shift-success.svg";

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';
import { BottomSheetFailure } from '~/components/bottomSheets';
import { usePlayer } from '~/components/context';

export default class ShiftSave extends Component {
    //const { fileDescription, clearFile } = usePlayer();

    //const joao = fileDescription
    //clearFile()

    state = {
        isLoading: false,
        token: '',
        modalInfo: false,
        messageInfo: '',
        modalSuccess: false,

        titulo: '',
        data: '',
        hospital: '',
        hospitalId: null,
        horas: '',
        valor: '',
        tipo: '',
        data_recebimento: '',
        recorrencia: '',
        data_recorrencia: [
            { 'semana': 'Dom', 'ativo': false },
            { 'semana': 'Seg', 'ativo': false },
            { 'semana': 'Ter', 'ativo': false },
            { 'semana': 'Qua', 'ativo': false },
            { 'semana': 'Qui', 'ativo': false },
            { 'semana': 'Sex', 'ativo': false },
            { 'semana': 'Sab', 'ativo': false }],
        fixar_plantao: '',
        dataFormat: '',
        itemEdit: null,

        date: '',
        hour: '',
        minutes: '',
        checkDates: [''],
        modalDate: false,
        modalHospital: false,

        hours: [6, 12, 18, 36, 72],
        weeksNames: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab']
    }

    select = (e) => {
        //{"city": "Campinas", "id": "e93606ce-8e3e-497a-a734-9e2f76a99e16", "iss": 20, "name": "Hospital Exemplo ", "profileId": "113353595481056821876"}
        this.setState({ hospital: e.name, modalHospital: false, hospitalId: e.id })
    }

    onDateChange = (date) => {
        this.setState({ date: date });
    }

    customDayHeaderStylesCallback = ({ dayOfWeek, month, year }) => {
        return {
            textStyle: {
                fontFamily: variables.fontBold,
            }
        };
    }

    setInputDate = () => {
        this.setState({
            data: `${moment(this.state.date).format("DD/MM/YY")} | ${this.state.hour}h${this.state.minutes}`,
            modalDate: false,
            dataFormat: `${moment(this.state.date).format("YYYY-MM-DD")} ${this.state.hour}:${this.state.minutes}`
        })
    }


    fillForm = () => {
        if (this.state.itemEdit) {

            //console.log(this.state.itemEdit)
            /*{"amount": 2400, "date": "2021-05-12 18:00", 
            "hospital": {"city": "Votorantim - SP", "name": "Hospital Unimed"}, 
            "hospitalId": "a6c21066-b8ee-4839-97fe-d8f257e9eba2", 
            "hourlyAmount": 200, 
            "id": "ac50a509-e8e8-46b0-baf1-29e9e7600260", "title": "Unimed - Semanal | Qua e Quinta"} */

            // get all infos
            ServiceApi.getScheduleDetails(this.state.token, this.state.itemEdit.id, (response) => {

                // {
                //     "recurrenceType": 7,
                //     "createdAt": "2021-03-25 17:32:24",
                //     "amountOfHours": 12,
                //     "isMontlyFixedDay": false,
                //     "estimatedDaysForPayment": 30,
                //     "daysOfWeek": [
                //         4,
                //         5
                //     ],
                //     "hospital": {
                //         "name": "Hospital Unimed",
                //         "city": "Votorantim - SP"
                //     },
                //     "isActive": true,
                //     "paymentScheme": "CNPJ",
                //     "hospitalId": "a6c21066-b8ee-4839-97fe-d8f257e9eba2",
                //     "updatedAt": null,
                //     "startDate": "2021-03-25 18:00",
                //     "amount": 2400,
                //     "id": "ad2455fd-2530-4bda-a417-70a4e0ec66c0",
                //     "title": "Unimed - Semanal | Qua e Quinta",
                //     "profileId": "111822660129338094026",
                //     "nextOrderlies": [
                //         "2021-05-12 18:00",
                //         "2021-05-13 18:00",
                //         "2021-05-19 18:00",
                //         "2021-05-20 18:00"
                //     ],
                //     "previousOrderlies": [
                //         "2021-05-05 18:00",
                //         "2021-05-06 18:00"
                //     ]
                // }

                const {
                    recurrenceType,
                    amountOfHours,
                    isMontlyFixedDay,
                    estimatedDaysForPayment,
                    daysOfWeek,
                    title,
                    hospital,
                    hospitalId,
                    paymentScheme,
                    amount,
                    startDate,
                    id
                } = response;

                console.log('dias semana' + daysOfWeek);

                if (daysOfWeek != null) {
                    if (daysOfWeek.length > 0) {
                        for (let i = 0; i < daysOfWeek.length; i++) {
                            this.onChangeIndexWeekDay(daysOfWeek[i] - 1)
                        }
                    }
                }

                var recebimento = ''
                if (estimatedDaysForPayment == 2) {
                    recebimento = 'Daqui 2 dias'
                } else if (estimatedDaysForPayment == 30) {
                    recebimento = 'Daqui 30 dias'
                } else if (estimatedDaysForPayment == 60) {
                    recebimento = 'Daqui 60 dias'
                }

                var qtdHoras = ''
                if (amountOfHours == 6) {
                    qtdHoras = '6 horas'
                } else if (amountOfHours == 12) {
                    qtdHoras = '12 horas'
                } else if (amountOfHours == 18) {
                    qtdHoras = '18 horas'
                } else if (amountOfHours == 36) {
                    qtdHoras = '36 horas'
                } else if (amountOfHours == 72) {
                    qtdHoras = '72 horas'
                }

                let recorrencia = recurrenceType
                var tipoRecorrencia = ''
                if (recorrencia == 7) {
                    tipoRecorrencia = 'Semanal'
                } else if (recorrencia == 15) {
                    tipoRecorrencia = 'Quinzenal'
                } else if (recorrencia == 30) {
                    tipoRecorrencia = 'Mensal'
                }
                this.setState({
                    id: id,
                    titulo: title,
                    hospital: hospital.name,
                    hospitalId: hospitalId,
                    horas: qtdHoras,
                    valor: amount.toString(),
                    tipo: paymentScheme == 'CPF' ? 'Pessoa Física' : 'Pessoa Jurídica',
                    data_recebimento: recebimento,
                    recorrencia: tipoRecorrencia,
                    fixar_plantao: isMontlyFixedDay,
                    data: `${moment(this.state.startDate).format("DD/MM/YY")} | ${moment(this.state.startDate).format("HH")}h${moment(this.state.startDate).format("mm")}`,
                    dataFormat: startDate
                });
            }, (error) => {
                this.setState({
                    isLoading: false, modalInfo: true, messageInfo: error
                })
            })
        }
    }

    getToken = async () => {
        const idToken = await ManagerAuth.getToken();
        this.setState({ token: idToken.toString() })

        try {
            const { route } = this.props;
            if (route.params != null) {
                if (route.params.shift != null) {
                    const itemEdit = route.params.shift
                    if (itemEdit) {
                        console.log(itemEdit.id);
                    }
                    this.setState({ itemEdit: itemEdit })
                }
            }
        } finally {

            this.fillForm();
        }
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    checkDaysWeekSelected = () => {
        for (let i = 0; i < this.state.data_recorrencia.length; i++) {
            if (this.state.data_recorrencia[i].ativo) {
                return true
            }
        }
        return false
    }
    validadeFields = () => {

        if (this.state.titulo == '') {
            this.setState({ messageInfo: 'Campo título é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.hospitalId == null) {
            this.setState({ messageInfo: 'Campo hospital é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.horas == '') {
            this.setState({ messageInfo: 'Campo quantidade de horas é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.data_recebimento == '') {
            this.setState({ messageInfo: 'Campo data do recebimento é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.recorrencia == '') {
            this.setState({ messageInfo: 'Campo recorrência é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.recorrencia != 'Mensal' && !this.checkDaysWeekSelected()) {
            this.setState({ messageInfo: 'Selecione um dia da semana de recorrência', modalInfo: true })
            return false
        } else if (this.state.tipo == '') {
            this.setState({ messageInfo: 'Campo tipo de pessoa é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.valor == '') {
            this.setState({ messageInfo: 'Campo valor bruto é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.dataFormat == '') {
            this.setState({ messageInfo: 'Campo data de entrada é obrigatório!', modalInfo: true })
            return false
        }

        return true
    }

    onPressSaveData = () => {
        const result = this.validadeFields();

        //console.log(this.state.data_recorrencia)
        /*console.log(this.state.titulo + ' ' +
            this.state.hospitalId + ' ' +
            this.state.dataFormat + ' ' +
            this.state.valor + ' ' +
            this.state.tipo + ' ' +
            this.state.data_recebimento + ' ' +
            this.state.recorrencia + ' ' +
            this.state.fixar_plantao + ' ')*/

        if (!result) {
            return;
        }

        this.setState({ isLoading: true })

        let recebimento = this.state.data_recebimento
        var valor_recebido = 0
        if (recebimento == 'Daqui 2 dias') {
            valor_recebido = 2
        } else if (recebimento == 'Daqui 30 dias') {
            valor_recebido = 30
        } else if (recebimento == 'Daqui 60 dias') {
            valor_recebido = 60
        }

        let horas = this.state.horas
        var qtdHoras = 0
        if (horas == '6 horas') {
            qtdHoras = 6
        } else if (horas == '12 horas') {
            qtdHoras = 12
        } else if (horas == '18 horas') {
            qtdHoras = 18
        } else if (horas == '36 horas') {
            qtdHoras = 36
        } else if (horas == '72 horas') {
            qtdHoras = 72
        }

        let recorrencia = this.state.recorrencia
        var tipoRecorrencia = 0
        if (recorrencia == 'Semanal') {
            tipoRecorrencia = 7
        } else if (recorrencia == 'Quinzenal') {
            tipoRecorrencia = 15
        } else if (recorrencia == 'Mensal') {
            tipoRecorrencia = 30
        }

        var daysWeekSelected = []
        for (let i = 0; i < this.state.data_recorrencia.length; i++) {
            if (this.state.data_recorrencia[i].ativo == true) {
                daysWeekSelected.push(i + 1)
            }
        }

        console.log('days week: ' + daysWeekSelected)

        let id = this.state.itemEdit ? this.state.itemEdit.id : null

        console.log('meu token')
        console.log(this.state.token)
        ServiceApi.saveSchedule(this.state.token,
            id,
            this.state.hospitalId,
            this.state.titulo,
            this.state.dataFormat,
            qtdHoras,
            tipoRecorrencia,
            daysWeekSelected,
            this.state.fixar_plantao == '' ? false : true,
            parseInt(this.state.valor),
            this.state.tipo == 'Pessoa Física' ? 'CPF' : 'CNPJ',
            valor_recebido, () => {
                this.setState({ isLoading: false, modalSuccess: true })

            }, (error) => {
                this.setState({
                    isLoading: false, modalInfo: true, messageInfo: error
                })
            }
        )
    }

    onChangeIndexWeekDay = (index) => {
        // 1. Make a shallow copy of the items
        let items = [...this.state.data_recorrencia];
        // 2. Make a shallow copy of the item you want to mutate
        let item = { ...this.state.data_recorrencia[index] };
        // 3. Replace the property you're intested in
        item.ativo = !item.ativo;
        // 4. Put it back into our array. N.B. we *are* mutating the array here, but that's why we made a copy first
        items[index] = item;
        // 5. Set the state to our new copy
        this.setState({ items });
        this.setState({ data_recorrencia: items })
    }

    render() {
        const { navigation } = this.props;

        return (
            <MenuProvider>
                <View style={theme.container}>
                    <View style={theme.navigation_bar}>
                        <IconBack width="32" height="32" onPress={() => navigation.goBack()}></IconBack>
                        <Text style={[theme.text_title]}>Adicionar Plantão</Text>
                        <View style={{ width: 30, height: 30 }} />
                    </View>
                    <ScrollView>
                        <View style={styles.page}>
                            <Text style={theme.title}>Dados Gerais</Text>
                            <View style={{ height: variables.paddingHorizontal }} />
                            <FloatingInput
                                label="Título do Plantão"
                                isMask={false}
                                value={this.state.titulo}
                                onChangeText={(text) => this.setState({ titulo: text })}
                            />

                            <TouchableOpacity style={styles.textfield} onPress={() => this.setState({ modalHospital: true })}>
                                <TextInput
                                    placeholder="Selecione o Hospital"
                                    placeholderTextColor={variables.gray3}
                                    editable={false}
                                    value={this.state.hospital}
                                    style={{ color: variables.secondary }}
                                />
                                <Icon name="chevron-down" type="feather" color={variables.accent} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.setState({ modalDate: true })}>
                                <FloatingInput
                                    label="Data e Horário de Entrada"
                                    isMask={false}
                                    value={this.state.data}
                                    editable={false}
                                    icon={<IconCalendar />}
                                />
                            </TouchableOpacity>
                            <Menu onSelect={value => this.setState({ horas: value })}>
                                <MenuTrigger>
                                    <View style={styles.textfield}>
                                        <TextInput
                                            placeholder="Quantidade de Horas do Plantão"
                                            placeholderTextColor={variables.gray3}
                                            editable={false}
                                            value={this.state.horas}
                                            style={{ color: variables.secondary }}
                                        />
                                        <Icon name="chevron-down" type="feather" color={variables.accent} />
                                    </View>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={{ marginTop: 50 }} customStyles={menu}>
                                    {this.state.hours.map((hour, index) => (<MenuOption key={index} value={`${hour} horas`}>
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>{`${hour} horas`}</Text>
                                        </View>
                                    </MenuOption>))}
                                </MenuOptions>
                            </Menu>
                            <View
                                style={{
                                    backgroundColor: '#F5F6FA',
                                    borderRadius: variables.borderRadius,
                                    paddingHorizontal: variables.paddingHorizontal
                                }}>
                                <View style={[styles.header, { marginTop: variables.paddingHorizontal }]}>
                                    <Text style={theme.title}>Recorrência</Text>
                                    <TouchableOpacity onPress={() => this.setState({ recorrencia: this.state.recorrencia === '' ? 'Semanal' : '' })} style={this.state.recorrencia ? switchCss.boxPrimary : switchCss.boxSecondary}>
                                        <View style={this.state.recorrencia ? switchCss.circleLight : switchCss.circle}></View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <CheckBox
                                        title='Semanal'
                                        containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, paddingHorizontal: 0, marginLeft: 0, marginRight: 5 }}
                                        textStyle={{ color: variables.gray2, fontWeight: this.state.recorrencia == 'Semanal' ? 'bold' : 'normal' }}
                                        checkedIcon={<IconRadioActive />}
                                        uncheckedIcon={<IconRadioInactive />}
                                        checked={this.state.recorrencia == 'Semanal'}
                                        onPress={() => this.setState({ recorrencia: 'Semanal' })}
                                    />
                                    <CheckBox
                                        title='Quinzenal'
                                        containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, paddingHorizontal: 0, marginLeft: 0, marginRight: 5 }}
                                        textStyle={{ color: variables.gray2, fontWeight: this.state.recorrencia == 'Quinzenal' ? 'bold' : 'normal' }}
                                        checkedIcon={<IconRadioActive />}
                                        uncheckedIcon={<IconRadioInactive />}
                                        checked={this.state.recorrencia == 'Quinzenal'}
                                        onPress={() => this.setState({ recorrencia: 'Quinzenal' })}
                                    />
                                    <CheckBox
                                        title='Mensal'
                                        containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, paddingHorizontal: 0, marginLeft: 0, marginRight: 0 }}
                                        textStyle={{ color: variables.gray2, fontWeight: this.state.recorrencia == 'Mensal' ? 'bold' : 'normal' }}
                                        checkedIcon={<IconRadioActive />}
                                        uncheckedIcon={<IconRadioInactive />}
                                        checked={this.state.recorrencia == 'Mensal'}
                                        onPress={() => this.setState({ recorrencia: 'Mensal' })}
                                    />
                                </View>
                                {(this.state.recorrencia === 'Mensal') &&
                                    <View>
                                        <Text style={theme.title}>Fixar plantão</Text>
                                        <View style={styles.days_box}>
                                            <CheckBox
                                                title='Todo dia 13'
                                                containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, paddingHorizontal: 0, marginLeft: 0, marginRight: 5 }}
                                                textStyle={{ color: variables.gray2, fontWeight: this.state.fixar_plantao == 'Todo dia 13' ? 'bold' : 'normal' }}
                                                checkedIcon={<IconRadioActive />}
                                                uncheckedIcon={<IconRadioInactive />}
                                                checked={this.state.fixar_plantao == 'Todo dia 13'}
                                                onPress={() => this.setState({ fixar_plantao: 'Todo dia 13' })}
                                            />
                                            <CheckBox
                                                title='Toda 2ª terça do mês'
                                                containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, paddingHorizontal: 0, marginLeft: 0, marginRight: 0 }}
                                                textStyle={{ color: variables.gray2, fontWeight: this.state.fixar_plantao == 'Toda 2ª terça do mês' ? 'bold' : 'normal' }}
                                                checkedIcon={<IconRadioActive />}
                                                uncheckedIcon={<IconRadioInactive />}
                                                checked={this.state.fixar_plantao == 'Toda 2ª terça do mês'}
                                                onPress={() => this.setState({ fixar_plantao: 'Toda 2ª terça do mês' })}
                                            />
                                        </View>
                                    </View>}
                                {(this.state.recorrencia !== '' && this.state.recorrencia !== 'Mensal') && <View style={styles.days_box}>
                                    {this.state.weeksNames.map((day, index) => (
                                        <TouchableOpacity key={index} style={[styles.day, this.state.data_recorrencia[index].ativo ? styles.day_active : {}]} onPress={() => this.onChangeIndexWeekDay(index)}>
                                            <Text style={[styles.day_txt, this.state.data_recorrencia[index].ativo ? styles.day_txt_active : {}]}>{day.split("")[0]}</Text>
                                        </TouchableOpacity>
                                    ))}
                                </View>}

                                {/* {(form.data_recorrencia.length > 0 || form.fixar_plantao !== '') && <><Divider />
                                <Accordian
                                    title={'Ver datas dos próximos plantões'}
                                >
                                    <View>
                                        <View style={{ flexDirection: 'row', marginBottom: variables.paddingHorizontal }}>
                                            <IconCalendar />
                                            <Text style={{ color: variables.gray2, marginLeft: 8 }}>Ter, 13/10/20</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginBottom: variables.paddingHorizontal }}>
                                            <IconCalendar />
                                            <Text style={{ color: variables.gray2, marginLeft: 8 }}>Ter, 13/10/20</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginBottom: variables.paddingHorizontal }}>
                                            <IconCalendar />
                                            <Text style={{ color: variables.gray2, marginLeft: 8 }}>Ter, 13/10/20</Text>
                                        </View>
                                    </View>
                                </Accordian>
                            </>
                            } */}
                            </View>
                            <View style={{ height: variables.paddingHorizontal }} />
                            <Text style={theme.title}>Dados Financeiros</Text>
                            <View style={{ height: variables.paddingHorizontal }} />
                            <FloatingInput
                                label="Valor Bruto ou Total"
                                value={this.state.valor}
                                isMask={false}
                                onChangeText={(text) => this.setState({ valor: text })}
                                keyboardType="decimal-pad"

                            />
                            <Text style={{ color: hexToRGB(variables.secondary, 0.7) }}>Regime de Pagamento</Text>
                            <View style={{ height: variables.paddingHorizontal / 2 }} />
                            <View style={{ flexDirection: 'row' }}>
                                <CheckBox
                                    title='Pessoa Física'
                                    containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, flex: 0.5, paddingHorizontal: 0, marginLeft: 0 }}
                                    textStyle={{ color: variables.gray2, fontWeight: this.state.tipo == 'Pessoa Física' ? 'bold' : 'normal' }}
                                    checkedIcon={<IconRadioActive />}
                                    uncheckedIcon={<IconRadioInactive />}
                                    checked={this.state.tipo == 'Pessoa Física'}
                                    onPress={() => this.setState({ tipo: 'Pessoa Física' })}
                                />
                                <CheckBox
                                    title='Pessoa Jurídica'
                                    containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, flex: 0.5, paddingHorizontal: 0, marginRight: 0 }}
                                    textStyle={{ color: variables.gray2, fontWeight: this.state.tipo == 'Pessoa Jurídica' ? 'bold' : 'normal' }}
                                    checkedIcon={<IconRadioActive />}
                                    uncheckedIcon={<IconRadioInactive />}
                                    checked={this.state.tipo == 'Pessoa Jurídica'}
                                    onPress={() => this.setState({ tipo: 'Pessoa Jurídica' })}
                                />
                            </View>
                            <View style={{ height: variables.paddingHorizontal / 2 }} />
                            <Menu onSelect={value => this.setState({ data_recebimento: value })}>
                                <MenuTrigger>
                                    <View style={styles.textfield}>
                                        <TextInput
                                            placeholder="Data do Recebimento"
                                            placeholderTextColor={variables.gray3}
                                            editable={false}
                                            value={this.state.data_recebimento}
                                            style={{ color: variables.secondary }}
                                        />
                                        <Icon name="chevron-down" type="feather" color={variables.accent} />
                                    </View>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={{ marginTop: 50 }} customStyles={menu}>
                                    <MenuOption value="Daqui 2 dias">
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>Daqui 2 dias</Text>
                                        </View>
                                    </MenuOption>
                                    <MenuOption value="Daqui 30 dias">
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>Daqui 30 dias</Text>
                                        </View>
                                    </MenuOption>
                                    <MenuOption value="Daqui 60 dias">
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>Daqui 60 dias</Text>
                                        </View>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>
                            <TouchableOpacity style={styles.button_continue} onPress={() => this.onPressSaveData()}>
                                {this.state.isLoading ?
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                        <ActivityIndicator size="large" color="#4493FF" />
                                    </View>
                                    :
                                    <Text style={styles.text_style_bold}>{this.state.itemEdit != null ? 'Salvar alterações' : 'Salvar'}</Text>
                                }
                            </TouchableOpacity>

                        </View>
                    </ScrollView>
                </View>
                <Modal visible={this.state.modalHospital}>
                    <Search onClose={() => this.setState({ modalHospital: false })} onSelect={(e) => this.select(e)} isSelected={this.state.hospital} />
                </Modal>
                <SimpleBottomSheet
                    visible={this.state.modalSuccess}
                    onClose={() => this.setState({ modalSuccess: false })}
                >
                    <View style={{ alignItems: 'center' }}>
                        <IconShiftSuccess />
                        <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Plantão salvo com sucesso!'}</Text>
                    </View>
                    <Button
                        title="Ver Plantão"
                        disabled={false}
                        buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                        titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                        onPress={() => { this.setState({ modalSuccess: false }); this.props.navigation.goBack() }}
                    />
                    <View style={{ height: variables.paddingHorizontal }} />
                    <Button
                        title="Voltar"
                        disabled={false}
                        buttonStyle={{ backgroundColor: variables.light, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight, borderWidth: 1 }}
                        titleStyle={{ color: variables.primary, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                        onPress={() => this.setState({ modalSuccess: false })}
                    />
                </SimpleBottomSheet>
                <SimpleBottomSheet visible={this.state.modalDate}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Text style={theme.title}>Selecione a Data de Entrada</Text>
                        <Icon name="close" color={variables.accent} onPress={() => this.setState({ modalDate: false })} />
                    </View>
                    <View style={{ marginVertical: variables.paddingHorizontal }}>
                        <CalendarPicker
                            onDateChange={this.onDateChange}
                            allowRangeSelection={false}
                            weekdays={['S', 'T', 'Q', 'Q', 'S', 'S', 'D']}
                            months={['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']}
                            headerWrapperStyle={{ backgroundColor: '#f7f7f9', width: 'auto', alignSelf: 'stretch', borderRadius: variables.borderRadius, padding: variables.paddingHorizontal, paddingBottom: variables.paddingHorizontal }}
                            previousComponent={<Icon name="chevron-left" color={variables.primary} />}
                            nextComponent={<Icon name="chevron-right" color={variables.primary} />}
                            todayBackgroundColor={variables.gray2}
                            selectedDayStyle={styles.selectedRangeStartStyle}
                            selectedDayTextStyle={{ color: variables.light }}
                            dayLabelsWrapper={{ borderTopWidth: 0, borderBottomWidth: 0 }}
                            customDayHeaderStyles={this.customDayHeaderStylesCallback}
                            textStyle={{
                                fontFamily: variables.fontFamily,
                                color: variables.secondary,
                            }}
                            monthTitleStyle={{ fontFamily: variables.fontBold }}
                            yearTitleStyle={{ fontFamily: variables.fontBold }}
                        />
                    </View>
                    <Text style={theme.title}>Indique a Hora de Entrada</Text>
                    <View style={{ height: variables.paddingHorizontal }} />
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <View style={[styles.textfield, { flex: 1 / 2, marginBottom: 0, paddingHorizontal: 0 }]}>
                            <TextInput
                                placeholder="Hora"
                                placeholderTextColor={variables.gray3}
                                value={this.state.hour}
                                style={{ color: variables.secondary, flex: 1, paddingHorizontal: variables.paddingHorizontal }}
                                onChangeText={(text) => this.setState({ hour: text })}
                                keyboardType="decimal-pad"
                            />
                        </View>
                        <Text style={{
                            fontSize: 20,
                            color: variables.gray3,
                            width: 10,
                            textAlign: 'center',
                        }}>:</Text>
                        <View style={[styles.textfield, { flex: 1 / 2, marginBottom: 0, paddingHorizontal: 0 }]}>
                            <TextInput
                                placeholder="Minuto"
                                placeholderTextColor={variables.gray3}
                                value={this.state.minutes}
                                style={{ color: variables.secondary, flex: 1, paddingHorizontal: variables.paddingHorizontal }}
                                onChangeText={(text) => this.setState({ minutes: text })}
                                keyboardType="decimal-pad"
                            />
                        </View>
                    </View>
                    <View style={{ height: variables.paddingHorizontal }} />
                    <Button
                        title="Aplicar"
                        disabled={!this.state.hour || !this.state.minutes || !this.state.date}
                        buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                        titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                        disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                        disabledTitleStyle={{ color: variables.light }}
                        onPress={() => this.setInputDate()}
                    />
                </SimpleBottomSheet>

                <BottomSheetFailure
                    visible={this.state.modalInfo}
                    text={this.state.messageInfo}
                    onClose={() => { this.setState({ modalInfo: false }); }}
                />
            </MenuProvider >
        )
    }
}

const windowWidth = Dimensions.get('window').width - variables.paddingHorizontal * 2;

const menu = {
    optionsContainer: {
        padding: 0,
        width: windowWidth,
    },
    optionWrapper: {
        margin: 0,
        padding: 0,
    },
}

const styles = StyleSheet.create({
    //meus estilos
    page: {
        // flex: 1,
        padding: variables.paddingHorizontal,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 12
    },
    textfield: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: variables.paddingHorizontal,
        borderRadius: variables.borderRadius,
        backgroundColor: variables.light,
        borderWidth: 1,
        borderColor: '#C0C4D0',
        marginBottom: variables.paddingHorizontal,
        position: 'relative',
    },
    dropdown_item: {
        padding: variables.paddingHorizontal,
        backgroundColor: variables.light,
    },
    dropdown_item_txt: {
        fontSize: variables.fontSize,
        color: variables.secondary,
    },
    paragraph: {
        fontSize: variables.fontSize,
        color: variables.gray2,
        marginVertical: 8,
    },
    selectedRangeStartStyle: {
        backgroundColor: variables.primary,
        borderRadius: 2,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 4,

        elevation: 3,
    },
    days_box: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: variables.paddingHorizontal,
    },
    day: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        borderWidth: 1,
        borderColor: variables.gray3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    day_txt: {
        fontSize: 14,
        color: variables.gray3,
    },
    day_active: {
        backgroundColor: hexToRGB(variables.accent, 0.2),
        borderColor: hexToRGB(variables.accent, 0.2),
    },
    day_txt_active: {
        color: variables.primary,
    },
    button_continue: {
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: '#006CFF',
        height: 48,

        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 16,
        marginRight: 16
    },
    text_style_bold: {
        color: '#ffffff',
        fontSize: 14,
        marginLeft: 24,
        fontFamily: 'Montserrat-SemiBold'
    },
})

const switchCss = StyleSheet.create({
    boxPrimary: {
        position: 'relative',
        width: 40,
        height: 20,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: variables.primary,
        backgroundColor: variables.primary,
        justifyContent: 'center',
    },
    boxSecondary: {
        position: 'relative',
        width: 40,
        height: 20,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: '#C0C3D0',
        backgroundColor: 'transparent',
        justifyContent: 'center',
    },
    circleLight: {
        width: 14,
        height: 14,
        borderRadius: 16,
        backgroundColor: variables.light,
        left: 40 - 14 - 3
    },
    circle: {
        width: 14,
        height: 14,
        borderRadius: 16,
        backgroundColor: '#C0C3D0',
        left: 3,
    },
});