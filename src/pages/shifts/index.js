import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Modal, Image, ActivityIndicator } from 'react-native';

import { Card, ListItem, Button, Icon, Badge, Input, BottomSheet } from 'react-native-elements';

import { theme, variables, hexToRGB } from '~/theme';

import IconMenu from "~/assets/images/menu.svg";
import IconFilter from "~/assets/images/filter.svg";
import IconCalendar from "~/assets/images/icon-calendar.svg";
import IconHospital from "~/assets/images/icon-hospital2.svg";
import IconMoney from "~/assets/images/icon-money.svg";
import IconPath from "~/assets/images/icon-shift-notfound.svg";
import IconCloseChip from "~/assets/images/icon-close-chip.svg";

import Filter from "~/components/bottomSheets/filter";

import moment from 'moment';

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';

import { BottomSheetFailure } from '~/components/bottomSheets';

import { MaskService } from 'react-native-masked-text';

export default class Shifts extends Component {
    state = {
        token: '',
        modalVisible: false,
        openFilter: false,
        filters: {
            periodo: '',
            hospital: ''
        },
        dates: [],
        selectedDay: 0,
        weeksNames: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        schedules: [],
        isLoading: true,
        modalInfo: false,
        messageInfo: '',
        myHospitals: [],
    };

    convertMoney = (value) => {
        let money = MaskService.toMask('money', value, {
            unit: 'R$ ',
            separator: ',',
            delimiter: '.'
        })

        return money;
    }

    getHospitals = () => {
        ServiceApi.getHospitals(this.state.token, (response) => {
            this.setState({ myHospitals: response, isLoading: false })
        }, (error) => {
            this.setState({
                isLoading: false, modalInfo: true, messageInfo: error
            })
        }, this.props);
    }

    getShifts = (initDate, endDate) => {

        ServiceApi.getSchedules(this.state.token, initDate, endDate, (response) => {

            // filter by hospitals local
            var filterList = []
            //console.log('testa entrada hospital:' + this.state.filters.hospital)
            if (this.state.filters.hospital != '') {
                filterList = response.filter(x => String(x.hospital.name).toLowerCase() == String(this.state.filters.hospital).toLowerCase())
            } else {
                filterList = response
            }

            this.setState({ schedules: filterList })
            this.getHospitals();
        }, (error) => {
            this.setState({
                isLoading: false, modalInfo: true, messageInfo: error
            })
        }, this.props);
    }

    getToken = async () => {
        this.setState({ isLoading: true })

        const idToken = await ManagerAuth.getToken();

        console.log(idToken.toString())
        this.setState({ token: idToken.toString() })

        var currentDate = moment().format('YYYY-MM-DD');
        //var weekStart = currentDate.clone().startOf('week');
        //let selectedDate = moment(weekStart.format('YYYY-MM-DD')
        this.getShifts(currentDate, currentDate);
    }

    componentDidMount() {
        var currentDate = moment();
        var weekStart = currentDate.clone().startOf('week');

        var days = [];

        var today = moment().format("DD")
        for (let i = 0; i <= 6; i++) {
            let someDay = moment(weekStart).add(i, 'days').format("DD")
            days.push(someDay);
            if (today == someDay) {
                this.setState({ selectedDay: i })
            }
        };

        this.setState({
            dates: days,
        })

        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.setState({ filters: { periodo: '', hospital: '' } })
            this.getToken();
        });
    }

    onSelectDateHeader = (day, index) => {
        this.setState({ selectedDay: index })

        var currentDate = moment();
        var weekStart = currentDate.clone().startOf('week');
        let selectedDate = moment(weekStart).add(index, 'days').format('YYYY-MM-DD')
        this.getShifts(selectedDate, selectedDate)
    }

    render() {
        const navigation = this.props.navigation
        //const items = new Array(5).fill(null);
        // const items = [];
        const closeFilter = () => {
            this.setState({ openFilter: false })
        }

        const filter = (e) => {
            this.setState({ openFilter: false, filters: e })
            // quando o filtro é aplicado passa por este metodo
            //e.periodo.
            console.log('periodo: ' + e.periodo)
            if (e.periodo.length > 0) {
                var array = e.periodo.toString().split(' ')
                //console.log(e.periodo + ' ' + e.hospital)

                var initDate = ''
                var endDate = ''
                console.log(array[0])
                console.log(array[2])
                if (array.length > 0) {
                    initDate = moment(array[0], 'DD/MM/YY').format('YYYY-MM-DD')
                    endDate = moment(array[2], 'DD/MM/YY').format('YYYY-MM-DD')
                }

                //console.log(this.state.selectedDay)
                console.log(initDate + ' ' + endDate)

                if (initDate.length == 0 || endDate.length == 0) {
                    var currentDate = moment();
                    var weekStart = currentDate.clone().startOf('week');
                    let selectedDate = moment(weekStart).add(this.state.selectedDay, 'days').format('YYYY-MM-DD')
                    console.log(selectedDate)

                    this.getShifts(selectedDate, selectedDate)

                } else {
                    this.getShifts(initDate, endDate)
                }
            } else {
                var currentDate = moment();
                var weekStart = currentDate.clone().startOf('week');
                let selectedDate = moment(weekStart).add(this.state.selectedDay, 'days').format('YYYY-MM-DD')
                console.log(selectedDate)

                this.getShifts(selectedDate, selectedDate)
            }
        }

        const changeFilters = (key, value) => {
            const data = { ...this.state.filters };
            data[key] = value;
            //console.log('oi' + data)
            this.setState({ filters: data });
        };

        return (
            <View style={theme.container}>
                <View style={theme.navigation_bar}>
                    <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <IconMenu width="32" height="32"></IconMenu>
                    </TouchableOpacity>
                    <Text style={[theme.text_title]}>Meus Plantões</Text>
                    <TouchableOpacity onPress={() => this.setState({ openFilter: true })} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <IconFilter width="32" height="32" style={{ position: 'relative', top: 4 }}></IconFilter>
                    </TouchableOpacity>
                </View>
                {this.state.isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#4493FF" />
                    </View>
                    :
                    <View style={{ flex: 1 }}>
                        {this.state.schedules.length > 0 && <View style={styles.page}>
                            <ScrollView>
                                <View style={styles.date_header}>
                                    {this.state.dates.map((date, index) => (
                                        <TouchableOpacity
                                            key={index}
                                            onPress={() => this.onSelectDateHeader(date, index)}
                                            style={[styles.date_box, index == this.state.selectedDay ? { backgroundColor: variables.light } : {}]}>
                                            <Text style={styles.day}>{this.state.weeksNames[index]}</Text>
                                            <Text style={[styles.date, index == this.state.selectedDay ? { color: variables.secondary_dark } : {}]}>{date}</Text>
                                        </TouchableOpacity>
                                    ))}
                                </View>
                                <View style={{ paddingHorizontal: variables.paddingHorizontal }}>
                                    {(this.state.filters.periodo !== '' || this.state.filters.hospital !== '') &&
                                        <View style={theme.chips}>
                                            {(this.state.filters.periodo !== '') && <TouchableOpacity onPress={() => changeFilters('periodo', '')} style={theme.chip}>
                                                <Text style={theme.chip_txt}>{this.state.filters.periodo}</Text>
                                                <IconCloseChip />
                                            </TouchableOpacity>}
                                            {(this.state.filters.hospital !== '') && <TouchableOpacity onPress={() => changeFilters('hospital', '')} style={[theme.chip, { marginRight: 0 }]}>
                                                <Text style={theme.chip_txt} numberOfLines={1}>{this.state.filters.hospital}</Text>
                                                <View>
                                                    <IconCloseChip />
                                                </View>
                                            </TouchableOpacity>}
                                        </View>}
                                    {this.state.schedules.map((e, index) => (
                                        <TouchableOpacity key={e.id} onPress={() => navigation.navigate('ShiftSave', { shift: e })} style={{ marginBottom: index === this.state.schedules.length - 1 ? 76 : 0 }} key={index}>
                                            <Card key={e.id} containerStyle={{ borderWidth: 0, borderRadius: 6, elevation: 0, marginLeft: 0, marginRight: 0 }}>
                                                <View style={styles.card_header}>
                                                    <Text style={styles.card_title}>{e.title}</Text>
                                                </View>
                                                <Card.Divider />
                                                <View style={styles.card_grid}>
                                                    <View style={styles.card_col}>
                                                        <View style={styles.card_icon}>
                                                            <IconHospital />
                                                        </View>
                                                        <View style={{ paddingRight: variables.paddingHorizontal * 2, }}>
                                                            <Text style={styles.text} numberOfLines={1}>{e.hospital.name}</Text>
                                                            <Text style={styles.text}>{e.hospital.city}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={[styles.card_col, { flex: 1 / 3, paddingRight: variables.paddingHorizontal }]}>
                                                        <View style={styles.card_icon}>
                                                            <IconCalendar />
                                                        </View>
                                                        <Text style={styles.text}>{moment(e.date).format('DD/MM')} | {moment(e.date).format('HH:mm')}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles.card_grid}>
                                                    <View style={styles.card_col}>
                                                        <View style={styles.card_icon}>
                                                            <IconMoney />
                                                        </View>
                                                        <View>
                                                            <Text style={styles.text} numberOfLines={1}>Valor Líquido:</Text>
                                                            <Text style={styles.text}>{this.convertMoney(e.amount)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={[styles.card_col, { flex: 1 / 3, paddingRight: variables.paddingHorizontal }]}>
                                                        <View style={styles.card_icon}>
                                                            <IconMoney />
                                                        </View>
                                                        <View>
                                                            <Text style={styles.text} numberOfLines={1}>Valor/Hora:</Text>
                                                            <Text style={styles.text}>{this.convertMoney(e.hourlyAmount)}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </Card>
                                        </TouchableOpacity>
                                    ))}
                                </View>
                            </ScrollView>
                            <View style={[theme.fab_button_container, { backgroundColor: 'transparent' }]}>
                                <TouchableOpacity style={theme.fab_button} onPress={() => navigation.push('ShiftSave')}>
                                    <Icon name="plus" type="feather" color="#ffffff" />
                                </TouchableOpacity>
                            </View>
                        </View>}

                        {this.state.schedules.length == 0 && <View style={styles.page}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', flexGrow: 1 }}>
                                <IconPath width="48" height="36" style={{ marginBottom: 19 }} />
                                <Text style={{ color: variables.gray3, fontSize: 14 }}>Nenhum plantão encontrado</Text>
                            </View>
                            <View style={theme.fab_button_container}>
                                <Text style={theme.fab_button_hint}>{'Você pode adicionar um novo \nplantão clicando aqui'}</Text>
                                <TouchableOpacity style={theme.fab_button} onPress={() => navigation.push('ShiftSave')}>
                                    <Icon name="plus" type="feather" color="#ffffff" />
                                </TouchableOpacity>
                            </View>
                        </View>}

                        <Filter
                            visible={this.state.openFilter}
                            onClose={() => closeFilter()}
                            onOpen={() => this.setState({ openFilter: true })}
                            onFilter={(e) => filter(e)}
                            hospitals={this.state.myHospitals}
                        />
                        <BottomSheetFailure
                            visible={this.state.modalInfo}
                            text={this.state.messageInfo}
                            onClose={() => { this.setState({ modalInfo: false }); }}
                        />
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    //meus estilos
    page: {
        flex: 1,
        backgroundColor: '#F5F6FA',
    },
    date_header: {
        height: 97,
        backgroundColor: '#1E263D',
        justifyContent: 'space-between',
        padding: variables.paddingHorizontal,
        flexDirection: 'row',
    },
    date_box: {
        flex: 1 / 7,
        // backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
    },
    day: {
        fontSize: 10,
        color: variables.gray3,
    },
    date: {
        fontSize: 16,
        fontFamily: variables.fontBold,
        // color: variables.secondary_dark,
        color: variables.light,
        marginTop: 11,
    },
    card_header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: variables.paddingVertical
    },
    card_title: {
        color: variables.secondary,
        fontFamily: variables.fontBold,
        fontSize: variables.fontSize,
        margin: 0,
    },
    card_grid: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginTop: variables.paddingVertical,
    },
    card_col: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 2 / 3,
    },
    card_icon: {
        marginRight: 8,
        alignSelf: 'flex-start',
        marginTop: 5,
    },
    text: {
        color: variables.gray2,
        fontSize: 14,
    },
});