import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, ImageBackground, Alert } from 'react-native';

import IconGoogle from "~/assets/images/google.svg";
import IconApple from "~/assets/images/apple.svg";
import LogoMedkipWhte from "~/assets/images/logo-medkip-white.svg";

import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
export default class Login extends Component {

  onPressGoogleLogin = async () => {
    // userInfo.idToken -> necessario para fazer fazer os requests
    // userInfo.user -> user infos googles

    try {
      await GoogleSignin.hasPlayServices();
      const { idToken } = await GoogleSignin.signIn();

      console.log('token google = ' + idToken);

      this.props.navigation.navigate('UseConditionTerms');

      // this.props.navigation.navigate('DrawerNavigator');

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        // Works on both Android and iOS
        Alert.alert(
          'SIGN_IN_CANCELLED',
          '',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') }
          ],
          { cancelable: false }
        );
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        // Works on both Android and iOS
        Alert.alert(
          'PLAY_SERVICES_NOT_AVAILABLE',
          '',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') }
          ],
          { cancelable: false }
        );

      } else {
        // some other error happened
        // Works on both Android and iOS
        Alert.alert(
          'Erro',
          error,
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') }
          ],
          { cancelable: false }
        );
      }
    }
  };


  onPressAppleLogin = () => {

  }

  render() {
    return (
      <View style={styles.container}>

        <LogoMedkipWhte width="113" height="28"></LogoMedkipWhte>

        <ImageBackground style={styles.background_style} source={require('~/assets/images/bg-login.png')}
          resizeMode='cover'>
          <View style={styles.center_container}>
            <Text style={styles.text_title_style}>Seja bem-vindo!</Text>
            <Text style={styles.text_sub_style}>Faça seu login utilizando</Text>
          </View>
        </ImageBackground>

        <TouchableOpacity style={styles.button_style} onPress={this.onPressGoogleLogin}>
          <Text style={styles.text_botton_style}>Continue com Google</Text>
          <IconGoogle width="18" height="18" style={styles.icon_style} />
        </TouchableOpacity>

        <TouchableOpacity style={[styles.button_style, styles.marginTop]} onPress={this.onPressAppleLogin}>
          <Text style={styles.text_botton_style}>Continue com Apple</Text>
          <IconApple width="18" height="25" style={styles.icon_style} />
        </TouchableOpacity>

        <Text style={styles.footer_text}>© Medkip 2020</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#202945',
    alignItems: 'center',
    justifyContent: 'center'
  },
  background_style: {
    height: 400,
    width: 400,
  },
  center_container: {
    position: 'absolute',
    width: "100%",
    bottom: 0
  },
  text_title_style: {
    fontSize: 32,
    color: '#ffffff',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center'
  },
  text_sub_style: {
    fontSize: 16,
    color: '#ffffff',
    opacity: 0.8,
    marginTop: 16,
    fontFamily: 'Montserrat-Regular',
    textAlign: 'center'
  },
  button_style: {
    backgroundColor: 'rgba(46, 55, 84, 0.8)',
    borderRadius: 8,
    width: '80%',
    height: 56,
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row-reverse'
  },
  text_botton_style: {
    fontSize: 16,
    color: '#ffffff',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat-SemiBold'
  },
  footer_text: {
    fontSize: 12,
    color: '#ffffff',
    opacity: 0.4,
    marginTop: 30,
    fontFamily: 'Montserrat-Regular',
    textAlign: 'center'
  },
  marginTop: {
    marginTop: 16
  }
});