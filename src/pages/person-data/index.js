import React, { Component, useState, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native';

import { Avatar } from 'react-native-elements'

import IconBack from "~/assets/images/arrow-back.svg";
import IconEdit from "~/assets/images/edit.svg";

import IconCamera from "~/assets/images/camera.svg";

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';
import { BottomSheetFailure } from '~/components/bottomSheets';
import { set } from 'react-native-reanimated';

const PersonData = ({ navigation }) => {
    const [form, setForm] = useState({
        name: '',
        address: '',
        bio: '',
        university: '',
        crm: '',
        speciality: '',
        avatar: '',
        initials: '',
    })
    const [token, setToken] = useState('');
    const [isLoading, setLoading] = useState(true);
    const [isPicture, setPicture] = useState(false);
    const [modalInfo, setModalInfo] = useState(false);
    const [messageInfo, setMessageInfo] = useState('');

    useEffect(() => {
        navigation.addListener('focus', () => {
            getToken();
        });
    }, [])

    const onPressBack = () => {
        navigation.goBack();
    }

    const onPressEdit = () => {
        navigation.navigate('PersonUpdate');
    }

    const getToken = async () => {
        try {
            setLoading(true)
            const idToken = await ManagerAuth.getToken();
            getUserData(idToken.toString());
        } catch (error) {
            console.log(error)
        } finally {
            setLoading(false)
        }
    }

    const setUserInfos = ({ name, city, state, bio, college, crm, expertise, avatar }) => {
        console.log(`${avatar}v1.${new Date()}`)


        setForm({
            name,
            address: city + ' - ' + state,
            bio,
            university: college,
            crm,
            speciality: expertise || '',
            avatar: `${avatar}?v1.${new Date()}`,
            initials: name ? name.split(" ").map((n) => n[0]).join(".") : ""
        })
        setPicture(avatar ? true : false)
        setLoading(false)
    }

    const onUploadImage = async () => {
        navigation.navigate('UploadImage')
    }

    const getUserData = (idToken) => {
        console.log('token', idToken)
        ServiceApi.getProfile(idToken, (data) => {
            setUserInfos(data.profile);
        }, (error) => {
            // tratar o erro ou disparar erro default
            setModalInfo(true)
            setLoading(false);
            setMessageInfo(error)
        });
    }


    return (
        <View style={styles.container}>

            <View style={styles.navigation_bar}>
                <TouchableOpacity onPress={onPressBack}>
                    <IconBack width="32" height="32"></IconBack>
                </TouchableOpacity>
                <Text style={[styles.text_title]}>Dados Pessoais</Text>
                <TouchableOpacity onPress={onPressEdit}>
                    <IconEdit width="32" height="32"></IconEdit>
                </TouchableOpacity>
            </View>

            {isLoading ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#4493FF" />
                </View>
                :
                <ScrollView style={styles.sub_container}>
                    {form.name === '' ? null
                        : <TouchableOpacity style={styles.view_image} onPress={onUploadImage}>
                            <View style={{
                                width: 80,
                            }}>
                                <Avatar
                                    rounded
                                    size="large"
                                    title={form.initials}
                                    avatarStyle={isPicture ? null : { backgroundColor: '#d3d3d3', opacity: 0.7 }}
                                    titleStyle={{ color: 'black', padding: 5, fontSize: 13 }}
                                    source={{
                                        uri:
                                            form.avatar,
                                    }}
                                />
                                <View style={{ position: 'absolute', bottom: 0, right: 0, justifyContent: 'center', alignItems: 'center', width: 30, height: 30, borderRadius: 30 / 2, backgroundColor: '#006CFF' }}>
                                    <IconCamera width="18" height="16"></IconCamera>
                                </View>
                            </View>
                        </TouchableOpacity>
                    }
                    <Text style={[styles.text_style1, { marginTop: 32 }]}>Dados Pessoais</Text>
                    <View style={styles.container_data}>

                        <View style={styles.padding_content}>
                            <Text style={styles.text_style2}>Nome</Text>
                            <Text style={styles.text_style1}>{form.name}</Text>
                        </View>

                        <View style={styles.line_space}></View>

                        <View style={styles.padding_content}>
                            <Text style={styles.text_style2}>Universidade</Text>
                            <Text style={styles.text_style1}>{form.university}</Text>
                        </View>

                        <View style={styles.line_space}></View>

                        <View style={styles.padding_content}>
                            <Text style={styles.text_style2}>Residência</Text>
                            <Text style={styles.text_style1}>{form.bio}</Text>
                        </View>

                        <View style={styles.line_space}></View>


                        <View style={styles.padding_content}>
                            <Text style={styles.text_style2}>Especialidade</Text>
                            <Text style={styles.text_style1}>{form.speciality}</Text>
                        </View>

                        <View style={styles.line_space}></View>

                        <View style={styles.padding_content}>
                            <Text style={styles.text_style2}>CRM</Text>
                            <Text style={styles.text_style1}>{form.crm}</Text>
                        </View>

                        <View style={styles.line_space}></View>

                        <View style={styles.padding_content}>
                            <Text style={styles.text_style2}>Localização</Text>
                            <Text style={styles.text_style1}>{form.address}</Text>
                        </View>
                    </View>

                    <View style={styles.margin_end}></View>
                </ScrollView>
            }

            <BottomSheetFailure
                visible={modalInfo}
                text={messageInfo}
                onClose={() => setModalInfo(false)}
            />

        </View >
    );
}

export default PersonData;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    sub_container: {
        backgroundColor: '#E5E5E5',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Bold',
        justifyContent: 'center'
    },
    view_image: {
        alignItems: 'center',
        marginTop: 24,
    },
    container_data: {
        marginLeft: 16,
        marginRight: 16,
        borderRadius: 6,
        marginTop: 16,
        backgroundColor: 'white'
    },
    text_style1: {
        fontSize: 14,
        fontFamily: 'Montserrat-Bold',
        color: '#364574',
        marginLeft: 16,
        marginRight: 16
    },
    text_style2: {
        fontSize: 13,
        fontFamily: 'Montserrat-Regular',
        color: '#626B8A',
        marginLeft: 16,
        marginRight: 16
    },
    line_space: {
        backgroundColor: 'black',
        opacity: 0.1,
        height: 1,
        marginLeft: 16,
        marginRight: 16
    },
    padding_content: {
        paddingTop: 16,
        paddingBottom: 16
    },
    margin_end: {
        marginTop: 40
    }
});