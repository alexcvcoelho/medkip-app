import React from "react";
import { createStackNavigator, TransitionPresets } from "@react-navigation/stack";

import Splash from '~/pages/splash';
import Onboarding from '~/pages/onboarding';
import Login from '~/pages/login';
import UseConditionTerms from '~/pages/use-condition';

import AlertConfigurations from '~/pages/alert-config';
import Privacy from '~/pages/privacy';
import Help from '~/pages/help';
import Files from '~/pages/files';
import FileCategory from '~/pages/files/categories';
import Notifications from '~/pages/notifications';

import Home from '~/pages/home';
import Cases from '~/pages/cases';
import Financial from '~/pages/financial';
import Shifts from '~/pages/shifts';
import Shift from '~/pages/shifts/get';
import ShiftSave from '~/pages/shifts/save';
import Profile from '~/pages/profile';

import PersonData from '~/pages/person-data';
import PersonUpdate from '~/pages/person-update';

import UploadImage from '~/pages/upload-image';

import SearchCase from '~/pages/cases/search-case';
import CaseSave from '~/pages/cases/save';
import Case from '~/pages/cases/get';


import CompanyList from '~/pages/companies/get';
import CompanySave from '~/pages/companies/save';

import HospitalList from '~/pages/hospitals/get';
import HospitalSave from '~/pages/hospitals/save';

import DrawerNavigator from "~/pages/drawer";

const Stack = createStackNavigator();

const screenOptionStyle = {
    headerStyle: {
        backgroundColor: "#9AC4F8",
    },
    headerTintColor: "white",
    headerBackTitle: "Back",
    ...TransitionPresets.SlideFromRightIOS,
};

const MainStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName="Splash" screenOptions={screenOptionStyle}>
            <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
            <Stack.Screen name="Onboarding" component={Onboarding} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="UseConditionTerms" component={UseConditionTerms} options={{ headerShown: false }} />
            <Stack.Screen name="DrawerNavigator" component={DrawerNavigator} options={{ headerShown: false }} />
            <Stack.Screen name="PersonData" component={PersonData} options={{ headerShown: false }} />
            <Stack.Screen name="PersonUpdate" component={PersonUpdate} options={{ headerShown: false }} />
            <Stack.Screen name="UploadImage" component={UploadImage} options={{ headerShown: false }} />
            <Stack.Screen name="Notifications" component={Notifications} options={{ headerShown: false }} />
            <Stack.Screen name="SearchCase" component={SearchCase} options={{ headerShown: false }} />
            <Stack.Screen name="CaseSave" component={CaseSave} options={{ headerShown: false }} />
            <Stack.Screen name="Case" component={Case} options={{ headerShown: false }} />

            <Stack.Screen name="CompanyList" component={CompanyList} options={{ headerShown: false }} />
            <Stack.Screen name="CompanySave" component={CompanySave} options={{ headerShown: false }} />
            <Stack.Screen name="HospitalList" component={HospitalList} options={{ headerShown: false }} />
            <Stack.Screen name="HospitalSave" component={HospitalSave} options={{ headerShown: false }} />

            <Stack.Screen name="Shift" component={Shift} options={{ headerShown: false }} />
            <Stack.Screen name="ShiftSave" component={ShiftSave} options={{ headerShown: false }} />

        </Stack.Navigator>
    );
}

const HomeStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

const CasesStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Cases" component={Cases} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}
const FinancialStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Financial" component={Financial} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}
const ShiftsStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Shifts" component={Shifts} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}
const ProfileStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

const AlertConfigStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="AlertConfigurations" component={AlertConfigurations} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

const PrivacyStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Privacy" component={Privacy} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

const HelpStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Help" component={Help} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

const FilesStackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Files" component={Files} options={{ headerShown: false }} />
            <Stack.Screen name="FileCategory" component={FileCategory} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

export {
    MainStackNavigator,
    AlertConfigStackNavigator,
    PrivacyStackNavigator,
    HelpStackNavigator,
    FilesStackNavigator,
    HomeStackNavigator,
    CasesStackNavigator,
    FinancialStackNavigator,
    ShiftsStackNavigator,
    ProfileStackNavigator,
};