import React, { useState, useEffect, useRef } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Animated, Image, Modal, Dimensions } from 'react-native';

import { Card, ListItem, Button, Icon, Badge, Divider, CheckBox } from 'react-native-elements';
import {
    MenuProvider
} from 'react-native-popup-menu';

import { theme, variables, hexToRGB } from '~/theme';

import Midia from '~/components/midia';
import FloatingInput from '~/components/floatingInput';
import { SimpleBottomSheet, SimpleModal, BottomSheetConfirm, BottomSheetSuccess } from '~/components/bottomSheets';

import IconBack from "~/assets/images/arrow-back.svg";

import IconCalendar from "~/assets/images/icon-calendar2.svg";
import IconHospital from "~/assets/images/icon-hospital.svg";
import IconUser from "~/assets/images/icon-user.svg";
import IconTrash from "~/assets/images/trash.svg";
import IconEdit from "~/assets/images/edit.svg";

import Moment from 'moment';
import ServiceApi from '~/api/api-caller';
import moment from 'moment';

const index = ({  route, navigation }) => {

    const [modalMidiaConfirm, setModalMidiaConfirm] = useState(false);
    const [modalMidiaSuccess, setModalMidiaSuccess] = useState(false);

    const [grid, setGrid] = useState(true);

    const [selectItems, setSelectItems] = useState([]);

    const [midias, setMidias] = useState([
        {
            type: 'image',
            name: 'IMG_002',
            size: '115 KB',
            file: require('~/assets/images/torax.png'),
            progress: 100,
            selected: false,
        },
        {
            type: 'image',
            name: 'IMG_001',
            size: '115 KB',
            file: require('~/assets/images/torax.png'),
            progress: 100,
            selected: false,
        },
        {
            type: 'audio',
            name: 'Gravação 01',
            size: '01:20',
            file: '',
            progress: 100,
            selected: false,
        }
    ]);

    const item  = route.params.case;

    useEffect(() => {

    }, [])

    const onSelect = (index) => {
        const data = [...midias];
        if (index) {
            data[index].selected = true;
        } else {
            for (let i = 0; i < data.length; i++) {
                if (data[i].progress == 100) data[i].selected = true;
            }
        }
        setMidias(data);
        setSelectItems(data.filter(e => e.selected));
    }

    const onUnselect = () => {
        const data = [...midias];
        for (let i = 0; i < data.length; i++) {
            if (data[i].progress == 100) data[i].selected = false;
        }
        setMidias(data);
        setSelectItems(data.filter(e => e.selected));
    }

    const deleteFiles = () => {
        const data = [...midias];

        setMidias(data.filter(e => !e.selected));

        setSelectItems([]);

        setModalMidiaSuccess(true);
    }

    return (
        <MenuProvider>
            <View style={styles.container}>
                {selectItems.length == 0 && <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <IconBack width="32" height="32" />
                    </TouchableOpacity>
                    <Text style={styles.text_title}>Caso</Text>
                    <TouchableOpacity onPress={() => navigation.push('CaseSave', { case: item })}>
                        <IconEdit />
                    </TouchableOpacity>
                </View>}
                {selectItems.length > 0 && <View style={[styles.navigation_bar, { backgroundColor: variables.primary }]}>
                    <TouchableOpacity onPress={() => onUnselect()}>
                        <IconBack width="32" height="32" />
                    </TouchableOpacity>
                    <View style={{ flexGrow: 1 }}>
                        <Text style={{ fontSize: 14, color: variables.light }}>{selectItems.length > 1 ? `${selectItems.length} Arquivos selecionados` : `${selectItems.length} Arquivo selecionado`}</Text>
                        <Text style={{ fontSize: 11, color: variables.light }}>32 MB</Text>
                    </View>
                    <IconTrash onPress={() => setModalMidiaConfirm(true)} />
                </View>}
                <ScrollView>
                    <View style={styles.page}>
                        <View style={styles.header}>
                        <Text style={item.type == 'Caso Modelo' ? styles.badge : [styles.badge, styles.badge_error]}>{item.type}</Text>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Título do caso</Text>
                            <View style={styles.textfield}>
                                <IconHospital style={styles.textfield_icon} />
                                <Text style={styles.textfield_input}>{ item.title }</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Hospital</Text>
                            <View style={styles.textfield}>
                                <IconHospital style={styles.textfield_icon} />
                                <Text style={styles.textfield_input}>{item.hospital.name}</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Data do Ocorrido</Text>
                            <View style={styles.textfield}>
                                <IconCalendar style={styles.textfield_icon} />
                                <Text style={styles.textfield_input}>{moment(item.date).format("DD/MM/YYYY")}</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_title}>Nome do Paciente</Text>
                            <View style={styles.textfield}>
                                <IconUser style={styles.textfield_icon} />
                                <Text style={styles.textfield_input}>{item.name}</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.textfield_input}>{item.risk}</Text>
                            <Text style={[styles.textfield_input, styles.textfield_paragraph]}>{item.description}</Text>
                        </View>
                    </View>

                </ScrollView>

                <BottomSheetConfirm
                    visible={modalMidiaConfirm}
                    text={'Você tem certeza que deseja \nexcluir este(s) documento(s)?'}
                    onClose={() => setModalMidiaConfirm(false)}
                    onConfirm={() => deleteFiles()}
                />
                <BottomSheetSuccess
                    visible={modalMidiaSuccess}
                    text={'Documento(s) removido(s) \ncom sucesso!'}
                    onClose={() => setModalMidiaSuccess(false)}
                />
            </View>
        </MenuProvider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: variables.light,
    },
    navigation_bar: {
        height: 96,
        backgroundColor: variables.secondary_dark,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: variables.light,
        fontSize: 16,
        fontFamily: variables.fontFamily,
        justifyContent: 'center'
    },
    //meus estilos
    page: {
        padding: variables.paddingHorizontal,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 12
    },
    title: {
        color: variables.secondary,
        fontFamily: variables.fontBold,
        fontSize: variables.fontSize,
        margin: 0,
    },
    badge: {
        fontSize: 12,
        paddingVertical: 6,
        paddingHorizontal: 12,
        borderRadius: 4,
        backgroundColor: 'rgba(0, 108, 255, 0.1)',
        color: variables.primary,
    },
    badge_error: {
        color: variables.danger,
        backgroundColor: 'rgba(247, 28, 93, 0.1)',
    },
    textfield_title: {
        color: hexToRGB(variables.secondary, 0.5),
        fontSize: variables.fontSize,
    },
    textfield: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: variables.light,
        borderBottomWidth: 1,
        borderColor: '#C0C4D0',
        marginBottom: variables.paddingHorizontal,
        height: 48,
    },
    textfield_input: {
        flexGrow: 1,
        color: variables.secondary,
        fontSize: 16,
        fontWeight: "bold"
    },
    textfield_icon: {
        flexShrink: 0,
        marginRight: 12,
    },
    textfield_paragraph: {
        marginTop: 8,
        borderRightWidth: 2,
        borderColor: variables.primary,
        paddingRight: 8,
    }
})

export default index
