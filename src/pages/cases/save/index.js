import React, { Component, useState } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Modal, Dimensions, ActivityIndicator } from 'react-native';

//import DateTimePickerModal from "react-native-modal-datetime-picker";


import { Button, Icon, Divider, CheckBox } from 'react-native-elements';
import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import { variables, hexToRGB } from '~/theme';

import Midia from '~/components/midia';
import FloatingInput from '~/components/floatingInput';
import { SimpleBottomSheet, SimpleModal, BottomSheetConfirm, BottomSheetSuccess, BottomSheetFailure } from '~/components/bottomSheets';

import Search from '~/components/search-hospital';

import IconBack from "~/assets/images/arrow-back.svg";

import IconRadioActive from "~/assets/images/icon-radio-active.svg";
import IconRadioInactive from "~/assets/images/icon-radio-inactive.svg";


import IconPathSuccess from "~/assets/images/icon-path-success.svg";
import IconTrash from "~/assets/images/trash.svg";

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';

import Moment from 'moment';

export default class CaseSave extends Component {
    state = {
        isLoading: false,
        token: '',
        modalInfo: false,
        messageInfo: '',

        tipo: '',
        titulo: '',
        data: '',
        paciente: '',
        anotacoes: '',
        risco: '',
        hospital: '',
        idCaso: null,
        hospitalId: null,

        modalVisible: false,
        modalHospital: false,
        modalSuccess: false,

        modalMidiaConfirm: false,
        modalMidiaSuccess: false,

        grid: false,

        selectItems: [],
        itemEdit: null,

        modalDatePicker: false,

        midias: [
            /*{
                type: 'image',
                name: 'IMG_002',
                size: '115 KB',
                file: require('~/assets/images/torax.png'),
                progress: 65,
                selected: true,
            },
            {
                type: 'image',
                name: 'IMG_001',
                size: '115 KB',
                file: require('~/assets/images/torax.png'),
                progress: 100,
                selected: false,
            },
            {
                type: 'audio',
                name: 'Gravação 01',
                size: '01:20',
                file: '',
                progress: 100,
                selected: false,
            }*/
        ]
    }

    getToken = async () => {
        this.setState({ isLoading: true })

        const idToken = await ManagerAuth.getToken();
        console.log(idToken);
        const { route } = this.props;
        const itemEdit = route.params.case
        if (itemEdit) {
            console.log(itemEdit.id);
        }
        this.setState({ token: idToken.toString(), itemEdit: itemEdit })
        this.fillForm();
        this.setState({ isLoading: false })
    }

    fillForm = () => {
        if (this.state.itemEdit) {

            const {
                description,
                id,
                files,
                risk,
                hospitalId,
                hospital,
                date,
                name,
                profileId,
                title,
                type
            } = this.state.itemEdit;

            console.log('total files: ' + files.length)     
            console.log(files);       
            this.setState({
                anotacoes: description,
                titulo: title,
                hospital: hospital.name,
                paciente: name,
                risco: risk,
                tipo: type,
                data: Moment(date, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY HH:mm'),
                idCaso: id,
                hospitalId: hospitalId,
                midias: files
            });
        }
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    validadeFields = () => {
        if (this.state.tipo.length == 0) {
            this.setState({ messageInfo: 'Campo tipo de caso é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.titulo.length == 0) {
            this.setState({ messageInfo: 'Campo título é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.hospital.length == 0) {
            this.setState({ messageInfo: 'Campo hospital é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.risco.length == 0) {
            this.setState({ messageInfo: 'Campo risco é obrigatório!', modalInfo: true })
            return false
        } else if (this.state.data.length == 0) {
            this.setState({ messageInfo: 'Campo data é obrigatório!', modalInfo: true })
            return false
        } else if (!Moment(this.state.data, 'DD/MM/YYYY HH:mm').isValid()) {
            this.setState({ messageInfo: 'Campo data inválido!', modalInfo: true })
            return false
        } else if (this.state.anotacoes.length == 0) {
            this.setState({ messageInfo: 'Campo anotações é obrigatório!', modalInfo: true })
            return false
        }

        return true
    }

    onPressSaveData = () => {
        const result = this.validadeFields();

        if (!result) {
            return;
        }

        this.setState({ isLoading: true })
        //console.log('data atual: ' + this.state.data)
        const serverDate = Moment(this.state.data, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm')
        //console.log('data enviada: ' + serverDate)
        ServiceApi.saveCase(this.state.token, this.state.idCaso, serverDate, this.state.hospitalId,
            this.state.risco, this.state.anotacoes, this.state.paciente, this.state.tipo, this.state.titulo,
            async (data) => {
                await this.saveFiles(data.id);
                this.setState({ isLoading: false, modalSuccess: true })
            }, (error) => {
                this.setState({
                    isLoading: false, modalInfo: true, messageInfo: error
                })
            }, this.props)
    }

    saveFiles = async (caseId) => {
        if (this.state.midias.length <= 0)
            return;
        let promises = [];
        this.state.midias.forEach(midia => {
            let promise = new Promise((resolve, reject) => {
                ServiceApi.uploadCaseFile(this.state.token, midia.url, midia.type, midia.name, caseId, (data) => {
                    resolve(data);
                }, () => {
                    console.log("Erro");
                    reject()
                })
            })
            promises.push(promise);
        })
        return Promise.all(promises)
    }


    onPressUploadData = () => {
        // sobe apenas apos salvar os dados para obter o id
    }

    onUnselect = () => {

    }

    onSelect = (index) => {

    }

    deleteFiles = () => {
        //const data = [...midias];

        //setMidias(data.filter(e => !e.selected));

        //setSelectItems([]);

        //setModalMidiaSuccess(true);
    }

    select = (e) => {
        //{"city": "Campinas", "id": "e93606ce-8e3e-497a-a734-9e2f76a99e16", "iss": 20, "name": "Hospital Exemplo ", "profileId": "113353595481056821876"}
        this.setState({ hospital: e.name, modalHospital: false, hospitalId: e.id })
    }

    setupForm = () => {
        /*const [midias, setMidias] = useState([
            {
                type: 'image',
                name: 'IMG_002',
                size: '115 KB',
                file: require('~/assets/images/torax.png'),
                progress: 65,
                selected: false,
            },
            {
                type: 'image',
                name: 'IMG_001',
                size: '115 KB',
                file: require('~/assets/images/torax.png'),
                progress: 100,
                selected: false,
            },
            {
                type: 'audio',
                name: 'Gravação 01',
                size: '01:20',
                file: '',
                progress: 100,
                selected: false,
            }
        ]);*/

        const onSelect = (index) => {
            const data = [...midias];
            if (index) {
                data[index].selected = true;
            } else {
                for (let i = 0; i < data.length; i++) {
                    if (data[i].progress == 100) data[i].selected = true;
                }
            }
            //setMidias(data);
            //setSelectItems(data.filter(e => e.selected));
        }

        const onUnselect = () => {
            const data = [...midias];
            for (let i = 0; i < data.length; i++) {
                if (data[i].progress == 100) data[i].selected = false;
            }
            //setMidias(data);
            //setSelectItems(data.filter(e => e.selected));
        }

        const deleteFiles = () => {
            //const data = [...midias];

            //setMidias(data.filter(e => !e.selected));

            //setSelectItems([]);

            //setModalMidiaSuccess(true);
        }

        const select = (e) => {
            //changeValue('hospital', e.name)
            //setModalHospital(false);
        }
    }


    onChangeData = (date) => {
        console.warn("A date has been picked: ", date);
    }


    render() {
        const { navigation } = this.props;

        return (
            <MenuProvider>
                <View style={styles.container}>
                    {this.state.selectItems.length == 0 && <View style={styles.navigation_bar}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <IconBack width="32" height="32" />
                        </TouchableOpacity>
                        <Text style={styles.text_title}>{this.state.itemEdit != null ? 'Editar Caso' : 'Adicionar Novo Caso '}</Text>
                        <View style={{ width: 30, height: 30 }}></View>
                    </View>}
                    {this.state.selectItems.length > 0 && <View style={[styles.navigation_bar, { backgroundColor: variables.primary }]}>
                        <TouchableOpacity onPress={() => this.onUnselect()}>
                            <IconBack width="32" height="32" />
                        </TouchableOpacity>
                        <View style={{ flexGrow: 1 }}>
                            <Text style={{ fontSize: 14, color: variables.light }}>{this.state.selectItems.length > 1 ? `${this.state.selectItems.length} Arquivos selecionados` : `${selectItems.length} Arquivo selecionado`}</Text>
                            <Text style={{ fontSize: 11, color: variables.light }}>32 MB</Text>
                        </View>
                        <IconTrash onPress={() => this.setState({ modalMidiaConfirm: true })} />
                    </View>}
                    <ScrollView>
                        <View style={styles.page}>
                            <View style={styles.header}>
                                <Text style={styles.title}>Selecione o Tipo de Caso: </Text>
                                <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                                    <Icon name="info" type="feather" color={variables.gray3} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <CheckBox
                                    title='Caso de Risco'
                                    containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, flex: 0.5, paddingHorizontal: 0, marginLeft: 0 }}
                                    textStyle={{ color: variables.gray2, fontWeight: this.state.tipo == 'Caso de Risco' ? 'bold' : 'normal' }}
                                    checkedIcon={<IconRadioActive />}
                                    uncheckedIcon={<IconRadioInactive />}
                                    checked={this.state.tipo == 'Caso de Risco'}
                                    onPress={() => this.setState({ tipo: 'Caso de Risco' })}
                                />
                                <CheckBox
                                    title='Caso Modelo'
                                    containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, flex: 0.5, paddingHorizontal: 0, marginRight: 0 }}
                                    textStyle={{ color: variables.gray2, fontWeight: this.state.tipo == 'Caso Modelo' ? 'bold' : 'normal' }}
                                    checkedIcon={<IconRadioActive />}
                                    uncheckedIcon={<IconRadioInactive />}
                                    checked={this.state.tipo == 'Caso Modelo'}
                                    onPress={() => this.setState({ tipo: 'Caso Modelo' })}
                                />
                            </View>
                            <Divider style={{ marginBottom: 24 }} />
                            <FloatingInput
                                label="Título do Caso"
                                isMask={false}
                                value={this.state.titulo}
                                onChangeText={(text) => this.setState({ titulo: text })}
                            />
                            <TouchableOpacity style={styles.textfield} onPress={() => this.setState({ modalHospital: true })}>
                                <TextInput
                                    placeholder="Selecione o Hospital"
                                    placeholderTextColor={variables.gray3}
                                    editable={false}
                                    value={this.state.hospital}
                                    style={{ color: variables.secondary, height: variables.inputHeight }}
                                />
                                <Icon name="chevron-down" type="feather" color={variables.accent} />
                            </TouchableOpacity>

                            <FloatingInput
                                label="Data do Ocorrido (DD/MM/YYYY HH:mm)"
                                maskType={'datetime'}
                                options={{
                                    format: 'DD/MM/YYYY HH:mm'
                                }}
                                isMask={true}
                                editable={true}
                                value={this.state.data}
                                onChangeText={(text) => this.setState({ data: text })}
                            />

                            <FloatingInput
                                label="Nome do Paciente"
                                isMask={false}
                                value={this.state.paciente}
                                onChangeText={(text) => this.setState({ paciente: text })}
                            />

                            <Menu onSelect={value => this.setState({ risco: value })}>
                                <MenuTrigger>
                                    <View style={styles.textfield}>
                                        <TextInput
                                            placeholder="Selecione o Risco"
                                            placeholderTextColor={variables.gray3}
                                            editable={false}
                                            value={this.state.risco}
                                            style={{ color: variables.secondary, height: variables.inputHeight }}
                                        />
                                        <Icon name="chevron-down" type="feather" color={variables.accent} />
                                    </View>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={{ marginTop: 50 }} customStyles={menu}>
                                    <MenuOption value="Risco Baixo">
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>Risco Baixo</Text>
                                        </View>
                                    </MenuOption>
                                    <MenuOption value="Risco Médio">
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>Risco Médio</Text>
                                        </View>
                                    </MenuOption>
                                    <MenuOption value="Risco Alto">
                                        <View style={styles.dropdown_item}>
                                            <Text style={styles.dropdown_item_txt}>Risco Alto</Text>
                                        </View>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>

                            <View style={styles.textfield}>
                                <TextInput
                                    placeholder="Escreva aqui anotações rápidas sobre o caso"
                                    placeholderTextColor={variables.gray3}
                                    multiline={true}
                                    numberOfLines={4}
                                    style={{ textAlignVertical: 'top', color: variables.secondary, height: variables.inputHeight }}
                                    onChangeText={(text) => this.setState({ anotacoes: text })}
                                    value={this.state.anotacoes}
                                />
                            </View>
                            <Divider />
                        </View>
                        <Midia
                            items={this.state.midias}
                            onSelect={(e) => this.onSelect(e)}
                            grid={this.state.grid}
                            onChangeView={() => this.setState({ grid: !this.state.grid })}
                        />

                        <TouchableOpacity style={styles.button_continue} onPress={() => this.onPressSaveData()}>
                            {this.state.isLoading ?
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator size="large" color="#4493FF" />
                                </View>
                                :
                                <Text style={styles.text_style_bold}>{this.state.itemEdit > 0 ? 'Salvar alterações' : 'Salvar'}</Text>
                            }
                        </TouchableOpacity>

                        <View style={{ height: variables.paddingHorizontal }} />

                    </ScrollView>

                    <BottomSheetFailure
                        visible={this.state.modalInfo}
                        text={this.state.messageInfo}
                        onClose={() => { this.setState({ modalInfo: false }); }}
                    />

                    <SimpleModal
                        visible={this.state.modalVisible}
                        onClose={() => this.setState({ modalVisible: false })}>
                        <Text style={styles.title}>Caso de risco</Text>
                        <Text style={styles.paragraph}>É aquele que poderá gerar futuramente qualquer tipo de Risco ou problema ao Médico (Usuário) em razão de eventual questionamento ético junto ao CRM ou ação judicial por parte do Paciente ou de seus familiares, visando uma reparação indenizatória contra Médico (Usuário), a Equipe Médica ou o Hospital.</Text>
                        <Text style={styles.title}>Caso modelo</Text>
                        <Text style={styles.paragraph}>É o Caso que poderá ser utilizado para publicação de artigo, como parâmetro em outras situações em razão da conduta utilizada (p. ex.), arquivo profissional, entre outros.</Text>
                    </SimpleModal>


                    <SimpleBottomSheet
                        visible={this.state.modalSuccess}
                        onClose={() => this.setState({ modalSuccess: false })}
                    >
                        <View style={{ alignItems: 'center' }}>
                            <IconPathSuccess />
                            {(this.state.itemEdit == null) && <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Um novo caso foi adicionado \ncom suceso!'}</Text>}
                            {(this.state.itemEdit != null) && <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Caso alterado \ncom suceso!'}</Text>}
                        </View>
                        <Button
                            title="Ver Caso"
                            disabled={false}
                            onPress={() => this.props.navigation.goBack()}
                            buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                            titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                            disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                            disabledTitleStyle={{ color: variables.light }}
                        />
                    </SimpleBottomSheet>
                    <BottomSheetConfirm
                        visible={this.state.modalMidiaConfirm}
                        text="Você tem certeza que deseja excluir os arquivos selecionados?"
                        onClose={() => this.setState({ modalMidiaConfirm: false })}
                        onConfirm={() => this.deleteFiles()}
                    />
                    <BottomSheetSuccess
                        visible={this.state.modalMidiaSuccess}
                        text="Arquivos apagados com sucesso!"
                        onClose={() => this.setState({ modalMidiaSuccess: false })}
                    />
                </View>
                <Modal visible={this.state.modalHospital}>
                    <Search onClose={() => this.setState({ modalHospital: false })} onSelect={(e) => this.select(e)} isSelected={this.state.hospital} />
                </Modal>
            </MenuProvider>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: variables.light,
    },
    navigation_bar: {
        height: 96,
        backgroundColor: variables.secondary_dark,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: variables.light,
        fontSize: 16,
        fontFamily: variables.fontFamily,
        justifyContent: 'center'
    },
    //meus estilos
    page: {
        // flex: 1,
        padding: variables.paddingHorizontal,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 12
    },
    title: {
        color: variables.secondary,
        fontFamily: variables.fontBold,
        fontSize: variables.fontSize,
        margin: 0,
    },
    textfield: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: variables.paddingHorizontal,
        borderRadius: variables.borderRadius,
        backgroundColor: variables.light,
        borderWidth: 1,
        borderColor: '#C0C4D0',
        marginBottom: variables.paddingHorizontal,
        position: 'relative',
    },
    dropdown_item: {
        padding: variables.paddingHorizontal,
        backgroundColor: variables.light,
    },
    dropdown_item_txt: {
        fontSize: variables.fontSize,
        color: variables.secondary,
    },
    paragraph: {
        fontSize: variables.fontSize,
        color: variables.gray2,
        marginVertical: 8,
    },
    button_continue: {
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: '#006CFF',
        height: 48,

        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 16,
        marginRight: 16
    },
    text_style_bold: {
        color: '#ffffff',
        fontSize: 14,
        marginLeft: 24,
        fontFamily: 'Montserrat-SemiBold'
    },
})

const windowWidth = Dimensions.get('window').width - variables.paddingHorizontal * 2;

const menu = {
    optionsContainer: {
        padding: 0,
        width: windowWidth,
    },
    optionWrapper: {
        margin: 0,
        padding: 0,
    },
}
const menu2 = {
    optionsContainer: {
        padding: 0,
    },
    optionWrapper: {
        margin: 0,
        padding: 0,
    },
}
