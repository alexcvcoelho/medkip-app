import React, { Component, useState, useRef } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Modal, Image } from 'react-native';

import { Card, ListItem, Button, Icon, Badge, Input, BottomSheet } from 'react-native-elements';

import { theme, variables, hexToRGB } from '~/theme';

import IconBack from "~/assets/images/arrow-back.svg";

import IconCalendar from "~/assets/images/icon-calendar.svg";
import IconUser from "~/assets/images/icon-user.svg";

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';
import { BottomSheetFailure } from '~/components/bottomSheets';

export default class SearchCase extends Component {
    state = {
        token: '',
        items: [],
        query: '',
        modalInfo: false,
        messageInfo: ''
    };

    getCases = () => {
        ServiceApi.getCases(this.state.token, (response) => {
            this.setState({ items: response, isLoading: false })
        }, (error) => {
            this.setState({
                isLoading: false, modalInfo: true, messageInfo: error
            })
        }, this.props);
    }

    getToken = async () => {
        this.setState({ isLoading: true })

        const idToken = await ManagerAuth.getToken();

        this.setState({ token: idToken.toString() })

        this.getCases();
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    render() {
        const navigation = this.props.navigation;

        return (
            <View style={styles.container}>
                <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <IconBack width="32" height="32" />
                    </TouchableOpacity>
                    <TextInput
                        style={{ flexGrow: 1, height: 40, top: 3, position: 'relative', color: variables.light }}
                        placeholder="Pesquisar..."
                        placeholderTextColor="#E0E1E8"
                        onChangeText={(text) => this.setState({ query: text })}
                        value={this.state.query}
                        autoFocus
                    />
                    {this.state.query.length > 0 && <Icon name="close" color={variables.light} onPress={() => this.setState({ query: '' })} />}
                </View>
                <View style={styles.page}>
                    {this.state.query.length > 0 && this.state.items.length > 0 ?
                        <ScrollView>
                            <TouchableOpacity onPress={() => navigation.push('Case')}>
                                <Card containerStyle={{ borderWidth: 0, borderRadius: 6, elevation: 0, marginLeft: 0, marginRight: 0 }}>
                                    <View style={styles.card_header}>
                                        <Text style={styles.card_title}>Pneumonias atípicas</Text>
                                        <Text style={[styles.badge, styles.badge_error]}>Caso de Risco</Text>
                                    </View>
                                    <Card.Divider />
                                    <Text style={styles.text}>Hospital Albert Einstein, São Paulo</Text>
                                    <View style={styles.card_grid}>
                                        <View style={styles.card_col}>
                                            <IconCalendar style={styles.card_icon} />
                                            <Text style={styles.text}>28/08</Text>
                                        </View>
                                        <View style={styles.card_col}>
                                            <IconUser style={styles.card_icon} />
                                            <Text style={styles.text}>Fernando Pacheco</Text>
                                        </View>
                                    </View>
                                </Card>
                            </TouchableOpacity>
                        </ScrollView>
                        :
                        <View />}

                    <BottomSheetFailure
                        visible={this.state.modalInfo}
                        text={this.state.messageInfo}
                        onClose={() => { this.setState({ modalInfo: false }); }}
                    />
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
    //meus estilos
    page: {
        flex: 1,
        backgroundColor: '#F5F6FA',
        padding: 16,
        paddingBottom: 76,
        justifyContent: 'center'
    },
    text: {
        color: variables.secondary,
        fontSize: 14,
    },
    search_bar: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 12,
        borderRadius: 6,
        backgroundColor: '#ffffff',
    },
    search_bar_input: {
        height: 48,
        flexGrow: 1,
    },
    search_bar_icon: {
        flexShrink: 0,
    },
    chips: {
        flexDirection: 'row',
        marginTop: variables.paddingHorizontal,
    },
    chip: {
        backgroundColor: variables.primary,
        padding: 6,
        borderRadius: 4,
        flexDirection: 'row',
        flex: 0.5,
        marginRight: 8,
        justifyContent: 'space-between',
    },
    chip_txt: {
        color: variables.light,
        fontSize: 12
    },
    card_header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: variables.paddingVertical
    },
    card_title: {
        color: variables.secondary,
        fontFamily: variables.fontBold,
        fontSize: variables.fontSize,
        margin: 0,
    },
    badge: {
        fontSize: 12,
        paddingVertical: 6,
        paddingHorizontal: 12,
        borderRadius: 4,
        backgroundColor: 'rgba(0, 108, 255, 0.1)',
        color: variables.primary
    },
    badge_error: {
        color: variables.danger,
        backgroundColor: 'rgba(247, 28, 93, 0.1)',
    },
    card_grid: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: variables.paddingVertical,
    },
    card_col: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    card_icon: {
        marginRight: 8,
        width: 17,
        height: 19,
    },
    fab_button_container: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: variables.paddingHorizontal,
        right: variables.paddingHorizontal,
        left: variables.paddingHorizontal,
        backgroundColor: hexToRGB(variables.accent, 0.15),
        borderRadius: 56 / 2,
    },
    fab_button: {
        width: 56,
        height: 56,
        borderRadius: 56 / 2,
        backgroundColor: variables.primary,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: variables.primary,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.6,
        shadowRadius: 24,

        elevation: 10,
    },
    fab_button_hint: {
        flexGrow: 1,
        paddingHorizontal: 28,
        paddingVertical: 10,
        color: variables.primary,
    }
});
