import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Image, ActivityIndicator } from 'react-native';

import { Card, Icon } from 'react-native-elements';

import { theme, variables, hexToRGB } from '~/theme';

import IconMenu from "~/assets/images/menu.svg";
import IconFilter from "~/assets/images/filter.svg";
import IconCalendar from "~/assets/images/icon-calendar.svg";
import IconUser from "~/assets/images/icon-user.svg";
import IconPath from "~/assets/images/icon-path-notfound.svg";
import IconCloseChip from "~/assets/images/icon-close-chip.svg";

import Filter from "~/components/bottomSheets/filter";

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';
import { BottomSheetFailure } from '~/components/bottomSheets';

import Moment from 'moment';


export default class Cases extends Component {
    state = {
        modalVisible: false,
        openFilter: false,
        filters: {
            periodo: '',
            hospital: ''
        },
        token: '',
        items: [],
        filterList: [],
        isLoading: true,
        modalInfo: false,
        messageInfo: '',
        myHospitals: [],
        resultList: [],
    };

    getCases = () => {
        ServiceApi.getCases(this.state.token, (response) => {
            this.orderList(response, false)
            this.getHospitals();
        }, (error) => {
            this.setState({
                isLoading: false, modalInfo: true, messageInfo: error
            })
        }, this.props);
    }

    orderList = (list, isFiltered) => {
        list.sort((a, b) => a.date - b.date);

        if (!isFiltered) {
            this.setState({ items: list, isLoading: false, resultList: list })
        } else {
            this.setState({ filterList: list, isLoading: false })
            list.length == 0
                ? this.orderList(this.state.items, false)
                : this.setState({ resultList: list })
        }
    }

    getFilterList = (e) => {
        var result = []

        if (e != null) {
            if (e.hospital != '') {
                // se periodo selecionado

                // se periodo nao selecionado
                result = this.state.items.filter(x => String(x.hospital.name).toLowerCase() == (String(e.hospital).toLowerCase()))
            }
            if (e.periodo != '') {
                console.log(e.periodo)
                const arrayPeriodo = String(e.periodo).split(" até ")
                const inicio = arrayPeriodo[0]
                const fim = arrayPeriodo[1]
                var dateInicio = Moment(inicio, 'DD/MM/YY').toDate();
                dateInicio.setHours(0)
                dateInicio.setMinutes(0)
                dateInicio.setSeconds(0)
                dateInicio.setMilliseconds(0)

                var dateFim = Moment(fim, 'DD/MM/YY').toDate();
                dateFim.setHours(0)
                dateFim.setMinutes(0)
                dateFim.setSeconds(0)
                dateFim.setMilliseconds(0)

                var fPeriodo = []
                if (result.length > 0) {
                    fPeriodo = result.filter(item => {
                        var t1 = Moment(item.date, 'YYYY-MM-DD HH:mm').toDate()
                        t1.setHours(0)
                        t1.setMinutes(0)
                        t1.setSeconds(0)
                        t1.setMilliseconds(0)
                        return t1.getTime() >= dateInicio.getTime() && t1.getTime() <= dateFim.getTime();
                    })
                } else {
                    fPeriodo = this.state.items.filter(item => {
                        var t1 = Moment(item.date, 'YYYY-MM-DD HH:mm').toDate()
                        t1.setHours(0)
                        t1.setMinutes(0)
                        t1.setSeconds(0)
                        t1.setMilliseconds(0)
                        return t1.getTime() >= dateInicio.getTime() && t1.getTime() <= dateFim.getTime();
                    })
                }

                if (fPeriodo.length > 0) {
                    result = fPeriodo
                }
                //console.log(result)
            }

            //console.log(result)
            this.orderList(result, true)
        } else {
            this.orderList([], true)
        }
    }

    getHospitals = () => {
        ServiceApi.getHospitals(this.state.token, (response) => {
            this.setState({ myHospitals: response, isLoading: false })
        }, (error) => {
            this.setState({
                isLoading: false, modalInfo: true, messageInfo: error
            })
        }, this.props);
    }

    getToken = async () => {
        this.setState({ isLoading: true })

        const idToken = await ManagerAuth.getToken();

        this.setState({ token: idToken.toString() })

        this.getCases();
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.setState({ filters: { periodo: '', hospital: '' } })
            this.getToken();
        });
    }

    render() {
        const navigation = this.props.navigation

        const closeFilter = () => {
            this.setState({ openFilter: false })
        }

        const filter = (e) => {
            this.setState({ openFilter: false, filters: e })
            //console.log(e) //aplicou filtro
            this.getFilterList(e)
        }

        const changeFilters = (key, value) => {
            const data = { ...this.state.filters };
            data[key] = value;
            this.setState({ filters: data });
            this.getFilterList(data)
        };

        return (
            <View style={styles.container}>
                <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <IconMenu width="32" height="32"></IconMenu>
                    </TouchableOpacity>
                    <Text style={[styles.text_title]}>Casos</Text>
                    <TouchableOpacity onPress={() => this.setState({ openFilter: true })} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <IconFilter width="32" height="32" style={{ position: 'relative', top: 4 }}></IconFilter>
                    </TouchableOpacity>
                </View>

                {this.state.isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#4493FF" />
                    </View>
                    :

                    <View style={{ flex: 1 }}>
                        <ScrollView>
                            <View style={styles.page}>
                                <TouchableOpacity style={styles.search_bar} onPress={() => navigation.push('SearchCase')}>
                                    <TextInput
                                        style={styles.search_bar_input}
                                        placeholder="Buscar por caso"
                                        placeholderTextColor="#8189A1"
                                        editable={false}
                                    />
                                    <Icon name="search" color={variables.primary} style={styles.search_bar_icon} />
                                </TouchableOpacity>
                                {this.state.resultList.length > 0 && <>
                                    {(this.state.filters.periodo !== '' || this.state.filters.hospital !== '') && <View style={theme.chips}>
                                        {(this.state.filters.periodo !== '') && <TouchableOpacity onPress={() => changeFilters('periodo', '')} style={theme.chip}>
                                            <Text style={theme.chip_txt}>{this.state.filters.periodo}</Text>
                                            <IconCloseChip />
                                        </TouchableOpacity>}
                                        {(this.state.filters.hospital !== '') && <TouchableOpacity onPress={() => changeFilters('hospital', '')} style={[theme.chip, { marginRight: 0 }]}>
                                            <Text style={theme.chip_txt} numberOfLines={1}>{this.state.filters.hospital}</Text>
                                            <View>
                                                <IconCloseChip />
                                            </View>
                                        </TouchableOpacity>}
                                    </View>}

                                    {this.state.resultList.map((item, index) => (
                                        <TouchableOpacity onPress={() => navigation.navigate('Case', { case: item })} style={{ marginBottom: index === this.state.resultList.length - 1 ? 76 : 0 }} key={index}>
                                            <Card key={index} containerStyle={{ borderWidth: 0, borderRadius: 6, elevation: 0, marginLeft: 0, marginRight: 0 }}>
                                                <View style={styles.card_header}>
                                                    <Text style={styles.card_title}>{item.title}</Text>
                                                    <Text style={item.type == 'Caso Modelo' ? styles.badge : [styles.badge, styles.badge_error]}>{item.type}</Text>
                                                </View>
                                                <Card.Divider />
                                                <Text style={styles.text}>{item.hospital.name}</Text>
                                                <View style={styles.card_grid}>
                                                    <View style={styles.card_col}>
                                                        <IconCalendar style={styles.card_icon} />
                                                        <Text style={styles.text}>{Moment(item.date).format('DD/MM/YYYY HH:mm')}</Text>
                                                    </View>
                                                    <View style={styles.card_col}>
                                                        <IconUser style={styles.card_icon} />
                                                        <Text style={styles.text}>{item.name}</Text>
                                                    </View>
                                                </View>
                                            </Card>
                                        </TouchableOpacity>
                                    ))}
                                </>}
                            </View>
                        </ScrollView>
                        {this.state.resultList.length > 0 && <>
                            <View style={[theme.fab_button_container, { backgroundColor: 'transparent' }]}>
                                <TouchableOpacity style={theme.fab_button} onPress={() => navigation.push('CaseSave', { case: null })}>
                                    <Icon name="plus" type="feather" color="#ffffff" />
                                </TouchableOpacity>
                            </View>
                        </>}
                        {this.state.resultList.length == 0 && <>
                            <View style={{ alignItems: 'center', flexGrow: 1 }}>
                                <IconPath width="48" height="36" style={{ marginBottom: 19 }} />
                                <Text style={{ color: variables.gray3, fontSize: 14 }}>Nenhum caso encontrado</Text>
                                <View style={theme.fab_button_container}>
                                    <Text style={theme.fab_button_hint}>{'Você pode adicionar um novo \ncaso clicando aqui'}</Text>
                                    <TouchableOpacity style={theme.fab_button} onPress={() => navigation.navigate('CaseSave', { case: null })}>
                                        <Icon name="plus" type="feather" color="#ffffff" />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </>}
                        <Filter
                            visible={this.state.openFilter}
                            onClose={() => closeFilter()}
                            hospitals={this.state.myHospitals}
                            onOpen={() => this.setState({ openFilter: true })}
                            onFilter={(e) => filter(e)}
                        />

                        <BottomSheetFailure
                            visible={this.state.modalInfo}
                            text={this.state.messageInfo}
                            onClose={() => { this.setState({ modalInfo: false }); }}
                        />
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5F6FA',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
    //meus estilos
    page: {
        flex: 1,
        padding: 16,
        // paddingBottom: 76,
    },
    text: {
        color: variables.secondary,
        fontSize: 14,
    },
    search_bar: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 12,
        borderRadius: 6,
        backgroundColor: '#ffffff',
    },
    search_bar_input: {
        height: 48,
        flexGrow: 1,
    },
    search_bar_icon: {
        flexShrink: 0,
    },
    card_header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: variables.paddingVertical
    },
    card_title: {
        color: variables.secondary,
        fontFamily: variables.fontBold,
        fontSize: variables.fontSize,
        margin: 0,
    },
    badge: {
        fontSize: 12,
        paddingVertical: 6,
        paddingHorizontal: 12,
        borderRadius: 4,
        backgroundColor: 'rgba(0, 108, 255, 0.1)',
        color: variables.primary,
    },
    badge_error: {
        color: variables.danger,
        backgroundColor: 'rgba(247, 28, 93, 0.1)',
    },
    card_grid: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: variables.paddingVertical,
    },
    card_col: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    card_icon: {
        marginRight: 8,
        width: 17,
        height: 19,
    },
    fab_button: {
        position: 'absolute',
        bottom: 56 / 2,
        right: 56 / 2,
        width: 56,
        height: 56,
        borderRadius: 56 / 2,
        backgroundColor: variables.primary,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: variables.primary,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.6,
        shadowRadius: 24,

        elevation: 10,
    },
});