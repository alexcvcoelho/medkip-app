import React, { Component } from 'react';
import { View } from 'react-native';

import { GoogleSignin } from '@react-native-community/google-signin';
export default class Logout extends Component {

    logout = async () => {
        try {
            await GoogleSignin.revokeAccess();
        } catch (error) {
            console.log("Cant revoke access token, cuz token is expired.");
        } finally {
            await GoogleSignin.signOut();
            console.log('User signed out!')
            this.props.navigation.navigate('Splash');
        }
    }

    componentDidMount() {
        this.logout();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

            </View>
        );
    }
}