import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Animated, Image, Modal, Dimensions } from 'react-native';

import { Divider, Icon } from 'react-native-elements';

import { MaskService } from 'react-native-masked-text';

import { theme, variables, hexToRGB } from '~/theme';
import { SimpleModal } from '~/components/bottomSheets';

import IconMenu from "~/assets/images/menu.svg";
import IconBell from "~/assets/images/bell.svg";
import IconMedkip from "~/assets/images/medkip-white.svg";

import IconFinancial from "~/assets/images/icon-financial.svg";
import IconCases from "~/assets/images/icon-cases.svg";
import IconCalendar from "~/assets/images/icon-calendar.svg";
import IconCalendar2 from "~/assets/images/icon-calendar2.svg";


export default class Home extends Component {

    state = {
        total: 5000,
        showValues: false,
        showInfo: false,
        schedules: [],
        cases: [],
    }

    convertMoney = (value) => {
        let money = MaskService.toMask('money', value, {
            unit: 'R$ ',
            separator: ',',
            delimiter: '.'
        })

        return money;
    }

    onShowValues = (value, show) => {
        if (show) {
            return value;
        } else {
            let v = value.split('R$ ');
            let points = '';
            for (let i = 0; i < v[1].length; i++) {
                points += '. ';
            }
            return 'R$ ' + points;
        }
    }

    render() {
        const navigation = this.props.navigation
        return (
            <View style={[theme.container, { backgroundColor: '#F5F6FA' }]}>
                <View style={theme.navigation_bar}>
                    <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <IconMenu width="32" height="32"></IconMenu>
                    </TouchableOpacity>
                    <IconMedkip width="91" height="23"></IconMedkip>
                    <TouchableOpacity onPress={() => navigation.navigate('Notifications')} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <IconBell width="32" height="32"></IconBell>
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View style={styles.subheader}>
                        <Text style={styles.header_subtitle}>Olá, Dr. Rogério Lopes</Text>
                    </View>
                    <View style={styles.page}>
                        <View style={styles.card}>
                            <View style={styles.row}>
                                <View style={styles.card_col}>
                                    <IconFinancial style={styles.card_icon} />
                                    <Text style={theme.title}>Financeiro</Text>
                                </View>
                                <TouchableOpacity onPress={() => this.setState({ showInfo: true })}>
                                    <Icon name="info" type="feather" color={variables.gray3} size={20} />
                                </TouchableOpacity>
                            </View>
                            <Divider style={styles.divider} />
                            <View style={[styles.row, { alignItems: 'flex-end' }]}>
                                <View>
                                    <Text style={styles.subtitle}>Valor líquido nos próximos 30 dias</Text>
                                    <Text style={styles.value}>{this.onShowValues(this.convertMoney(this.state.total), this.state.showValues)}</Text>
                                </View>
                                <TouchableOpacity onPress={() => this.setState({ showValues: !this.state.showValues })} style={{ bottom: 3 }}>
                                    <Icon name={this.state.showValues ? 'eye' : 'eye-off'} type="feather" color={variables.primary} size={20} />
                                </TouchableOpacity>
                            </View>
                            <Divider style={styles.divider} />
                            <TouchableOpacity onPress={() => navigation.navigate("Financial")} style={styles.spacing}>
                                <Text style={styles.link}>Ver mais</Text>
                            </TouchableOpacity>
                        </View>

                        {this.state.schedules.length > 0 ?
                            <View>
                                <View style={styles.card}>
                                    <View style={styles.row}>
                                        <View style={styles.card_col}>
                                            <IconCalendar2 style={styles.card_icon} />
                                            <Text style={theme.title}>Próximo Plantão</Text>
                                        </View>
                                        <View style={[styles.badge, styles.badge_error]}>
                                            <Icon name="alert-triangle" type="feather" size={12} color={variables.danger} />
                                            <Text style={{ color: variables.danger, fontSize: 12, marginLeft: 5 }}>Começa em 1h</Text>
                                        </View>
                                    </View>
                                    <Text style={[styles.text, styles.spacing_h]}>Hospital Albert Einstein, São Paulo</Text>
                                    <View style={styles.row}>
                                        <View style={[styles.card_col, { flex: 1 / 2 }]}>
                                            <IconCalendar style={styles.card_icon} />
                                            <Text style={styles.text}>28/08</Text>
                                        </View>
                                        <View style={[styles.card_col, { flex: 1 / 2 }]}>
                                            <Icon name="clock" type="feather" style={styles.card_icon} size={20} color={variables.accent} />
                                            <Text style={styles.text}>15h - 20h</Text>
                                        </View>
                                    </View>
                                    <Divider style={styles.divider} />
                                    <TouchableOpacity onPress={() => navigation.push('Shift')} style={styles.spacing}>
                                        <Text style={styles.link}>Ver mais</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            : null
                        }

                        {this.state.cases.length > 0 ?
                            <View style={styles.card}>
                                <View style={styles.row}>
                                    <View style={styles.card_col}>
                                        <IconCases style={styles.card_icon} />
                                        <Text style={theme.title}>Casos recentes</Text>
                                    </View>
                                </View>
                                <Divider style={styles.divider} />
                                <TouchableOpacity onPress={() => navigation.push("Case")} style={styles.row}>
                                    <Text style={[styles.text, { fontSize: 15 }]}>Diabetes</Text>
                                    <View style={styles.card_col}>
                                        <Text style={[styles.badge, styles.badge_error, styles.card_icon]}>Risco</Text>
                                        <Icon name="chevron-right" color={variables.accent} />
                                    </View>
                                </TouchableOpacity>
                                <Divider style={styles.divider} />
                                <TouchableOpacity onPress={() => navigation.push("Case")} style={styles.row}>
                                    <Text style={[styles.text, { fontSize: 15 }]}>Pneumonias atípicas</Text>
                                    <View style={styles.card_col}>
                                        <Text style={[styles.badge, styles.card_icon]}>Modelo</Text>
                                        <Icon name="chevron-right" color={variables.accent} />
                                    </View>
                                </TouchableOpacity>
                                <Divider style={styles.divider} />
                                <TouchableOpacity onPress={() => navigation.push("Case")} style={styles.row}>
                                    <Text style={[styles.text, { fontSize: 15 }]}>Pneumonias atípicas</Text>
                                    <View style={styles.card_col}>
                                        <Text style={[styles.badge, styles.card_icon]}>Modelo</Text>
                                        <Icon name="chevron-right" color={variables.accent} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            : null
                        }
                    </View>
                </ScrollView>
                <SimpleModal visible={this.state.showInfo} onClose={() => this.setState({ showInfo: false })}>
                    <Text style={theme.title}>Texto sobre financeiro</Text>
                    <Text style={styles.paragraph}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pitor nibh id sapien condsectetur fringilla. Cras lacinia ipsum nec arcu tincidunt, id elementum massa suscipit. </Text>
                </SimpleModal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    //meus estilos
    subheader: {
        position: 'absolute',
        height: 96,
        backgroundColor: variables.secondary_dark,
        left: 0,
        right: 0,
        top: 0,
    },
    header_subtitle: {
        fontFamily: variables.fontBold,
        color: variables.light,
        fontSize: 16,
        paddingTop: variables.paddingHorizontal,
        textAlign: 'center',
    },
    page: {
        flex: 1,
        paddingHorizontal: variables.paddingHorizontal,
        paddingBottom: variables.paddingHorizontal,
        marginTop: 96 / 2,
    },
    spacing: {
        padding: variables.paddingHorizontal,
    },
    spacing_h: {
        paddingHorizontal: variables.paddingHorizontal,
    },
    card: {
        backgroundColor: variables.light,
        borderRadius: variables.borderRadius,
        // padding: variables.paddingHorizontal,
        marginVertical: variables.paddingHorizontal / 2,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: variables.paddingHorizontal,
    },
    divider: {
        backgroundColor: hexToRGB(variables.secondary_light, 0.1),
    },
    value: {
        color: variables.secondary,
        fontSize: 28,
        fontFamily: variables.fontBold,
    },
    subtitle: {
        color: variables.gray2,
        fontSize: variables.fontSize,
        marginBottom: 7,
    },
    text: {
        color: variables.gray2,
        fontSize: variables.fontSize,
        marginTop: 5,
    },
    text3: {
        color: variables.gray3,
    },
    rect: {
        width: 12,
        height: 12,
        backgroundColor: variables.secondary,
        marginRight: 8,
        alignSelf: 'center',
        borderRadius: 2,
    },
    textfield: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: variables.paddingHorizontal / 2,
        borderRadius: variables.borderRadius,
        backgroundColor: variables.light,
        borderWidth: 1,
        borderColor: '#C0C4D0',
        position: 'relative',
        height: 29,
        width: 130,
    },
    input: {
        flexGrow: 1,
        color: variables.gray2,
        fontSize: 12,
    },
    table: {
        paddingHorizontal: variables.paddingHorizontal,
        paddingBottom: variables.paddingHorizontal,
    },
    tr: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: variables.paddingHorizontal / 2,
    },
    tr_color: {
        backgroundColor: '#E8F2FF',
    },
    paragraph: {
        fontSize: variables.fontSize,
        color: variables.gray2,
        marginVertical: 8,
    },
    badge: {
        fontSize: 12,
        paddingVertical: 6,
        paddingHorizontal: 12,
        borderRadius: 4,
        backgroundColor: 'rgba(0, 108, 255, 0.1)',
        color: variables.primary,
        alignItems: 'center',
        flexDirection: 'row',
    },
    badge_error: {
        color: variables.danger,
        backgroundColor: 'rgba(247, 28, 93, 0.1)',
    },
    card_col: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    card_icon: {
        marginRight: 8,
    },
    link: {
        textAlign: 'center',
        color: variables.primary,
        fontWeight: 'bold',
        fontSize: variables.fontSize,
    }
});