import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Modal, TextInput, ScrollView } from 'react-native';
import { Icon } from 'react-native-elements';

import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import { theme, variables, hexToRGB } from '~/theme';

import Preview from '~/components/midia/preview';
import ServiceApi from '~/api/api-caller';
import ManagerAuth from '~/manger-auth/manager';

import IconClose from "~/assets/images/close-white.svg";
import IconBack from "~/assets/images/arrow-back.svg";
import IconSearch from "~/assets/images/search.svg";
import Midia from '~/components/midia';
import IconMenu from "~/assets/images/icon-ellipsis-v-white.svg";

import IconGalery from "~/assets/images/icon-galery.svg";
import IconMic from "~/assets/images/icon-mic.svg";
import IconFile from "~/assets/images/icon-file.svg";

import IconGaleryNotFound from "~/assets/images/icon-galery-notfound.svg";
import IconMicNotFound from "~/assets/images/icon-mic-notfound.svg";
import IconFileNotFound from "~/assets/images/icon-file-notfound.svg";

import IconGaleryInactive from "~/assets/images/icon-galery-inactive.svg";
import IconMicInactive from "~/assets/images/icon-mic-inactive.svg";
import IconFileInactive from "~/assets/images/icon-file-inactive.svg";

export default class Files extends Component {

    state = {
        backToHome: false,
        searching: false,
        query: '',
        selectItems: [],
        filtredItems: [],
        selectedTab: null,
        midias: [],
        token: '',
        sizeSelected: 0,
        addCase: false,
        modalDeleteFile: false,
    }

    componentDidMount() {
        let category = null;

        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });

        if (this.props.route.params) {
            category = this.props.route.params.category;
            this.changeCategory(category);
        }
        if (category) {
            this.setState({
                backToHome: true,
                searching: false,
                selectedTab: category,
            })

        } else {
            this.setState({
                backToHome: true,
                searching: true,
                selectedTab: null,
                filtredItems: [],
            })
        }
        const data = [...this.state.midias];
        this.setState({
            filtredItems: data.filter(e => e.type.includes(category)),
        })
    }

    getToken = async () => {
        const idToken = await ManagerAuth.getToken();

        this.setState({ token: idToken.toString() })
        this.getFiles();
    }

    getFiles = () => {
        ServiceApi.getFiles(this.state.token, (data) => {
            console.log("Arquivos carregados")
            this.setState({ midias: data });
            let category = this.props.route.params.category;
            this.changeCategory(category);
        }, () => {
            console.log("Erro ao buscar arquivos")
        })
    }

    sizeToText = (bytes) => {
        bytes = parseInt(bytes);
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
        if (bytes === 0) return 'n/a'
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
        if (i === 0) return `${bytes} ${sizes[i]})`
        return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
    }

    goBack = (_searching) => {
        if (this.state.backToHome || _searching) {
            this.props.navigation.goBack();
        } else {
            this.setState({ searching: false })
        }
    }

    search = (e) => {
        this.setState({
            backToHome: e,
            searching: true,
        })
    }

    onSelect = (index) => {
        console.log("Select", index);
        const data = [...this.state.midias];
        if (index !== undefined) {
            console.log("Entrou")
            data[index].selected = true;
        } else {
            for (let i = 0; i < data.length; i++) {
                if (data[i].progress == 100) data[i].selected = true;
            }
        }
        this.setState({
            midias: data,
            selectItems: data.filter((e) => e.selected),
            sizeSelected: data.filter((e) => e.selected)?.reduce((acc, e) => acc + e.size, 0)
        });
    };

    onUnselect = (item, index) => {
        console.log("Unselect", index)
        const data = [...this.state.midias];
        if (item !== undefined) {
            data[index].selected = false;
        } else {
            data.forEach(e => {
                e.selected = false
            })
        }
        this.setState({
            midias: data,
            selectItems: data.filter((e) => e.selected),
            sizeSelected: data.filter((e) => e.selected)?.reduce((acc, e) => acc + e.size, 0)
        });
    };

    deleteSelecteds = (e) => {
        this.setState({modalDeleteFile: e})
      }

    changeCategory = (category) => {
        const data = [...this.state.midias];
        this.setState({
            filtredItems: data.filter(e => e.type.includes(category)),
            selectedTab: category,
        });
    }

    render() {
        const HeaderItems = () => {

            const menu2 = {
                optionsContainer: {
                    padding: 0,
                },
                optionWrapper: {
                    margin: 0,
                    padding: 0,
                },
            }

            return (<>
                {this.state.selectItems.length > 0 && <View style={[theme.navigation_bar, { backgroundColor: '#0056CC' }]}>
                    <TouchableOpacity onPress={() => this.onUnselect()}>
                        <IconBack width="32" height="32" />
                    </TouchableOpacity>
                    <View style={{ flexGrow: 1 }}>
                        <Text style={{ fontSize: 14, color: variables.light }}>{this.state.selectItems.length > 1 ? `${this.state.selectItems.length} Arquivos selecionados` : `${this.state.selectItems.length} Arquivo selecionado`}</Text>
                        <Text style={{ fontSize: 11, color: variables.light }}>{this.sizeToText(this.state.sizeSelected)}</Text>
                    </View>
                    <Menu onSelect={func => func()}>
                        <MenuTrigger>
                            <View style={{ width: 30, height: 30, alignItems: 'flex-end', justifyContent: 'center' }}>
                                <IconMenu />
                            </View>
                        </MenuTrigger>
                        <MenuOptions optionsContainerStyle={{ marginTop: 30 }} customStyles={menu2}>
                            {/* <MenuOption value={() => this.onSelect()}>
                                <View style={theme.dropdown_item}>
                                    <Text style={theme.dropdown_item_txt}>Selecionar tudo</Text>
                                </View>
                            </MenuOption> */}
                            <MenuOption value={() => this.setState({ addCase: true })}>
                                <View style={theme.dropdown_item}>
                                    <Text style={theme.dropdown_item_txt}>Adicionar a um caso</Text>
                                </View>
                            </MenuOption>
                            <MenuOption value={() => { this.deleteSelecteds(true) }}>
                                <View style={theme.dropdown_item}>
                                    <Text style={theme.dropdown_item_txt}>Excluir</Text>
                                </View>
                            </MenuOption>
                        </MenuOptions>
                    </Menu>
                </View>}
            </>
            )
        }

        const active_line = {
            height: this.state.selectedTab ? 6 : 0,
            backgroundColor: variables.accent,
            bottom: -3,
            borderRadius: variables.borderRadius,
            width: '33.3%',
            position: 'absolute',
            left: this.state.selectedTab == 'mp4' ? '33.3%' : this.state.selectedTab == 'application' ? '66.6%' : 0,
        }

        return (
            <MenuProvider>
                <View style={theme.container}>
                    {this.state.selectItems.length == 0 && <View style={theme.navigation_bar}>
                        <TouchableOpacity onPress={() => this.goBack(this.state.searching)}>
                            <IconBack width="32" height="32" />
                        </TouchableOpacity>
                        {this.state.searching && <TextInput
                            style={{ flexGrow: 1, height: 40, top: 3, position: 'relative', color: variables.light }}
                            placeholder="Pesquisar..."
                            placeholderTextColor="#E0E1E8"
                            onChangeText={(text) => this.setState({ query: text })}
                            value={this.state.query}
                            autoFocus={true}
                        />}
                        {!this.state.searching && <Text style={[theme.text_title]}>Arquivos</Text>}
                        {this.state.searching && this.state.query.length > 0 && <Icon name="close" color={variables.light} onPress={() => this.setState({ query: '' })} />}
                        {!this.state.searching && <IconSearch name="search" width="24" height="24" onPress={() => this.search(false)} />}
                    </View>}
                    <HeaderItems />
                    <View style={styles.cat_tabs}>
                        <TouchableOpacity style={styles.cat_box} onPress={() => this.changeCategory('image')}>
                            {this.state.selectedTab === 'image' && <IconGalery width="20" height="20" />}
                            {this.state.selectedTab !== 'image' && <IconGaleryInactive width="20" height="20" />}
                            <Text style={[styles.cat_text, { color: this.state.selectedTab === 'image' ? variables.accent : '#A1A6B9' }]}>Galeria</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cat_box} onPress={() => this.changeCategory('mp4')}>
                            {this.state.selectedTab === 'mp4' && <IconMic width="15" height="20" />}
                            {this.state.selectedTab !== 'mp4' && <IconMicInactive width="15" height="20" />}
                            <Text style={[styles.cat_text, { color: this.state.selectedTab === 'mp4' ? variables.accent : '#A1A6B9' }]}>Áudios</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.cat_box, { marginBottom: 0 }]} onPress={() => this.changeCategory('application')}>
                            {this.state.selectedTab === 'application' && <IconFile width="16" height="20" />}
                            {this.state.selectedTab !== 'application' && <IconFileInactive width="16" height="20" />}
                            <Text style={[styles.cat_text, { color: this.state.selectedTab === 'application' ? variables.accent : '#A1A6B9' }]}>Arquivos</Text>
                        </TouchableOpacity>
                        <View style={active_line} />
                    </View>
                    {this.state.filtredItems.length > 0 && this.state.query.length == 0 && <ScrollView>
                        <Midia
                            items={this.state.filtredItems}
                            onSelect={(e) => this.onSelect(e)}
                            onUnselect={(e, index) => this.onUnselect(e, index)}
                            grid={true}
                            showHeader={false}
                            showUploadBox={false}
                            wrap={true}
                            addCase={true}
                            navigation={this.props.navigation}
                            modalCase={this.state.addCase}
                            onChangeModalCase={(e) => this.setState({ addCase: e })}
                            modalDeleteFile={this.state.modalDeleteFile}
                            onChangeModalDeleteFile={(e) => { this.deleteSelecteds(e) }}
                            reloadFiles={() => { this.getFiles() }}
                        />
                    </ScrollView>}
                    {this.state.query.length > 0 &&
                        <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
                            {this.state.selectedTab === 'image' &&
                                <>
                                    <View style={{ padding: variables.paddingHorizontal }}>
                                        <IconGaleryNotFound />
                                    </View>
                                    <Text style={{ textAlign: 'center', fontSize: variables.fontSize, color: variables.gray3 }}>{`Não encontramos nenhuma mídia em \nsua galeria para `}
                                        <Text style={{ color: variables.primary }}>"{this.state.query}"</Text>
                                    </Text>
                                </>
                            }
                            {this.state.selectedTab === 'mp4' &&
                                <>
                                    <View style={{ padding: variables.paddingHorizontal }}>
                                        <IconMicNotFound />
                                    </View>
                                    <Text style={{ textAlign: 'center', fontSize: variables.fontSize, color: variables.gray3 }}>{`Não encontramos nenhuma gravação \npara `}
                                        <Text style={{ color: variables.primary }}>"{this.state.query}"</Text>
                                    </Text>
                                </>
                            }
                            {this.state.selectedTab === 'application' &&
                                <>
                                    <View style={{ padding: variables.paddingHorizontal }}>
                                        <IconFileNotFound />
                                    </View>
                                    <Text style={{ textAlign: 'center', fontSize: variables.fontSize, color: variables.gray3 }}>{`Não encontramos nenhum arquivo \npara `}
                                        <Text style={{ color: variables.primary }}>"{this.state.query}"</Text>
                                    </Text>
                                </>
                            }
                        </View>
                    }
                </View>
            </MenuProvider>
        );
    }
}

const styles = StyleSheet.create({
    cat_tabs: {
        flexDirection: 'row',
        position: 'relative',
    },
    cat_box: {
        flexDirection: 'row',
        height: 56,
        alignItems: 'center',
        paddingHorizontal: variables.paddingHorizontal,
        flex: 3.3,
        borderBottomWidth: 1,
        borderColor: hexToRGB(variables.secondary_light, 0.1),
    },
    cat_text: {
        marginLeft: 10,
        color: '#A1A6B9',
        fontSize: variables.fontSize,
    }
});