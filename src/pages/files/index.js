import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Modal,
  TextInput,
  ScrollView,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import { Icon } from 'react-native-elements';

import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

import { theme, variables, hexToRGB } from '~/theme';

import Preview from '~/components/midia/preview';

import IconClose from '~/assets/images/close-white.svg';
import IconBack from '~/assets/images/arrow-back.svg';
import IconSearch from '~/assets/images/search.svg';
import Midia from '~/components/midia';
import IconMenu from '~/assets/images/icon-ellipsis-v-white.svg';

import IconGalery from '~/assets/images/icon-galery.svg';
import IconMic from '~/assets/images/icon-mic.svg';
import IconFile from '~/assets/images/icon-file.svg';

import IconGaleryInactive from '~/assets/images/icon-galery-inactive.svg';
import IconMicInactive from '~/assets/images/icon-mic-inactive.svg';
import IconFileInactive from '~/assets/images/icon-file-inactive.svg';
import ServiceApi from '~/api/api-caller';
import ManagerAuth from '~/manger-auth/manager';
//import { SlideFromRightIOS } from '@react-navigation/stack/lib/typescript/src/TransitionConfigs/TransitionPresets';

export default class Files extends Component {
  state = {
    selectItems: [],
    sizeSelected: 0,
    addCase: false,
    modalDeleteFile: false,
    midias: [],
    token: '',
  };

  getToken = async () => {
    const idToken = await ManagerAuth.getToken();

    this.setState({ token: idToken.toString() })
    this.getFiles();
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getToken();
    });
  }

  getFiles = () => {
    ServiceApi.getFiles(this.state.token, (data) => {
      console.log("Arquivos carregados")
      this.setState({ midias: data });
    }, () => {
      console.log("Erro ao buscar arquivos")
    })
  }

  saveAsset = async (item, index) => {
    //const token = await ManagerAuth.getToken();
    console.log("TOKEN", this.state.token);
    ServiceApi.uploadFile(this.state.token, item.url, item.type, item.name, (data) => {
      console.log("Arquivo salvo")
      this.state.midias.splice(index, 1, item);
    }, () => {
      console.log("Erro ao salvar arquivo")
      this.state.midias.splice(index, 1);
    })
  }

  onPressBack = () => {
    this.props.navigation.goBack();
  };

  openCategory = (category) => {
    this.props.navigation.push('FileCategory', { category: category });
  };

  onSelect = (index) => {
    console.log("Select", index);
    const data = [...this.state.midias];
    if (index !== undefined) {
      data[index].selected = true;
    } else {
      for (let i = 0; i < data.length; i++) {
        if (data[i].progress == 100) data[i].selected = true;
      }
    }
    this.setState({
      midias: data,
      selectItems: data.filter((e) => e.selected),
      sizeSelected: data.filter((e) => e.selected)?.reduce((acc, e) => acc + e.size, 0)
    });
  };

  onUnselect = (item, index) => {
    console.log("Unselect", index)
    const data = [...this.state.midias];
    if (item !== undefined) {
      data[index].selected = false;
    } else {
      data.forEach(e => {
        e.selected = false
      })
    }
    this.setState({
      midias: data,
      selectItems: data.filter((e) => e.selected),
      sizeSelected: data.filter((e) => e.selected)?.reduce((acc, e) => acc + e.size, 0)
    });
  };

  deleteSelecteds = (e) => {
    this.setState({modalDeleteFile: e})
  }

  sizeToText = (bytes) => {
    bytes = parseInt(bytes);
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
    if (bytes === 0) return 'n/a'
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
    if (i === 0) return `${bytes} ${sizes[i]})`
    return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
  }

  render() {
    const HeaderItems = () => {
      const menu2 = {
        optionsContainer: {
          padding: 0,
        },
        optionWrapper: {
          margin: 0,
          padding: 0,
        },
      };
      return (
        <>
          {this.state.selectItems.length > 0 && (
            <View style={[theme.navigation_bar, { backgroundColor: '#0056CC' }]}>
              <TouchableOpacity onPress={() => this.onUnselect()}>
                <IconBack width="32" height="32" />
              </TouchableOpacity>
              <View style={{ flexGrow: 1 }}>
                <Text style={{ fontSize: 14, color: variables.light }}>
                  {this.state.selectItems.length > 1
                    ? `${this.state.selectItems.length} Arquivos selecionados`
                    : `${this.state.selectItems.length} Arquivo selecionado`}
                </Text>
                <Text style={{ fontSize: 11, color: variables.light }}>
                  {this.sizeToText(this.state.sizeSelected)}
                </Text>
              </View>
              <Menu onSelect={(func) => func()}>
                <MenuTrigger>
                  <View
                    style={{
                      width: 30,
                      height: 30,
                      alignItems: 'flex-end',
                      justifyContent: 'center',
                    }}>
                    <IconMenu />
                  </View>
                </MenuTrigger>
                <MenuOptions
                  optionsContainerStyle={{ marginTop: 30 }}
                  customStyles={menu2}>
                  {/* <MenuOption value={() => this.onSelect()}>
                    <View style={theme.dropdown_item}>
                      <Text style={theme.dropdown_item_txt}>
                        Selecionar tudo
                      </Text>
                    </View>
                  </MenuOption> */}
                  <MenuOption value={() => this.setState({ addCase: true })}>
                    <View style={theme.dropdown_item}>
                      <Text style={theme.dropdown_item_txt}>
                        Adicionar a um caso
                      </Text>
                    </View>
                  </MenuOption>
                  <MenuOption value={() => { this.deleteSelecteds(true) }}>
                    <View style={theme.dropdown_item}>
                      <Text style={theme.dropdown_item_txt}>Excluir</Text>
                    </View>
                  </MenuOption>
                </MenuOptions>
              </Menu>
            </View>
          )}
        </>
      );
    };

    return (
      <MenuProvider>
        <View style={theme.container}>
          {this.state.selectItems.length == 0 && (
            <View style={theme.navigation_bar}>
              <TouchableOpacity onPress={this.onPressBack}>
                <IconClose width="32" height="32" style={{ top: 6 }}></IconClose>
              </TouchableOpacity>
              <Text style={[theme.text_title]}>Arquivos</Text>
              <IconSearch
                name="search"
                width="24"
                height="24"
                onPress={() => this.props.navigation.push('FileCategory')}
              />
            </View>
          )}
          <HeaderItems />
          <ScrollView>
            <View
              style={{
                paddingHorizontal: variables.paddingHorizontal,
                marginTop: variables.paddingHorizontal,
              }}>
              <Text style={theme.title}>Recentes</Text>
            </View>
            <Midia
              items={this.state.midias}
              onSelect={(e) => this.onSelect(e)}
              onUnselect={(e, index) => this.onUnselect(e, index)}
              grid={true}
              showHeader={false}
              showCategories={true}
              onClickCategory={(e) => this.openCategory(e)}
              onAddAsset={(item, index) => this.saveAsset(item, index)}
              addCase={true}
              navigation={this.props.navigation}
              modalCase={this.state.addCase}
              onChangeModalCase={(e) => this.setState({ addCase: e })}
              modalDeleteFile={this.state.modalDeleteFile}
              onChangeModalDeleteFile={(e) => {this.deleteSelecteds(e)}}
              reloadFiles={() => {this.getFiles()}}
            />
          </ScrollView>
        </View>
      </MenuProvider>
    );
  }
}

const styles = StyleSheet.create({
  cat_tabs: {
    flexDirection: 'row',
    position: 'relative',
  },
  cat_box: {
    flexDirection: 'row',
    height: 56,
    alignItems: 'center',
    paddingHorizontal: variables.paddingHorizontal,
    flex: 3.3,
    borderBottomWidth: 1,
    borderColor: hexToRGB(variables.secondary_light, 0.1),
  },
  cat_text: {
    marginLeft: 10,
    color: '#A1A6B9',
    fontSize: variables.fontSize,
  },
  button_continue: {
    marginTop: 40,
    flexDirection: 'row',
    backgroundColor: '#006CFF',
    height: 48,

    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 16,
    marginRight: 16,
  },
  text_style_bold: {
    color: '#ffffff',
    fontSize: 14,
    marginLeft: 24,
    fontFamily: 'Montserrat-SemiBold',
  },
});
