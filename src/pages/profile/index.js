import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ActivityIndicator } from 'react-native';

import { Avatar } from 'react-native-elements'

import IconMenu from "~/assets/images/menu.svg";
import IconBell from "~/assets/images/bell.svg";
import IconRightArrow from "~/assets/images/arrow-right.svg";
import IconMyProfile from "~/assets/images/person-data.svg";
import Enterprise from "~/assets/images/enterprise.svg";
import Hospital from "~/assets/images/hospital.svg";

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';
import { BottomSheetFailure } from '~/components/bottomSheets';

export default class Profile extends Component {
    state = {
        isLoading: true,
        token: '',
        name: '',
        initials: '',
        speciality: '',
        address: '',
        isPicture: false,
        avatar: '',
        modalInfo: false,
        messageInfo: '',
    }

    onPressPersonData = () => {
        this.props.navigation.navigate('PersonData');
    }

    onPressEnterprise = () => {
        this.props.navigation.navigate('CompanyList');
    }

    onPressHospitals = () => {
        this.props.navigation.navigate('HospitalList');
    }

    getToken = async () => {
        const idToken = await ManagerAuth.getToken();

        this.setState({ token: idToken.toString(), isLoading: true })
        this.getUserData();
    }

    setUserInfos(profile) {
        console.log('avatar: ' + profile.avatar)
        this.setState({
            name: profile.name,
            address: profile.city && profile.state ? `${profile.city} - ${profile.state}` : "",
            speciality: profile.expertise ? profile.expertise : "",
            isPicture: profile.avatar ? true : false,
            avatar: `${profile.avatar}?v1.${new Date()}`,
            initials: profile.name ? profile.name.split(" ").map((n) => n[0]).join(".") : "",
            isLoading: false
        })
    }
    getUserData() {
        ServiceApi.getProfile(this.state.token, (data) => {
            this.setUserInfos(data.profile);
        }, (error) => {
            // tratar o erro ou disparar erro default
            this.setState({
                isLoading: false, modalInfo: true, messageInfo: error
            })
        }, this.props);
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getToken();
        });
    }

    render() {
        const navigation = this.props.navigation

        return (
            <View style={styles.container}>

                <View style={styles.navigation_bar}>
                    <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <IconMenu width="32" height="32"></IconMenu>
                    </TouchableOpacity>
                    <Text style={[styles.text_title]}>Conta</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Notifications')} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <IconBell width="32" height="32"></IconBell>
                    </TouchableOpacity>
                </View>

                {this.state.isLoading
                    ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#4493FF" />
                    </View>
                    :
                    <View>
                        <View style={styles.container_infos}>
                            {this.state.name == '' ? null
                                : (
                                    this.state.isPicture ?
                                        <Avatar
                                            rounded
                                            size="medium"
                                            source={{
                                                uri:
                                                    this.state.avatar,
                                            }}
                                        />
                                        :
                                        <Avatar
                                            size="medium"
                                            rounded
                                            title={this.state.initials}
                                            avatarStyle={{ backgroundColor: '#d3d3d3', opacity: 0.7 }}
                                            titleStyle={{ color: 'black', padding: 5, fontSize: 13 }}
                                        />
                                )
                            }
                            <View style={styles.container_sub_infos}>
                                <Text style={styles.title_person_data}>{this.state.name}</Text>
                                <Text style={styles.title2_person_data}>{this.state.speciality}</Text>
                                <Text style={styles.title2_person_data}>{this.state.address}</Text>
                            </View>

                        </View>

                        <TouchableOpacity style={[styles.line, { marginTop: 30 }]} onPress={this.onPressPersonData}>
                            <View style={styles.container_line}>
                                <IconMyProfile width="24" height="24"></IconMyProfile>
                                <Text style={[styles.title_person_data, { marginLeft: 16 }]}>Dados Pessoais</Text>
                            </View>
                            <IconRightArrow width="5" height="9" style={styles.arrow_right}></IconRightArrow>
                        </TouchableOpacity>

                        <View style={styles.line_space}></View>

                        <TouchableOpacity style={styles.line} onPress={this.onPressEnterprise}>
                            <View style={styles.container_line}>
                                <Enterprise width="24" height="24"></Enterprise>
                                <Text style={[styles.title_person_data, { marginLeft: 16 }]}>Empresa</Text>
                            </View>
                            <IconRightArrow width="5" height="9" style={styles.arrow_right}></IconRightArrow>
                        </TouchableOpacity>

                        <View style={styles.line_space}></View>

                        <TouchableOpacity style={styles.line} onPress={this.onPressHospitals}>
                            <View style={styles.container_line}>
                                <Hospital width="24" height="24"></Hospital>
                                <Text style={[styles.title_person_data, { marginLeft: 16 }]}>Hospitais</Text>
                            </View>
                            <IconRightArrow width="5" height="9" style={styles.arrow_right}></IconRightArrow>
                        </TouchableOpacity>
                    </View>
                }

                <BottomSheetFailure
                    visible={this.state.modalInfo}
                    text={this.state.messageInfo}
                    onClose={() => { this.setState({ modalInfo: false }); }}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
    container_infos: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center',
    },
    container_sub_infos: {
        flexDirection: 'column',
        marginLeft: 20
    },
    line: {
        height: 77,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    container_line: {
        flexDirection: 'row',
        marginLeft: 16,
    },
    arrow_right: {
        marginRight: 16
    },
    line_space: {
        backgroundColor: 'black',
        opacity: 0.1,
        height: 1,
        marginLeft: 16,
        marginRight: 16
    },
    title_person_data: {
        fontSize: 16,
        color: '#364574',
        fontFamily: 'Montserrat-Bold',
        justifyContent: 'center',
    },
    title2_person_data: {
        fontSize: 14,
        color: '#707B9F',
        fontFamily: 'Montserrat-Regular'
    }
});