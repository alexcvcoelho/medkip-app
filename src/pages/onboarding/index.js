import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground, Text, TouchableOpacity, PermissionsAndroid, Platform, BackHandler } from 'react-native';

import Logo from "~/assets/images/logo_onboarding.svg";

import IconPhone from "~/assets/images/phone.svg";
import IconCoffee from "~/assets/images/coffee.svg";
import IconCash from "~/assets/images/cash.svg";
import IconBolt from "~/assets/images/bolt.svg";

import IconArrowRight from "~/assets/images/arrow_right.svg";

export default class Onboarding extends Component {
    state = {

    }

    onPressContinue = () => {
        this.props.navigation.navigate('Login');
    }

    requestAllPermissions = async () => {
        if (Platform.OS === "android") {
            try {
                const granted = await PermissionsAndroid.requestMultiple([
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                ]);

                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("You can use all permissons");
                } else {
                    console.log("Permission denied");
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.requestAllPermissions()
        });
        //BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        //BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }
    render() {

        return (
            <ImageBackground style={[styles.container]} source={require('~/assets/images/onboarding.png')} >
                <View style={styles.center_container}>
                    <Logo width="48" height="48" />
                    <Text style={[styles.text_title, { marginTop: 20 }]}>Medkip</Text>
                    <Text style={styles.text_title2}>Idealizado por Médicos</Text>
                    <Text style={styles.text_title2}>Feito por Médicos</Text>
                    <View style={styles.container_line}>
                        <IconCash width="10" height="18"></IconCash>
                        <Text style={styles.text_style}>Controle Financeiro</Text>
                    </View>
                    <View style={styles.container_line}>
                        <IconPhone width="11" height="16"></IconPhone>
                        <Text style={styles.text_style}>Calendário de Plantões</Text>
                    </View>
                    <View style={styles.container_line}>
                        <IconBolt width="14" height="16"></IconBolt>
                        <Text style={styles.text_style}>Armazenamento de Casos</Text>
                    </View>
                    <View style={styles.container_line}>
                        <IconCoffee width="15" height="15"></IconCoffee>
                        <Text style={styles.text_style}>Tranquilidade para você</Text>
                    </View>
                    <TouchableOpacity style={styles.button_continue} onPress={this.onPressContinue}>
                        <Text style={styles.text_style_bold}>Continuar</Text>
                        <IconArrowRight style={styles.arrow} />
                    </TouchableOpacity>

                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#202945',
        justifyContent: 'center',
        resizeMode: 'cover'
    },
    center_container: {
        marginLeft: 40,
        width: '80%'
    },
    container_line: {
        marginTop: 30,
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center'
    },
    text_title: {
        color: '#ffffff',
        fontSize: 22,
        fontFamily: 'Montserrat-Bold'
    },
    text_title2: {
        color: '#ffffff',
        fontSize: 20,
        fontFamily: 'Montserrat-Regular'
    },
    text_style: {
        color: '#ffffff',
        fontSize: 14,
        marginLeft: 24,
        fontFamily: 'Montserrat-Regular'
    },
    text_style_bold: {
        color: '#ffffff',
        fontSize: 14,
        marginLeft: 24,
        fontFamily: 'Montserrat-SemiBold'
    },
    button_continue: {
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: '#006CFF',
        height: 48,
        width: '100%',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    arrow: {
        marginRight: 24
    }
});