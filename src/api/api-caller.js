import Axios from 'axios';
import ManagerAuth from '~/manger-auth/manager';
import { ResponseProfile } from '~/model/ResponseProfile';

const baseURL = 'https://viasrwpom0.execute-api.us-east-1.amazonaws.com/v1';
const contentType = 'application/json';


export default class ServiceApi {

    static showError = (error, onFailure, props) => {
        try {
            console.log('erro na requisicao: ' + error.message);
            console.log('status code: ' + error.response.status);
            if (error.response.status == 403) {
                if (props) {
                    ManagerAuth.logout(props)
                } else {
                    onFailure("Não foi possível completar a solicitação. Tente novamente mais tarde!");
                }
            } else {
                onFailure("Não foi possível completar a solicitação. Tente novamente mais tarde!");
            }
        } catch {
            onFailure("Não foi possível completar a solicitação. Tente novamente mais tarde!");
        }
    }
    /* 
        PROFILE DATA 

        GET PROFILE
        UPLOAD AVATAR PROFILE
        SAVE PROFILE
    */


    // GET PROFILE
    static getProfile = (token, onSuccess, onFailure, props) => {
        Axios({
            "method": "GET",
            "url": baseURL + "/profile",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            if (response.data) {
                const json = JSON.stringify(response.data).toString();

                console.log('response body: ' + json);

                const data = new ResponseProfile(response.data);

                onSuccess(data);
            } else {
                onFailure(null)
            }

        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props);
        })
    }

    // UPLOAD AVATAR PROFILE
    static uploadAvatar = (token, dataPicture, onSuccess, onFailure, props) => {
        Axios({
            "method": "POST",
            "url": baseURL + "/profile",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": {
                "mime": "image/jpeg",
                "image": "base64, " + dataPicture.base64,
            }
        }).then((response) => {
            /*
            {
                "imgUrl": "https://medkip-avatar-642514536731-us-east-1-storage.s3.amazonaws.com/uploads/117463040776664226989.png"
            }
            */
            onSuccess();
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props);
        })
    }

    // SAVE PROFILE
    static saveProfile = (token, bio, city, college, crm, expertise, name, state, onSuccess, onFailure, props) => {
        Axios({
            "method": "PATCH",
            "url": baseURL + "/profile",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": {
                "bio": bio,
                "city": city,
                "college": college,
                "crm": crm,
                "expertise": expertise,
                "name": name,
                "state": state
            }
        }).then((response) => {
            //response.data 
            /*
            {
                "id": "117463040776664226989",
                "profile": {
                    "bio": "Pequeno grande homem",
                    "city": "Sorocaba",
                    "college": "Engenheiro de Computação",
                    "crm": "1234",
                    "expertise": "Analista de Sistemas",
                    "name": "Juan",
                    "state": "SP",
                    "avatar": null
                }
            }
            */
            onSuccess()
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props);
        })
    }


    /* 
        COMPANY DATA
        
        GET COMPANY
        SAVE COMPANY
        DELETE COMPANY
    */

    // GET COMPANY
    static getCompanies = (token, onSuccess, onFailure, props) => {
        Axios({
            "method": "GET",
            "url": baseURL + "/company",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('lista empresas')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    // SAVE COMPANY
    static saveCompany = (token, id, name, cnpj, members, tax, aliquot, onSuccess, onFailure, props) => {
        console.log(name + " " + cnpj + " " + members + " " + tax + " " + aliquot + " ")

        const add = {
            "name": name,
            "cnpj": cnpj,
            "members": members,
            "tax": tax,
            "aliquot": aliquot
        }

        const edit = {
            "id": id,
            "name": name,
            "cnpj": cnpj,
            "members": members,
            "tax": tax,
            "aliquot": aliquot
        }

        Axios({
            "method": "POST",
            "url": baseURL + "/company",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": id == null ? add : edit
        }).then((response) => {
            //response.data 
            console.log('empresa salva com sucesso')
            onSuccess();
        }).catch((error) => {
            console.log(error.message)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    // DELETE COMPANY
    static deleteCompany = (token, id, onSuccess, onFailure, props) => {
        Axios({
            "method": "DELETE",
            "url": baseURL + `/company?id=${id}`,
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('empresa apagada com sucesso')
            onSuccess();
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props)
        })
    }



    /**
        HOSPITALS DATA 

        GET HOSPITALS
        DELETE HOSPITALS
        SAVE HOSPITALS
    */


    // GET HOSPITALS
    static getHospitals = (token, onSuccess, onFailure, props) => {
        Axios({
            "method": "GET",
            "url": baseURL + "/hospital",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('lista hospitais')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    // DELETE HOSPITALS
    static deleteHospital = (token, id, onSuccess, onFailure, props) => {
        Axios({
            "method": "DELETE",
            "url": baseURL + `/hospital?id=${id}`,
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('empresa apagada com sucesso')
            onSuccess();
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props)
        })
    }

    // SAVE HOSPITALS
    static saveHospital = (token, id, name, city, iss, onSuccess, onFailure, props) => {

        const add = {
            "name": name,
            "city": city,
            "iss": iss,
        }

        const edit = {
            "id": id,
            "name": name,
            "city": city,
            "iss": iss,
        }

        Axios({
            "method": "POST",
            "url": baseURL + "/hospital",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": id == null ? add : edit
        }).then((response) => {
            //response.data 
            console.log('hospital salvo com sucesso')
            onSuccess();
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props)
        })
    }

    /**
        CASES DATA 
    */
    static getCases = (token, onSuccess, onFailure, props) => {
        Axios({
            "method": "GET",
            "url": baseURL + "/case",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            console.log('lista casos')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    // SAVE CASE
    static saveCase = (token, id, date, hospitalId, risk, description, name, type, title, onSuccess, onFailure, props) => {
        const add = {
            "date": date,
            "hospitalId": hospitalId,
            "risk": risk,
            "description": description,
            "name": name,
            "type": type,
            "title": title
        }

        const edit = {
            "id": id,
            "date": date,
            "hospitalId": hospitalId,
            "risk": risk,
            "description": description,
            "name": name,
            "type": type,
            "title": title
        }

        Axios({
            "method": "POST",
            "url": baseURL + "/case",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": id == null ? add : edit
        }).then((response) => {
            //response.data 
            console.log('caso salvo com sucesso')
            onSuccess(response.data);
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props)
        })
    }

    static uploadCaseFile = (token, base64, type, name, caseId, onSuccess, onFailure, props) => {
        if(!/^data:.*;/.test(base64))
            return
        Axios({
            "method": "POST",
            "url": baseURL + "/case/upload",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": {
                "caseId": caseId,
                "name": name,
                "type": type, //"image/jpeg",
                "data": base64.replace(/^data:.*;/, ''),
            }
        }).then((response) => {
            onSuccess(response);
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props);
        })
    }

    static getScheduleDetails = (token, id, onSuccess, onFailure, props) => {
        Axios({
            "method": "GET",
            "url": baseURL + '/schedule?id=' + id,
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('schedule item details')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    static getSchedules = (token, startDate, endDate, onSuccess, onFailure, props) => {
        var url = ''
        if (startDate != '' && endDate != '') {
            url = baseURL + `/schedule?startDate=${startDate}&endDate=${endDate}`
        } else {
            url = baseURL + `/schedule`
        }
        Axios({
            "method": "GET",
            "url": url,
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('lista plantoes')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    static saveSchedule = (token,
        id, hospitalId, title, startDate, amountOfHours, recurrenceType, daysOfWeek, isMontlyFixedDay, amount, paymentScheme, estimatedDaysForPayment,
        onSuccess, onFailure, props) => {

        console.log('body post:')
        console.log(token)
        console.log(id)
        console.log(hospitalId)
        console.log(title)
        console.log(startDate)
        console.log(amountOfHours)
        console.log(recurrenceType)
        console.log(daysOfWeek)
        console.log(isMontlyFixedDay)
        console.log(amount)
        console.log(paymentScheme)
        console.log(estimatedDaysForPayment)
        console.log(token)

        const add = {
            "hospitalId": hospitalId,
            "title": title,
            "startDate": startDate,
            "amountOfHours": amountOfHours,
            "recurrenceType": recurrenceType, //7, 15 ou 30
            "daysOfWeek": daysOfWeek, //de 1(domingo) a 7(sabado)
            "isMontlyFixedDay": isMontlyFixedDay,
            "amount": amount,
            "paymentScheme": paymentScheme, //CPF ou CNPJ
            "estimatedDaysForPayment": estimatedDaysForPayment
        }

        const edit = {
            "id": id,
            "hospitalId": hospitalId,
            "title": title,
            "startDate": startDate,
            "amountOfHours": amountOfHours,
            "recurrenceType": recurrenceType, //7, 15 ou 30
            "daysOfWeek": daysOfWeek, //de 1(domingo) a 7(sabado)
            "isMontlyFixedDay": isMontlyFixedDay,
            "amount": amount,
            "paymentScheme": paymentScheme, //CPF ou CNPJ
            "estimatedDaysForPayment": estimatedDaysForPayment
        }

        console.log(JSON.stringify(add).toString())

        Axios({
            "method": "POST",
            "url": baseURL + "/schedule",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": id == null ? add : edit,
        }).then((response) => {
            //response.data 
            console.log('schedule salvo com sucesso')
            onSuccess();
        }).catch((error) => {
            console.log('erro schedule salvar')
            ServiceApi.showError(error, onFailure, props)
        })
    }

    static deleteSchedule = (token, id, onSuccess, onFailure, props) => {
        Axios({
            "method": "DELETE",
            "url": baseURL + `/schedule?id=${id}`,
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('plantao apagado com sucesso')
            onSuccess();
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props)
        })
    }

    static uploadFile = (token, dataPicture, type, name, onSuccess, onFailure, props) => {
        if(!/^data:.*;/.test(dataPicture))
            return
        Axios({
            "method": "POST",
            "url": baseURL + "/file/upload",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": {
                "name": name,
                "type": type, //"image/jpeg",
                "data": dataPicture.replace(/^data:.*;/, ''),
            }
        }).then((response) => {
            console.log('sucesso upload arquivo')
            onSuccess(response.data);
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props);
        })
    }

    static copyFileToCase = (token, key, caseId, onSuccess, onFailure, props) => {
        Axios({
            "method": "POST",
            "url": baseURL + "/file/copy",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": {
                "key": key,
                "caseId": caseId
            }
        }).then((response) => {
            console.log('sucesso na cópia do arquivo')
            onSuccess(response.data);
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props);
        })
    }

    static getFiles = (token, onSuccess, onFailure, props) => {
        Axios({
            "method": "GET",
            "url": baseURL + "/file",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            console.log('lista arquivos')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    static deleteFile = (token, key, onSuccess, onFailure, props) => {
        Axios({
            "method": "DELETE",
            "url": baseURL + `/file?key=${key}`,
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('arquivo apagado com sucesso')
            onSuccess();
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props)
        })
    }

    static renameFile = (token, key, newName, onSuccess, onFailure, props) => {
        Axios({
            "method": "PUT",
            "url": baseURL + `/file?key=${key}`,
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }, "data": {
                "newName": newName,
            }
        }).then((response) => {
            console.log('arquivo renomeado com sucesso')
            onSuccess(response.data);
        }).catch((error) => {
            ServiceApi.showError(error, onFailure, props)
        })
    }


    static financialGetDashNextOrdeal = (token, onSuccess, onFailure, props) => {
        Axios({
            "method": "GET",
            "url": baseURL + "/finance/next",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('dash financial next ordeal')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    // financial apis

    static financialGetDashDataChart = (token, onSuccess, onFailure, props) => {
        Axios({
            "method": "GET",
            "url": baseURL + "/finance/chart",
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('dash financial data chart')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }

    static financialGetDashDataGrid = (token, period, onSuccess, onFailure, props) => {

        Axios({
            "method": "GET",
            "url": baseURL + "/finance/grid?period=" + period,
            "headers": {
                "content-type": contentType,
                "Authorization": `Bearer ${token}`
            }
        }).then((response) => {
            //response.data 
            console.log('dash financial grid')
            onSuccess(response.data)
        }).catch((error) => {
            console.log(error)
            ServiceApi.showError(error, onFailure, props)
        })
    }
}

