// ./App.js

import React from "react";
import { NavigationContainer } from "@react-navigation/native";

import { MainStackNavigator } from "./pages/stack-navigation";

import { GoogleSignin } from '@react-native-community/google-signin';

import { SafeAreaProvider } from 'react-native-safe-area-context';

import { LogBox } from 'react-native';
import { PlayerContextProvider } from "./components/context";


/** Register GoogleSignIn */
GoogleSignin.configure({
    webClientId:
        '792023139324-7idn08tli5osfk9g4cl941ak2sivv3on.apps.googleusercontent.com',
    offlineAccess: false,
    forceCodeForRefreshToken: true,
});

const App = () => {
    LogBox.ignoreAllLogs()
    return (
        <SafeAreaProvider>
            <PlayerContextProvider>
                <NavigationContainer>
                    <MainStackNavigator />
                </NavigationContainer>
            </PlayerContextProvider>
        </SafeAreaProvider>
    );
}
export default App;