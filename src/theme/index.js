import hexToRGB from '~/theme/utils';
import variables from '~/theme/variables';

import { StyleSheet } from 'react-native';

const theme = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: variables.light,
    },
    navigation_bar: {
        height: 96,
        backgroundColor: variables.secondary_dark,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: variables.light,
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
    title: {
        color: variables.secondary,
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 14,
        margin: 0,
    },
    chips: {
        flexDirection: 'row',
        marginTop: variables.paddingHorizontal,
    },
    chip: {
        backgroundColor: variables.primary,
        padding: 6,
        borderRadius: 4,
        flexDirection: 'row',
        flex: 0.5,
        marginRight: 8,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    chip_txt: {
        color: variables.light,
        fontSize: 12,
        flex: 0.9,
    },
    dropdown_item: {
        padding: variables.paddingHorizontal,
        backgroundColor: variables.light,
    },
    dropdown_item_txt: {
        fontSize: variables.fontSize,
        color: variables.secondary,
    },
    modalOverlay: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
        flexDirection: 'row',
        backgroundColor: hexToRGB(variables.secondary_dark, 0.7)
    },
    modalView: {
        flex: 1,
        margin: 20,
        backgroundColor: variables.light,
        borderRadius: 4,
        paddingHorizontal: 16,
        paddingVertical: 16,
    },
    bsOverlay: {
        position: 'absolute', 
        left: 0, 
        right: 0, 
        top: -30, 
        bottom: 0,
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: "flex-end",
        marginTop: 22,
        backgroundColor: hexToRGB(variables.secondary_dark, 0.7),
    },
    bsView: {
        flex: 1,
        backgroundColor: variables.light,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        paddingHorizontal: variables.paddingHorizontal,
        paddingVertical: variables.paddingHorizontal,
    },
    fab_button_container: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: variables.paddingHorizontal,
        right: variables.paddingHorizontal,
        left: variables.paddingHorizontal,
        backgroundColor: hexToRGB(variables.accent, 0.15),
        borderRadius: 56 / 2,
        justifyContent: 'flex-end',
    },
    fab_button: {
        width: 56,
        height: 56,
        borderRadius: 56 / 2,
        backgroundColor: variables.primary,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: variables.primary,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.6,
        shadowRadius: 24,

        elevation: 10,
    },
    fab_button_hint: {
        flexGrow: 1,
        paddingHorizontal: 28,
        paddingVertical: 10,
        color: variables.primary,
    }
})

export { 
    variables,
    hexToRGB,
    theme
};