const primary = '#006CFF';
const secondary = '#364574';
const secondary_dark = '#202945';
const secondary_light = '#707B9F';
const accent = '#4493FF';
const danger = '#F71C5D';
const light = '#ffffff';
const gray2 = '#626B8A';
const gray3 = '#8189A1';
const paddingHorizontal = 16;

const variables = {
    primary,
    secondary,
    secondary_dark,
    secondary_light,
    accent,
    danger,
    light,
    gray2,
    gray3,
    paddingHorizontal,
    paddingVertical: 12,
    borderRadius: 6,
    fontFamily: 'Montserrat-Regular',
    fontSize: 14,
    fontBold: 'Montserrat-SemiBold',
    inputHeight: 48,
}

export default variables;