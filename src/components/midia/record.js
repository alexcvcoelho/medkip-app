import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Animated, Image, Modal, Dimensions, FlatList } from 'react-native';

import { Card, ListItem, Button, Icon, Badge, Divider, CheckBox } from 'react-native-elements';

import { SimpleBottomSheet, SimpleModal, BottomSheetConfirm, BottomSheetSuccess } from '~/components/bottomSheets';
import Player from '~/components/midia/player';


import { theme, variables, hexToRGB } from '~/theme';
const RNFS = require('react-native-fs');

import IconMic from "~/assets/images/icon-mic-white.svg";
import IconPlay from "~/assets/images/icon-play-white.svg";

import AudioRecorderPlayer, {
  AVEncoderAudioQualityIOSType,
  AVEncodingOption,
  AudioEncoderAndroidType,
  AudioSet,
  AudioSourceAndroidType,
} from 'react-native-audio-recorder-player';
import { usePlayer } from '../context';
import moment from 'moment';

export default class record extends Component {

  //const { updateFile } = usePlayer()
  //updateFile()

  constructor(props) {
    super(props);
    this.state = {
      isLoggingIn: false,
      recordSecs: 0,
      recordTime: '00:00:00',
      currentPositionSec: 0,
      currentDurationSec: 0,
      playTime: '00:00:00',
      duration: '00:00:00',
      path: '/data/user/0/com.medkip.app/files/sound.mp4',
      resultPath: "",
      min: 0,
      max: 0,
      status: 0,
      save: false,
      cancel: false,
    };
    this.audioRecorderPlayer = new AudioRecorderPlayer();
    this.audioRecorderPlayer.setSubscriptionDuration(0.09); // optional. Default is 0.1
  }

  onStartRecord = async () => {
    // const path = 'sdcard/sound.mp4';
    const audioSet = {
      AudioEncoderAndroid: AudioEncoderAndroidType.AAC,
      AudioSourceAndroid: AudioSourceAndroidType.MIC,
      AVEncoderAudioQualityKeyIOS: AVEncoderAudioQualityIOSType.high,
      AVNumberOfChannelsKeyIOS: 2,
      AVFormatIDKeyIOS: AVEncodingOption.aac,
    };

    console.log('audioSet', audioSet);
    const result = await this.audioRecorderPlayer.startRecorder(this.state.path, audioSet);
    this.audioRecorderPlayer.addRecordBackListener((e) => {
      this.setState({
        recordSecs: e.currentPosition,
        recordTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.currentPosition),
        ),
        status: 1
      });
      return;
    });
    this.setState({resultPath: result});
    console.log(result);
  };

  onStopRecord = async () => {
    const result = await this.audioRecorderPlayer.stopRecorder();
    this.audioRecorderPlayer.removeRecordBackListener();
    this.setState({
      recordSecs: 0,
      recordTime: '00:00:00',
      save: true
    });
    const base64 = await RNFS.readFile(this.state.resultPath, 'base64');
    const info = await RNFS.stat(this.state.resultPath)
    const asset = {
      url: `data:video/mp4;base64,${base64}`,
      //uri: info.path,
      size: info.size,
      type: 'video/mp4',
      name: `AUD_${moment().format("YYYY-MM-DDTHH:mm:ss.SSS")}`,
      //duration: this.state.duration,
  }
    this.props.onAddFile(asset);
  };

  onPauseRecord = async () => {
    console.log("Pause audio")
    await this.audioRecorderPlayer.pauseRecorder();
    this.setState({ status: 2 });
  };

  onResumeRecord = async () => {
    await this.audioRecorderPlayer.resumeRecorder();
    this.setState({ status: 1 })
  }

  onCancelRecord = async () => {
    console.log("Cancel record");
    const result = await this.audioRecorderPlayer.stopRecorder();
    this.audioRecorderPlayer.removeRecordBackListener();
    this.setState({
      recordSecs: 0,
      recordTime: '00:00:00',
      status: 0,
      cancel: false
    });
  }

  render() {
    if (!this.state.save) {
      return (
        <View style={styles.page}>
          <View style={styles.content}>
            <View>
              <Text style={styles.time}>{this.state.recordTime}</Text>
              <Text style={styles.status}>{this.state.status == 0 ? 'Comece a gravar' : this.state.status == 2 ? 'Pausado' : ''}</Text>
            </View>
          </View>
          <View style={styles.buttons_container}>
            <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => this.setState({ cancel: true })}>
              <View style={[theme.fab_button, styles.btn]}>
                <Icon name="close" color={variables.primary} />
              </View>
              <Text style={styles.btn_txt}>Cancelar</Text>
            </TouchableOpacity>
            {this.state.status == 0 && <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => this.onStartRecord()}>
              <View style={[theme.fab_button, styles.rec_btn]}>
                <IconMic width="16" height="26" />
              </View>
              <Text style={styles.btn_txt}>Gravar</Text>
            </TouchableOpacity>}
            {this.state.status == 1 && <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => this.onPauseRecord()}>
              <View style={[theme.fab_button, styles.play_btn]}>
                <Icon name="pause" color={variables.light} />
              </View>
              <Text style={styles.btn_txt}>Pausar</Text>
            </TouchableOpacity>}
            {this.state.status == 2 && <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => this.onResumeRecord()}>
              <View style={[theme.fab_button, styles.play_btn]}>
                <IconPlay style={{ marginLeft: 2 }} />
              </View>
              <Text style={styles.btn_txt}>Continuar</Text>
            </TouchableOpacity>}
            <TouchableOpacity style={{ alignItems: 'center' }} disabled={this.state.status == 0} onPress={() => this.onStopRecord()}>
              <View style={[theme.fab_button, styles.btn]}>
                <Icon name="check" color={variables.primary} />
              </View>
              <Text style={styles.btn_txt}>Salvar</Text>
            </TouchableOpacity>
          </View>
          <SimpleModal visible={this.state.cancel} onClose={() => this.setState({ cancel: false })}>
            <Text style={styles.title}>Cancelar a gravação?</Text>
            <Text style={styles.paragraph}>A gravação será excluída.</Text>
            <View style={{ height: variables.paddingHorizontal }} />
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 0.5, marginRight: variables.paddingHorizontal }}>
                <Button
                  title="Sim"
                  disabled={false}
                  buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                  titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                  disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                  disabledTitleStyle={{ color: variables.light }}
                  onPress={() => this.onCancelRecord()}
                />
              </View>
              <View style={{ flex: 0.5 }}>
                <Button
                  title="Não"
                  disabled={false}
                  buttonStyle={{ backgroundColor: variables.light, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight, borderWidth: 1 }}
                  titleStyle={{ color: variables.primary, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                  disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                  disabledTitleStyle={{ color: variables.light }}
                  onPress={() => this.setState({ cancel: false })}
                />
              </View>
            </View>
          </SimpleModal>
        </View>
      )
    } else {
      return (
        <View style={[styles.page, { justifyContent: 'center' }]}>
          <View>
            <TextInput value="Gravação 01" style={{ alignSelf: 'center', textAlign: 'center', color: variables.gray3, fontSize: 16, borderBottomColor: variables.gray3, borderBottomWidth: 1, paddingHorizontal: 8, paddingVertical: 3, marginBottom: 12 }} />
            <Player path={this.state.resultPath} style={{ paddingVertical: 16 }} />
            <Text style={{ alignSelf: 'center', textAlign: 'center', color: variables.gray2, fontSize: 16 }}>Áudio salvo nas suas gravações</Text>
            <View style={{ height: variables.paddingHorizontal }} />
            <Button
              title="Voltar"
              disabled={false}
              buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
              titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
              disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
              disabledTitleStyle={{ color: variables.light }}
              onPress={() => this.props.onClose()}
            />
            <View style={{ height: variables.paddingHorizontal }} />
            <Button
              title="Gravar outro áudio"
              disabled={false}
              buttonStyle={{ backgroundColor: variables.light, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight, borderWidth: 1 }}
              titleStyle={{ color: variables.primary, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
              disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
              disabledTitleStyle={{ color: variables.light }}
              onPress={() => this.setState({ save: false })}
            />
          </View>
        </View>
      )
    }
  }
}
const styles = StyleSheet.create({
  //meus estilos
  page: {
    flexGrow: 1,
    backgroundColor: '#F5F6FA',
    padding: variables.paddingHorizontal,
    position: 'relative',
  },
  title: {
    color: variables.secondary,
    fontFamily: variables.fontBold,
    fontSize: variables.fontSize,
    margin: 0,
    textAlign: 'center',
  },
  paragraph: {
    fontSize: variables.fontSize,
    color: variables.gray2,
    marginVertical: 8,
    textAlign: 'center',
  },
  content: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  time: {
    fontSize: 42,
    color: variables.secondary,
    fontFamily: variables.fontBold,
    textAlign: 'center',
  },
  status: {
    fontSize: 16,
    color: variables.gray2,
    textAlign: 'center',
    marginTop: 8,
  },
  buttons_container: {
    // position: 'absolute',
    // width: '100%',
    padding: 49 - variables.paddingHorizontal,
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    flexDirection: 'row',
  },
  btn: {
    width: 44,
    height: 44,
    borderRadius: 44 / 2,
    elevation: 0,
    backgroundColor: hexToRGB(variables.primary, 0.2),
  },
  rec_btn: {
    backgroundColor: '#D23D58',
    shadowColor: '#D23D58',
  },
  play_btn: {
    backgroundColor: '#D23D58',
    borderWidth: 8,
    borderColor: hexToRGB('#D23D58', 0.2),
    width: 72 - 8,
    height: 72 - 8,
    borderRadius: 72 - 8 / 2,
    elevation: 0,
  },
  btn_txt: {
    color: variables.gray3,
    marginTop: 12,
    fontSize: 12,
  }
})