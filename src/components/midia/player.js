import React, { Component } from 'react'
import { Text, View } from 'react-native'

import { Card, ListItem, Button, Icon, Badge, Divider, Slider } from 'react-native-elements';

import { theme, variables, hexToRGB } from '~/theme';

import AudioRecorderPlayer from 'react-native-audio-recorder-player';
import { TouchableOpacity } from 'react-native';

import IconPlay from "~/assets/images/icon-play.svg";


export default class player extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoggingIn: false,
            recordSecs: 0,
            recordTime: '00:00',
            currentPositionSec: 0,
            currentDurationSec: 0,
            playTime: '00:00',
            duration: '00:00',
            path: '/data/user/0/com.medkip.app/files/sound.mp4',
            status: 0
        };
        this.audioRecorderPlayer = new AudioRecorderPlayer();
        this.audioRecorderPlayer.setSubscriptionDuration(0.09); // optional. Default is 0.1
    };

    onStartOrPauseOrResume = async () => {
        console.log(this.state.status)
        if (this.state.status == 0)
            this.onStartPlay();
        else if (this.state.status == 1)
            this.onPausePlay();
        else if (this.state.status == 2)
            this.onResumePlay();

    };

    onStartPlay = async (e) => {
        console.log('onStartPlay');
        const msg = await this.audioRecorderPlayer.startPlayer(this.props.path);
        this.audioRecorderPlayer.addPlayBackListener((e) => {
            const playTime = this.audioRecorderPlayer.mmssss(Math.floor(e.currentPosition));
            const duration = this.audioRecorderPlayer.mmssss(Math.floor(e.duration))
            const statusNow = this.state.status == 2 ? 2 : duration == playTime ? 0 : 1;
            this.setState({
                currentPositionSec: e.currentPosition,
                currentDurationSec: e.duration,
                playTime: playTime.substr(3),
                duration: duration.substr(3),
                status: statusNow
            });
            return;
        });
    };

    onPausePlay = async () => {        
        this.setState({ status: 2 });
        await this.audioRecorderPlayer.pausePlayer();
        console.log("Pause Player ", this.state.status)
    };

    onResumePlay = async () => {        
        await this.audioRecorderPlayer.resumePlayer();
        this.setState({ status: 1 });
        console.log("Resume Player", this.state.status)
    };

    render() {

        return (
            <TouchableOpacity onPress={() => this.onStartOrPauseOrResume()} {...this.props}>
                <View style={{ flexDirection: 'row', alignItems: 'center', height: 32 }}>
                    <View style={{ width: 32 }}>
                        {this.state.status != 1 && <IconPlay />}
                        {this.state.status == 1 && <Icon name="pause" color={variables.secondary_dark} />}
                    </View>
                    <View style={{ flexGrow: 1 }}>
                        <Slider
                            value={this.state.currentPositionSec}
                            maximumValue={this.state.currentDurationSec}
                            minimumValue={0}
                            step={1}
                            style={{ height: 10 }}
                            maximumTrackTintColor={hexToRGB(variables.secondary_light, 0.4)}
                            minimumTrackTintColor={variables.primary}
                            trackStyle={{ height: 4, backgroundColor: 'transparent' }}
                            thumbStyle={{ height: 10, width: 10, backgroundColor: variables.primary }}
                        />
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 32 }}>
                    <Text style={{ fontSize: 12, color: this.props.textColor ? this.props.textColor : variables.gray2 }}>{this.state.playTime}</Text>
                    <Text style={{ fontSize: 12, color: this.props.textColor ? this.props.textColor : variables.gray2 }}>{this.state.duration}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}
