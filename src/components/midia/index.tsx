import React, { useState, useEffect, useRef } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    TextInput,
    Image,
    Modal,
    Dimensions,
    FlatList,
    ImageResizeMode
} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
const RNFS = require('react-native-fs');
import { Button, Icon, CheckBox } from 'react-native-elements';
import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import {
    SimpleBottomSheet,
    SimpleModal,
    BottomSheetConfirm,
    BottomSheetSuccess,
} from '~/components/bottomSheets';
import Record from '~/components/midia/record';
import Player from '~/components/midia/player';
import Preview from '~/components/midia/preview';

import { theme, variables, hexToRGB } from '~/theme';

import IconCheckboxActive from '~/assets/images/icon-checkbox-active.svg';
import IconCheckboxInactive from '~/assets/images/icon-checkbox-inactive.svg';

import IconUpload from '~/assets/images/icon-upload.svg';
import IconClock from '~/assets/images/icon-clock.svg';
import IconMenu from '~/assets/images/icon-ellipsis-v.svg';
import IconMenuWhite from '~/assets/images/icon-ellipsis-v-white.svg';
import IconGrid from '~/assets/images/icon-grid.svg';
import IconList from '~/assets/images/icon-list.svg';
import IconGalery from '~/assets/images/icon-galery.svg';
import IconMic from '~/assets/images/icon-mic.svg';
import IconFile from '~/assets/images/icon-file.svg';
import IconListCheck from '~/assets/images/icon-list-check.svg';
import IconTrash from '~/assets/images/trash.svg';
import IconBack from '~/assets/images/arrow-back.svg';
import IconPathSuccess from '~/assets/images/icon-path-success2.svg';
import ServiceApi from '~/api/api-caller';
import ManagerAuth from '~/manger-auth/manager';

const index = ({
    items = [],
    grid = false,
    onSelect,
    onUnselect = null,
    onChangeView = null,
    onAddAsset = null,
    headerStyles = {},
    showHeader = true,
    showUploadBox = true,
    showCategories = false,
    onClickCategory = null,
    onChangeModalCase = null,
    onChangeModalDeleteFile = null,
    reloadFiles = null,
    wrap = false,
    addCase = false,
    navigation = null,
    tokenStr = null,
    modalCase = false,
    modalDeleteFile = false,
    filepath = {
        data: '',
        uri: ''
    },
    fileData = '',
    fileUri = '',
}) => {
    const [progress, setProgress] = useState(65);
    const [order, setOrder] = useState(0);
    const [bsOrder, setBsOrder] = useState(false);

    const [dontShow, setDontShow] = useState(false);
    const [modalVisible2, setModalVisible2] = useState(false);
    const [modalGalery, setModalGalery] = useState(false);
    const [modalAudio, setModalAudio] = useState(false);
    const [modalFile, setModalFile] = useState(false);
    // let itemSelected: any;
    // let setItemSelected: any;
    const [itemSelected, setItemSelected] = useState({
        url: '',
        size: 0,
        type: '',
        name: '',
        key: '',
    });
    const [indexSelected, setIndexSelected] = useState(null)
    const [modalImage, setModalImage] = useState(false);

    const [modalMidiaConfirm, setModalMidiaConfirm] = useState(false);
    const [modalMidiaSuccess, setModalMidiaSuccess] = useState(false);
    const [modalRecord, setModalRecord] = useState(false);

    const [_modalCase, setModalCase] = useState(false);
    const [modalCaseConfirm, setModalCaseConfirm] = useState(false);
    const [caseSelected, setCaseSelected] = useState({
        date: '',
        hospitalId: '',
        risk: '',
        description: '',
        name: '',
        type: '',
        title: '',
    });

    const [numOfColumns, setNumOfColumns] = useState(10);

    const [filterCaseList, setFilterCaseList] = useState([])
    const [onChangeSearchCase, setOnChangeSearchCase] = useState('')
    const [caseList, setCaseList] = useState([])
    const [resultCaseList, setResultCaseList] = useState([])

    const [token, setToken] = useState('');

    useEffect(() => {
        getToken();
    }, [])


    const getToken = async () => {
        try {
            setLoading(true)
            const idToken = await ManagerAuth.getToken();
            setToken(idToken.toString());
            tokenStr = idToken
        } catch (error) {
            console.log(error)
        } finally {
            setLoading(false)
        }
    }

    const [isLoading, setLoading] = useState(true);


    const orders = [
        'Data (mais antiga primeiro)',
        'Data (mais antiga primeiro)',
        'Tamanho (maior primeiro)',
        'Tamanho (menor primeiro)',
        'Nome do arquivo',
    ];

    useEffect(() => {
        if (modalCase)
            selectOneCase()
        else
            setModalCase(modalCase)
    }, [modalCase]);

    useEffect(() => {
        if (onChangeModalCase)
            onChangeModalCase(_modalCase);
    }, [_modalCase]);

    useEffect(() => {
        setModalMidiaConfirm(modalDeleteFile)
    }, [modalDeleteFile]);

    useEffect(() => {
        if (onChangeModalDeleteFile)
        onChangeModalDeleteFile(modalMidiaConfirm);
    }, [modalMidiaConfirm]);

    useEffect(() => {
        const windowWidth =
            Dimensions.get('window').width - variables.paddingHorizontal * 2;
        const x = windowWidth / 108;
        setNumOfColumns(x);
    }, []);

    Dimensions.addEventListener('change', () => {
        const windowWidth =
            Dimensions.get('window').width - variables.paddingHorizontal * 2;
        const x = windowWidth / 108;
        setNumOfColumns(x);
    });

    const openViewFile = (item, index) => {
        setItemSelected(item);
        setIndexSelected(index);
        setModalImage(true);
    }

    const closeItem = () => {
        setItemSelected({
            url: '',
            size: 0,
            type: '',
            name: '',
            key: '',
        });
        setIndexSelected(null);
        setModalImage(false);
    };

    const selectCase = async (e) => {
        setCaseSelected(e)
        if (itemSelected.key == '') {
            addFilesSelectedToCase(e, itemSelected)
        } else {
            setModalCase(false);
            await getToken();
            addFileToCase(e, itemSelected).then(() => {
                setModalCaseConfirm(true);
                setModalImage(false);
                closeItem();
            })
        }

    };

    const addFilesSelectedToCase = async (c, f) => {
        let promises = [];
        await getToken();
        items.filter(e => e.selected).forEach(e => {
            let promise = addFileToCase(c, f);
            promises.push(promise);
        })
        Promise.all(promises).then(() => {
            setModalCaseConfirm(true);
            onUnselect()
        });
        setModalCase(false);
    };

    const addFileToCase = (c, f) => {
        return new Promise((resolve, reject) => {
            console.log("TOKEN", token)
            ServiceApi.copyFileToCase(token, f.key, c.id, (data) => {
                console.log("Salvo", f.key);
                resolve(true);
            }, () => {
                console.log("Erro na cópia")
            })
        })
    }

    const goToCase = () => {
        setModalCaseConfirm(false);
        setModalImage(false);
        //navigation.navigate('CaseSave', { case: caseSelected })
        navigation.push('Case', {case: caseSelected});
    };

    const filterOnChangeSearchCase = (text) => {
        setOnChangeSearchCase(text)
        if (onChangeSearchCase.length == 0) {
            setResultCaseList(caseList)
        } else {
            setResultCaseList(caseList.filter(x => String(x.title).toLocaleLowerCase().includes(text.toLocaleLowerCase())));
        }
    }
    const selectOneCase = async () => {
        console.log("Lista casos")
        setOnChangeSearchCase('')
        setLoading(true)
        await getToken();
        ServiceApi.getCases(tokenStr, (response) => {
            setCaseList(response)
            setResultCaseList(response)
            setLoading(false)
            setModalCase(true)
        }, (error) => {
            setLoading(false)
        })
    };

    const handleFileChangeName = (text) => {
        setItemSelected(prevState => ({
            ...prevState,
            name: text
        }));
    }

    const saveFileSelected = async () => {
        items.splice(indexSelected, 1, itemSelected);
        if (!/^data:.*;/.test(itemSelected.url)) {
            await getToken();
            ServiceApi.renameFile(tokenStr, itemSelected.key, itemSelected.name, (data) => { console.log(data) }, () => { console.log("Erro ao renomear") })
        }
    }

    const deleteFile = (f, i) => {
        if (!/^data:.*;/.test(itemSelected.url)) {
            return new Promise((resolve, reject) => {
                ServiceApi.deleteFile(token, f.key, () => {                    
                    resolve(true);
                }, () => { console.log("Erro ao deletar arquivo") })
            })
        } else {
            return new Promise((resolve, reject) => {
                items.splice(i, 1);
                resolve(true);
            })
        }
    }

    const deleteFilesSelected = () => {
        let promises = [];
        let elementsToRemove = []
        items.filter(e => e.selected).forEach((e,i) => {
            let promise = deleteFile(e, i);
            promises.push(promise);
            elementsToRemove.push(e.url)
        })
        return Promise.all(promises).then(() => {
            reloadFiles()
        });
    }


    const deleteFileSelectedOrSelecteds = async () => {
        console.log("Delete files")
        setModalMidiaSuccess(true)
        await getToken();
        console.log("TOKEN", token)
        if(itemSelected.url == ''){
            deleteFilesSelected().then(() => {
                setModalMidiaSuccess(true);
                setModalMidiaConfirm(false)
                setModalImage(false);                
            })
        }else{
            deleteFile(itemSelected, indexSelected).then(() => {
                items.splice(indexSelected, 1);
                setModalMidiaSuccess(true);
                setModalMidiaConfirm(false)
                setModalImage(false);
            })
        }
    }

    const styles = StyleSheet.create({
        page: {
            padding: variables.paddingHorizontal,
        },
        box_upload: {
            padding: variables.paddingHorizontal,
            borderWidth: 3,
            borderColor: hexToRGB(variables.accent, 0.5),
            borderStyle: 'dashed',
            borderRadius: 3,
            marginTop: variables.paddingHorizontal,
            alignItems: 'center',
        },
        box_upload_txt: {
            marginTop: 6,
            color: variables.gray3,
        },
        midia_header: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: variables.paddingHorizontal,
            paddingHorizontal: variables.paddingHorizontal,
            position: 'relative',
            marginTop: 24,
            zIndex: 12,
        },
        midia_list: {
            marginBottom: items.length > 0 ? 30 : 0,
            flexDirection: 'row',
        },
        paragraph: {
            fontSize: variables.fontSize,
            color: variables.gray2,
            marginVertical: 8,
        },
        midia_opt: {
            alignItems: 'center',
            marginVertical: 32,
        },
        midia_opt_txt: {
            color: variables.accent,
            fontSize: variables.fontSize,
            marginTop: 5,
        },
        cat_box: {
            flexDirection: 'row',
            height: 56,
            alignItems: 'center',
            backgroundColor: '#F8F9FC',
            borderRadius: variables.borderRadius,
            paddingHorizontal: variables.paddingHorizontal,
            marginBottom: 14,
        },
        cat_text: {
            marginLeft: 18,
            color: variables.secondary_light,
        },
        textfield: {
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: variables.paddingHorizontal,
            borderRadius: variables.borderRadius,
            backgroundColor: variables.light,
            borderWidth: 1,
            borderColor: '#C0C4D0',
            marginBottom: variables.paddingHorizontal,
        },
        textfield_input: {
            height: 48,
            flexGrow: 1,
            color: variables.secondary,
            fontSize: variables.fontSize,
        },
        textfield_icon: {
            flexShrink: 0,
            width: 24,
            height: 20,
        },
        badge: {
            fontSize: 12,
            paddingVertical: 6,
            paddingHorizontal: 12,
            borderRadius: 4,
            backgroundColor: 'rgba(0, 108, 255, 0.1)',
            color: variables.primary,
        },
        badge_error: {
            color: variables.danger,
            backgroundColor: 'rgba(247, 28, 93, 0.1)',
        },
        button_continue: {
            marginTop: 40,
            flexDirection: 'row',
            backgroundColor: '#006CFF',
            height: 48,

            borderRadius: 6,
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 16,
            marginRight: 16,
        },
        text_style_bold: {
            color: '#ffffff',
            fontSize: 14,
            marginLeft: 24,
            fontFamily: 'Montserrat-SemiBold',
        },
    });

    const menu2 = {
        optionsContainer: {
            padding: 0,
        },
        optionWrapper: {
            margin: 0,
            padding: 0,
        },
    };

    function addRecord(asset) {
        items.push(asset);
        onAddAsset(asset, items.length - 1);
    }

    async function PickImage() {
        PickAsset([DocumentPicker.types.images])
    }

    async function PickFile() {
        PickAsset([
            DocumentPicker.types.doc,
            DocumentPicker.types.docx,
            DocumentPicker.types.csv,
            DocumentPicker.types.pdf,
            DocumentPicker.types.plainText,
            DocumentPicker.types.ppt,
            DocumentPicker.types.pptx,
            DocumentPicker.types.video,
            DocumentPicker.types.xls,
            DocumentPicker.types.xlsx,
            DocumentPicker.types.audio
        ])
    }

    async function PickAsset(types) {
        try {
            const res = await DocumentPicker.pick({
                type: types,
            });
            const base64 = await RNFS.readFile(res.uri, 'base64');
            const item = {
                key: null,
                url: `data:${res.type};base64,${base64}`,
                size: res.size,
                type: res.type,
                name: res.name.replace(/\..+$/, ''),
            }
            items.push(item);
            onAddAsset(item, items.length - 1);
            setIndexSelected(items.length - 1);
            setItemSelected(item);
            setModalImage(true);
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log("Cancelled img pick")
            } else {
                throw err;
            }
        }
    }

    return (
        <View>
            {items.length > 0 && (
                <>
                    {showHeader && (
                        <View style={[styles.midia_header, headerStyles]}>
                            <Text style={theme.title}>Mídia</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity
                                    style={{ width: 35, alignItems: 'center' }}
                                    onPress={() => onChangeView()}>
                                    {grid && <IconList />}
                                    {!grid && <IconGrid />}
                                </TouchableOpacity>
                                <Menu onSelect={(func) => func()}>
                                    <MenuTrigger>
                                        <View style={{ width: 35, alignItems: 'center' }}>
                                            <IconMenu />
                                        </View>
                                    </MenuTrigger>
                                    <MenuOptions
                                        optionsContainerStyle={{ marginTop: 30 }}
                                        customStyles={menu2}>
                                        <MenuOption value={() => setBsOrder(true)}>
                                            <View style={theme.dropdown_item}>
                                                <Text style={theme.dropdown_item_txt}>
                                                    Classificar por
                                                </Text>
                                            </View>
                                        </MenuOption>
                                        {/* <MenuOption value={() => onSelect()}>
                                            <View style={theme.dropdown_item}>
                                                <Text style={theme.dropdown_item_txt}>
                                                    Selecionar tudo
                                                </Text>
                                            </View>
                                        </MenuOption> */}
                                        <MenuOption value={() => onSelect()}>
                                            <View style={theme.dropdown_item}>
                                                <Text style={theme.dropdown_item_txt}>Selecionar</Text>
                                            </View>
                                        </MenuOption>
                                    </MenuOptions>
                                </Menu>
                            </View>
                        </View>
                    )}
                    <View
                        style={
                            grid
                                ? {
                                    paddingVertical: variables.paddingVertical,
                                    paddingHorizontal: variables.paddingHorizontal - 8,
                                }
                                : {}
                        }>
                        <FlatList
                            horizontal={grid && !wrap}
                            pagingEnabled={true}
                            decelerationRate={0}
                            scrollEventThrottle={8}
                            snapToAlignment="start"
                            showsHorizontalScrollIndicator={false}
                            snapToInterval={108 + 8 * 0.375}
                            data={items} // your array should go here
                            numColumns={wrap ? Math.round(numOfColumns) : 0}
                            key={numOfColumns}
                            keyExtractor={(item, index) => 'key' + index}
                            renderItem={({ item, index }) => (
                                <Preview
                                    item={item}
                                    index={index}
                                    onOpen={(e) => openViewFile(e, index)}
                                    onSelect={(e) => onSelect(e)}
                                    onUnselect={(e, index) => onUnselect(e, index)}
                                    grid={grid}
                                />
                            )}
                        />
                    </View>
                </>
            )}
            {showCategories && (
                <View style={styles.page}>
                    <Text
                        style={[theme.title, { marginBottom: variables.paddingHorizontal }]}>
                        Categorias
                    </Text>
                    <TouchableOpacity
                        style={styles.cat_box}
                        onPress={() => onClickCategory('image')}>
                        <IconGalery width="23" height="23" />
                        <Text style={styles.cat_text}>Galeria ({items && items.filter(a => a.type.includes("image")) !== null ? items.filter(a => a.type.includes("image")).length : 0})</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.cat_box}
                        onPress={() => onClickCategory('mp4')}>
                        <IconMic width="17" height="23" />
                        <Text style={styles.cat_text}>Áudios ({items && items.filter(a => a.type.includes("mp4")) !== null ? items.filter(a => a.type.includes("mp4")).length : 0})</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.cat_box, { marginBottom: 0 }]}
                        onPress={() => onClickCategory('application')}>
                        <IconFile width="19" height="23" />
                        <Text style={styles.cat_text}>Arquivos ({items && items.filter(a => a.type.includes("application")) !== null ? items.filter(a => a.type.includes("application")).length : 0})</Text>
                    </TouchableOpacity>
                </View>
            )}
            {showUploadBox && (
                <View style={styles.page}>
                    <Text style={theme.title}>
                        Você quer adicionar agora alguma mídia?
                    </Text>
                    <TouchableOpacity
                        style={styles.box_upload}
                        onPress={() => setModalVisible2(true)}>
                        <IconUpload />
                        <Text style={styles.box_upload_txt}>
                            Mídia: Galeria, Áudio e Arquivo
                        </Text>
                    </TouchableOpacity>
                </View>
            )}

            <SimpleModal visible={modalGalery} onClose={() => setModalGalery(false)}>
                <Text style={theme.title}>Galeria de Fotos</Text>
                <Text style={styles.paragraph}>
                    Aqui você poderá adicionar Fotos de prontuários, exames, Arquivos e
                    Áudio referentes ao Caso e que poderão servir de auxílio ou prova,
                    processo ético (CRM), publicação de artigos ou como parâmetro em
                    outros casos, entre outros.
                </Text>
                <CheckBox
                    title="Não mostrar novamente"
                    containerStyle={{
                        backgroundColor: 'transparent',
                        borderWidth: 0,
                        paddingHorizontal: 0,
                        marginLeft: 0,
                        paddingVertical: variables.paddingHorizontal,
                    }}
                    textStyle={{
                        color: variables.gray2,
                        fontWeight: dontShow ? 'bold' : 'normal',
                    }}
                    checked={dontShow}
                    onPress={() => setDontShow(!dontShow)}
                    checkedIcon={<IconCheckboxActive />}
                    uncheckedIcon={<IconCheckboxInactive />}
                />
                <Button
                    title="Continuar"
                    disabled={false}
                    buttonStyle={{
                        backgroundColor: variables.primary,
                        borderColor: variables.primary,
                        borderRadius: variables.borderRadius,
                        height: variables.inputHeight,
                    }}
                    titleStyle={{
                        color: variables.light,
                        fontFamily: variables.fontBold,
                        fontSize: variables.fontSize,
                    }}
                    disabledStyle={{
                        backgroundColor: hexToRGB(variables.primary, 0.3),
                        borderColor: variables.primary,
                    }}
                    disabledTitleStyle={{ color: variables.light }}
                    onPress={() => {
                        setModalGalery(false);
                        setModalVisible2(false);
                        PickImage();
                    }}
                />
            </SimpleModal>

            <SimpleModal visible={modalAudio} onClose={() => setModalAudio(false)}>
                <Text style={theme.title}>Gravar Áudio</Text>
                <Text style={styles.paragraph}>
                    Grave aqui tudo de relevante que ocorreu no Caso, principalmente
                    aquilo que não está escrito em prontuário ou outros documentos, mas
                    que ocorreu de fato, tais como: condutas solicitadas, mas não
                    realizadas por membro da equipe, erros de conduta, erro de
                    diagnóstico, exames ou procedimentos negados sem justificativa
                    técnica, ausência de medicamentos, equipe especializada ou aparelhos
                    para seguir com determinada conduta etc.
                </Text>
                <Text style={styles.paragraph}>
                    Não deixe de mencionar qual foi a sua participação, orientações e os
                    nomes dos membros da equipe médica envolvidos e quais foram as suas
                    participações, orientações e ordens referentes ao Caso.
                </Text>
                <CheckBox
                    title="Não mostrar novamente"
                    containerStyle={{
                        backgroundColor: 'transparent',
                        borderWidth: 0,
                        paddingHorizontal: 0,
                        marginLeft: 0,
                        paddingVertical: variables.paddingHorizontal,
                    }}
                    textStyle={{
                        color: variables.gray2,
                        fontWeight: dontShow ? 'bold' : 'normal',
                    }}
                    checked={dontShow}
                    onPress={() => setDontShow(!dontShow)}
                    checkedIcon={<IconCheckboxActive />}
                    uncheckedIcon={<IconCheckboxInactive />}
                />
                <Button
                    title="Continuar"
                    disabled={false}
                    buttonStyle={{
                        backgroundColor: variables.primary,
                        borderColor: variables.primary,
                        borderRadius: variables.borderRadius,
                        height: variables.inputHeight,
                    }}
                    titleStyle={{
                        color: variables.light,
                        fontFamily: variables.fontBold,
                        fontSize: variables.fontSize,
                    }}
                    disabledStyle={{
                        backgroundColor: hexToRGB(variables.primary, 0.3),
                        borderColor: variables.primary,
                    }}
                    disabledTitleStyle={{ color: variables.light }}
                    onPress={() => {
                        setModalAudio(false);
                        setModalVisible2(false);
                        setModalRecord(true);
                    }}
                />
            </SimpleModal>

            <SimpleModal visible={modalFile} onClose={() => setModalFile(false)}>
                <Text style={theme.title}>Arquivos</Text>
                <Text style={styles.paragraph}>
                    Aqui você poderá adicionar Fotos de prontuários, exames, Arquivos e
                    Áudio referentes ao Caso e que poderão servir de auxílio ou prova,
                    processo ético (CRM), publicação de artigos ou como parâmetro em
                    outros casos, entre outros.
                </Text>
                <CheckBox
                    title="Não mostrar novamente"
                    containerStyle={{
                        backgroundColor: 'transparent',
                        borderWidth: 0,
                        paddingHorizontal: 0,
                        marginLeft: 0,
                        paddingVertical: variables.paddingHorizontal,
                    }}
                    textStyle={{
                        color: variables.gray2,
                        fontWeight: dontShow ? 'bold' : 'normal',
                    }}
                    checked={dontShow}
                    onPress={() => setDontShow(!dontShow)}
                    checkedIcon={<IconCheckboxActive />}
                    uncheckedIcon={<IconCheckboxInactive />}
                />
                <Button
                    title="Continuar"
                    disabled={false}
                    buttonStyle={{
                        backgroundColor: variables.primary,
                        borderColor: variables.primary,
                        borderRadius: variables.borderRadius,
                        height: variables.inputHeight,
                    }}
                    titleStyle={{
                        color: variables.light,
                        fontFamily: variables.fontBold,
                        fontSize: variables.fontSize,
                    }}
                    disabledStyle={{
                        backgroundColor: hexToRGB(variables.primary, 0.3),
                        borderColor: variables.primary,
                    }}
                    disabledTitleStyle={{ color: variables.light }}
                    onPress={() => {
                        setModalFile(false);
                        setModalVisible2(false);
                        PickFile();
                    }}
                />
            </SimpleModal>

            <SimpleBottomSheet
                visible={modalVisible2}
                onCloseOutside={() => setModalVisible2(false)}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={theme.title}>Selecione o tipo de mídia</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <TouchableOpacity
                        style={styles.midia_opt}
                        onPress={() => setModalGalery(true)}>
                        <IconGalery />
                        <Text style={styles.midia_opt_txt}>Galeria</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.midia_opt}
                        onPress={() => setModalAudio(true)}>
                        <IconMic />
                        <Text style={styles.midia_opt_txt}>Áudio</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.midia_opt}
                        onPress={() => { setModalVisible2(false); setModalFile(true) }}>
                        <IconFile />
                        <Text style={styles.midia_opt_txt}>Arquivo</Text>
                    </TouchableOpacity>
                </View>
            </SimpleBottomSheet>

            <SimpleBottomSheet
                visible={bsOrder}
                onCloseOutside={() => setBsOrder(false)}>
                <View>
                    <Text style={[theme.title, { marginBottom: 16 }]}>Classificar por</Text>
                    {orders.map((item, index) => (
                        <TouchableOpacity
                            style={[theme.dropdown_item, { paddingHorizontal: 0 }]}
                            key={index}
                            onPress={() => {
                                setOrder(index);
                                setBsOrder(false);
                            }}>
                            <Text style={theme.dropdown_item_txt}>{item}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            </SimpleBottomSheet>

            <Modal visible={modalImage} animationType="slide">
                <MenuProvider>
                    <View style={{ flex: 1, backgroundColor: variables.secondary_dark }}>
                        <View style={theme.navigation_bar}>
                            <TouchableOpacity onPress={() => closeItem()}>
                                <IconBack width="32" height="32" />
                            </TouchableOpacity>
                            <View style={{ width: '80%' }}>
                                <TextInput
                                    style={theme.text_title}
                                    onChangeText={handleFileChangeName}
                                    onBlur={saveFileSelected}
                                    value={itemSelected.name} />
                            </View>

                            {!addCase && (
                                <IconTrash onPress={() => setModalMidiaConfirm(true)} />
                            )}
                            {addCase && (
                                <Menu onSelect={(func) => func()}>
                                    <MenuTrigger>
                                        <View
                                            style={{
                                                width: 28,
                                                height: 30,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}>
                                            <IconMenuWhite />
                                        </View>
                                    </MenuTrigger>
                                    <MenuOptions
                                        optionsContainerStyle={{ marginTop: 30 }}
                                        customStyles={menu2}>
                                        <MenuOption value={() => setModalMidiaConfirm(true)}>
                                            <View style={theme.dropdown_item}>
                                                <Text style={theme.dropdown_item_txt}>Excluir</Text>
                                            </View>
                                        </MenuOption>
                                        <MenuOption value={() => selectOneCase()}>
                                            <View style={theme.dropdown_item}>
                                                <Text style={theme.dropdown_item_txt}>
                                                    Adicionar a um caso
                                                </Text>
                                            </View>
                                        </MenuOption>
                                    </MenuOptions>
                                </Menu>
                            )}
                        </View>
                        {itemSelected.type && (
                            <>
                                {itemSelected.type!.includes('image') && (
                                    <Image source={{ uri: itemSelected.url }} style={{
                                        height: 200,
                                        flex: 1,
                                        width: null,
                                        resizeMode: 'cover',
                                    }} />


                                )}
                                {itemSelected.type!.includes('mp4') && (
                                    <View
                                        style={{
                                            flexGrow: 1,
                                            justifyContent: 'center',
                                            paddingHorizontal: variables.paddingHorizontal,
                                        }}>
                                        <View style={{ alignSelf: 'center', marginBottom: 25 }}>
                                            <IconMic width="35" height="55" />
                                        </View>
                                        <Player
                                            path={itemSelected.url}
                                            style={{ height: 72, justifyContent: 'center' }}
                                            textColor="#E0E1E8"
                                        />
                                    </View>
                                )}
                                {itemSelected.type!.includes('application') && (
                                    <View
                                        style={{
                                            flexGrow: 1,
                                            justifyContent: 'center',
                                            paddingHorizontal: variables.paddingHorizontal,
                                        }}>
                                        <View style={{ alignSelf: 'center', marginBottom: 25 }}>
                                            <IconFile width="35" height="55" />
                                        </View>
                                    </View>
                                )}
                            </>
                        )}
                        <View style={theme.navigation_bar}></View>

                    </View>
                </MenuProvider>
                {/* <View style={{ backgroundColor: variables.secondary_dark }}>
                    <TouchableOpacity
                        style={styles.button_continue}
                        onPress={() => {saveFileSelected}}>
                        <Text style={styles.text_style_bold}>
                            Salvar
                        </Text>
                    </TouchableOpacity>
                </View> */}
            </Modal>

            <SimpleBottomSheet
                visible={_modalCase}
                onCloseOutside={() => setModalCase(false)}>
                <View>
                    <View style={styles.textfield}>
                        <TextInput
                            onChangeText={(text) => filterOnChangeSearchCase(text)}
                            style={styles.textfield_input}
                            placeholder="Buscar caso"
                            placeholderTextColor={variables.gray3}
                        />
                        <Icon
                            name="search"
                            style={styles.textfield_icon}
                            color={variables.primary}
                        />
                    </View>

                    <Text style={[theme.title, { marginBottom: 16 }]}>
                        Selecione o caso para adicionar mídias
                    </Text>

                    <View>
                        {resultCaseList.map((item, index) => (
                            <TouchableOpacity
                                style={styles.cat_box}
                                onPress={() => selectCase(item)}>
                                <Text
                                    style={{
                                        marginLeft: 0,
                                        color: variables.secondary,
                                        fontFamily: variables.fontBold,
                                        flexGrow: 1,
                                    }}>
                                    {item.title}
                                </Text>
                                <Text style={item.type == 'Caso Modelo' ? styles.badge : [styles.badge, styles.badge_error]}>{item.type}</Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </View>
            </SimpleBottomSheet>

            <SimpleBottomSheet
                visible={modalCaseConfirm}
                onClose={() => setModalCaseConfirm(false)}
                onCloseOutside={() => setModalCaseConfirm(false)}>
                <View
                    style={{
                        alignItems: 'center',
                        marginHorizontal: variables.paddingHorizontal,
                    }}>
                    <IconPathSuccess />
                    <Text
                        style={{
                            fontSize: 16,
                            color: variables.gray2,
                            marginVertical: variables.paddingHorizontal,
                            textAlign: 'center',
                        }}>
                        {`Mídia adicionada com \nsucesso ao caso `}
                        <Text style={{ fontWeight: '600' }}>{caseSelected.name}</Text>
                    </Text>
                </View>
                <Button
                    title="Ver caso"
                    disabled={false}
                    buttonStyle={{
                        backgroundColor: variables.primary,
                        borderColor: variables.primary,
                        borderRadius: variables.borderRadius,
                        height: variables.inputHeight,
                    }}
                    titleStyle={{
                        color: variables.light,
                        fontFamily: variables.fontBold,
                        fontSize: variables.fontSize,
                    }}
                    disabledStyle={{
                        backgroundColor: hexToRGB(variables.primary, 0.3),
                        borderColor: variables.primary,
                    }}
                    disabledTitleStyle={{ color: variables.light }}
                    onPress={() => goToCase()}
                />
                <View style={{ height: variables.paddingHorizontal }} />
                <Button
                    title="Fechar"
                    disabled={false}
                    buttonStyle={{
                        backgroundColor: variables.light,
                        borderColor: variables.primary,
                        borderRadius: variables.borderRadius,
                        height: variables.inputHeight,
                        borderWidth: 1,
                    }}
                    titleStyle={{
                        color: variables.primary,
                        fontFamily: variables.fontBold,
                        fontSize: variables.fontSize,
                    }}
                    disabledStyle={{
                        backgroundColor: hexToRGB(variables.primary, 0.3),
                        borderColor: variables.primary,
                    }}
                    disabledTitleStyle={{ color: variables.light }}
                    onPress={() => setModalCaseConfirm(false)}
                />
            </SimpleBottomSheet>

            <BottomSheetConfirm
                visible={modalMidiaConfirm}
                text={`Você tem certeza que deseja \napagar ${itemSelected.type.includes("image")
                    ? 'esta foto?'
                    : itemSelected.type.includes('mp4')
                        ? 'este áudio?'
                        : 'este arquivo?'
                    }`}
                onClose={() => setModalMidiaConfirm(false)}
                onConfirm={() => deleteFileSelectedOrSelecteds()}
            />
            <BottomSheetSuccess
                visible={modalMidiaSuccess}
                text={`${itemSelected.type.includes('image')
                    ? 'Foto apagada'
                    : itemSelected.type.includes('mp4')
                        ? 'Áudio apagado'
                        : 'Arquivo apagado'
                    } com sucesso!`}
                onClose={() => {
                    setModalMidiaSuccess(false);
                    closeItem();
                }}
            />
            <Modal visible={modalRecord}>
                <View style={{ flex: 1 }}>
                    <View style={theme.navigation_bar}>
                        <TouchableOpacity onPress={() => setModalRecord(false)}>
                            <IconBack width="32" height="32" />
                        </TouchableOpacity>
                        <Text style={theme.text_title}>Gravar Áudio</Text>
                        <TouchableOpacity onPress={() => setModalAudio(true)}>
                            <Icon name="info" type="feather" color={variables.light} />
                        </TouchableOpacity>
                    </View>
                    <Record onClose={() => setModalRecord(false)} onAddFile={(asset) => addRecord(asset)} />
                </View>
            </Modal>
        </View>
    );
};

export default index;
