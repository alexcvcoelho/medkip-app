import React, { useState, useEffect, useRef } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Animated, Image, Modal, Dimensions, FlatList, TouchableHighlight } from 'react-native';

import { Card, ListItem, Button, Icon, Badge, Divider, CheckBox, Slider } from 'react-native-elements';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import { theme, variables, hexToRGB } from '~/theme';

import IconClock from "~/assets/images/icon-clock.svg";
import IconMenu from "~/assets/images/icon-ellipsis-v.svg";
import IconListCheck from "~/assets/images/icon-list-check.svg";
import IconMic from "~/assets/images/icon-mic.svg";
import IconPlay from "~/assets/images/icon-play.svg";
import IconFile from "~/assets/images/icon-file.svg";

const preview = ({ item, index, onOpen, onSelect, onUnselect, grid }) => {

    const styles = StyleSheet.create({
        midia_item_box: {
            paddingHorizontal: grid ? 0 : variables.paddingHorizontal,
        },
        midia_item: {
            paddingVertical: grid ? 0 : variables.paddingHorizontal,
            borderBottomColor: hexToRGB(variables.secondary_light, 0.1),
            borderBottomWidth: grid ? 0 : 1,
            flexDirection: grid ? 'column' : 'row',
            alignItems: grid ? 'flex-start' : 'center',
            justifyContent: 'space-between',
            width: grid ? 108 : null,
            padding: grid ? 8 : null,
            flexBasis: 0,
            position: 'relative',
            zIndex: 10,
        },
        midia_item_selected: {
            backgroundColor: grid ? null : hexToRGB(variables.secondary, 0.5),
        },
        midia_item_txt: {
            fontSize: variables.fontSize,
            color: variables.secondary,
            marginTop: grid ? 5 : 0,
        },
        midia_preview: {
            width: grid ? 100 : 32,
            height: grid ? 100 : 32,
            marginRight: 12,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: grid ? '#F4F5FB' : 'transparent',
            borderRadius: 4,
        },
        menu_v: {
            width: 35,
            height: 35,
            alignItems: 'center',
            justifyContent: 'center',
            position: grid ? 'absolute' : null,
            top: grid ? 0 : null,
            right: grid ? 0 : null,
            zIndex: 20,
        },
        menu_v_selected: {
            right: 0,
            bottom: 0,
            top: 0,
            padding: 8,
            backgroundColor: hexToRGB(variables.secondary, 0.6),
            position: 'absolute',
            height: 100,
            width: 100,
            borderRadius: 4,
        },
        midia_size: {
            fontSize: 12,
            color: variables.gray2,
            display: grid ? 'none' : 'flex',
        },
        midia_size_audio: {
            fontSize: 12,
            color: variables.gray2,
            display: grid ? 'flex' : 'none',
            marginTop: 6,
        },
        dropdown_item: {
            padding: variables.paddingHorizontal,
            backgroundColor: variables.light,
        },
        dropdown_item_txt: {
            fontSize: variables.fontSize,
            color: variables.secondary,
        },
    })

    const menu2 = {
        optionsContainer: {
            padding: 0,
        },
        optionWrapper: {
            margin: 0,
            padding: 0,
        },
    }

    const sizeToText = (bytes) => {
        bytes = parseInt(bytes);
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
        if (bytes === 0) return 'n/a'
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
        if (i === 0) return `${bytes} ${sizes[i]})`
        return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
    }

    const Audio = () => {
        return (
            <>
                <TouchableOpacity onPress={() => onOpen(item)}>
                    <View style={styles.midia_preview}>
                        {grid && item.type.includes('mp4') && <IconMic />}
                        {!grid && item.type.includes('mp4') && <IconPlay />}
                        <Text style={styles.midia_size_audio}>{sizeToText(item.size)}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ flexGrow: 1 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.midia_item_txt}>{item.name}</Text>
                    </View>
                    {!grid && <Slider
                        value={0}
                        maximumValue={120}
                        minimumValue={0}
                        step={1}
                        style={{ height: 10 }}
                        maximumTrackTintColor={hexToRGB(variables.secondary_light, 0.4)}
                        minimumTrackTintColor={variables.primary}
                        trackStyle={{ height: 4, backgroundColor: 'transparent' }}
                        thumbStyle={{ height: 10, width: 10, backgroundColor: variables.primary }}
                    />}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.midia_size}>00:00</Text>
                        <Text style={styles.midia_size}>{item.size}</Text>
                    </View>
                </View>
            </>
        )
    }

    const File = () => {
        const bytes = sizeToText(item.size)
        return (
            <>
                <TouchableOpacity onPress={() => onOpen(item)}>
                    {item.type.includes('image') && <Image source={{ uri: item.url }} style={styles.midia_preview} />}
                    {item.type.includes('application') && <View style={styles.midia_preview}><IconFile /></View>}
                </TouchableOpacity>
                <View style={{ flexGrow: 1 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.midia_item_txt} numberOfLines={1}>{item.name.substr(0, 40)}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.midia_size}>{bytes}</Text>
                    </View>
                </View>
            </>
        )
    }

    if (item.progress < 100) {
        return (
            <View style={styles.midia_item_box} key={'m_' + index}>
                <View style={styles.midia_item}>
                    {item.type.includes('image') && <View style={styles.midia_preview}><Image source={{ uri: item.url }} style={{ width: 32, height: 32 }} /></View>}
                    {item.type.includes('mp4') && <View style={styles.midia_preview}><Icon name="play" type="font-awesome" color={variables.primary} /></View>}
                    {item.type.includes('application') && <View style={styles.midia_preview}><IconFile /></View>}
                    <View style={{ flexGrow: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={[styles.midia_item_txt]} numberOfLines={1}>{item.name}</Text>
                            <Text style={{ fontSize: 10, color: hexToRGB(variables.secondary, 0.5), display: grid ? 'none' : 'flex' }}>{`${item.progress}%`}</Text>
                        </View>
                        {!grid && <Slider
                            value={item.progress}
                            maximumValue={100}
                            minimumValue={0}
                            step={1}
                            style={{ height: 10 }}
                            maximumTrackTintColor={'#E8F2FF'}
                            minimumTrackTintColor={variables.primary}
                            trackStyle={{ height: 4, backgroundColor: 'transparent' }}
                            thumbStyle={{ height: 0, width: 0 }}
                        />}
                    </View>
                    <View style={styles.menu_v}>
                        <IconClock />
                    </View>
                </View>
            </View>
        )
    } else {
        return (
            <TouchableHighlight onLongPress={() => {onSelect(index) }} style={[styles.midia_item_box, item.selected ? styles.midia_item_selected : {}]} key={'m_' + index}>
                <View style={[styles.midia_item]}>
                    {item.type?.includes('mp4') && <Audio />}
                    {!item.type?.includes('mp4') && <File />}

                    {item.selected && <View style={grid ? styles.menu_v_selected : styles.menu_v}>
                        <TouchableHighlight onPress={() => {onUnselect(item, index)}}>
                            <IconListCheck />
                        </TouchableHighlight>
                    </View>}
                    {!item.selected && <Menu onSelect={func => func()} style={styles.menu_v}>
                        <MenuTrigger>
                            <View style={{ width: 28, height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                <IconMenu />
                            </View>
                        </MenuTrigger>
                        <MenuOptions optionsContainerStyle={{ marginTop: 30 }} customStyles={menu2}>
                            <MenuOption value={() => onOpen(item)}>
                                <View style={styles.dropdown_item}>
                                    <Text style={styles.dropdown_item_txt}>Excluir</Text>
                                </View>
                            </MenuOption>
                            <MenuOption value={() => onOpen(item)}>
                                <View style={styles.dropdown_item}>
                                    <Text style={styles.dropdown_item_txt}>Renomear</Text>
                                </View>
                            </MenuOption>
                            <MenuOption value={() => onSelect(index)}>
                                <View style={styles.dropdown_item}>
                                    <Text style={styles.dropdown_item_txt}>Selecionar</Text>
                                </View>
                            </MenuOption>
                        </MenuOptions>
                    </Menu>}
                </View>
            </TouchableHighlight>
        )
    }
}

export default preview
