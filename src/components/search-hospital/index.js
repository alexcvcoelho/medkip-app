import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Modal, ActivityIndicator } from 'react-native';

import { Card, Button, Icon, Divider, CheckBox } from 'react-native-elements';
import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import { theme, variables, hexToRGB } from '~/theme';

import { BottomSheetFailure, SimpleBottomSheet, SimpleModal } from '~/components/bottomSheets';

import FloatingInput from '~/components/floatingInput';

import IconBack from "~/assets/images/arrow-back.svg";
import IconMenu from "~/assets/images/icon-ellipsis-v.svg";
import IconTrash from "~/assets/images/icon-trash.svg";
import IconCheckSuccess from "~/assets/images/icon-check-success.svg";

import IconRadioActive from "~/assets/images/icon-radio-active.svg";
import IconRadioInactive from "~/assets/images/icon-radio-inactive.svg";

import IconExternalLink from "~/assets/images/icon-external-link.svg";
import IconMapMaker from "~/assets/images/icon-mapmaker.svg";
import IconHospital from "~/assets/images/icon-hospital-2.svg";
import IconHospitalSuccess from "~/assets/images/icon-hospital-success.svg";
import IconHospitalNotFound from "~/assets/images/icon-hospital-notfound.svg";

import ManagerAuth from '~/manger-auth/manager';
import ServiceApi from '~/api/api-caller';
export default class SearchHospital extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            token: '',

            modalError: false,
            messageInfo: '',

            modalSearch: false,
            check: false,
            selected: null,
            modalSuccess: false,
            modalForm: false,
            modalInfo: false,
            modalConfirm: false,
            modalDelete: false,
            isFocused: false,
            inputH: null,
            query: '',
            hospitals: [],
            filterHospitals: [],
            id: null,
            hospital: '',
            cidade: '',
            aliquota: '',
        };
    }

    onClose = () => {
        this.props.onClose()
    }

    onSelect = (e) => {
        console.log(e)
        this.props.onSelect(e)
    }

    setNavigation = (props) => {
        this.props = props
    }

    isSelected = (item) => {
        this.props.isSelected(item)
    }

    edit = (item) => {
        this.setState({ modalForm: true, hospital: item.name, cidade: item.city, aliquota: item.iss.toString(), id: item.id })
    }

    add = () => {
        this.setState({ modalForm: true, hospital: '', cidade: '', aliquota: '' })
    }

    delete = (item) => {
        this.setState({ modalConfirm: true, id: item.id })
    }

    removeItem = () => {
        this.setState({ isLoading: true })

        ServiceApi.deleteHospital(this.state.token, this.state.id, () => {
            this.setState({ modalConfirm: false, modalDelete: true, id: null, isLoading: false })
            this.getHospitals()
        }, (error) => {
            this.setState({
                isLoading: false, modalError: true, messageInfo: error, modalConfirm: false
            })
        }, null)
    }

    validateFields = () => {

        if (this.state.hospital.length == 0) {
            this.setState({ messageInfo: 'Campo nome do hospital é obrigatório!', modalError: true })
            return false
        } else if (this.state.cidade.length == 0) {
            this.setState({ messageInfo: 'Campo cidade é obrigatório!', modalError: true })
            return false
        } else if (this.state.aliquota.length == 0) {
            this.setState({ messageInfo: 'Campo iss é obrigatório!', modalError: true })
            return false
        }
        return true
    }

    handleISSNumber = (text) => {
        if (/^\d+$/.test(text) || text === '') {
            this.setState({ aliquota: text })
        }
    }

    onPressSaveHospital = () => {

        const result = this.validateFields()

        if (!result) {
            return
        }

        this.setState({ modalForm: false, isLoading: true })
        ServiceApi.saveHospital(this.state.token,
            this.state.id,
            this.state.hospital,
            this.state.cidade,
            parseInt(this.state.aliquota.toString()), () => {
                this.setState({ isLoading: false, modalSuccess: true })
                this.getHospitals()
            }, (error) => {
                this.setState({
                    isLoading: false, modalError: true, messageInfo: error
                })
            }, null)
    }

    onChangeQuery = (text) => {
        var data = []

        if (this.state.hospitals != null) {
            data = this.state.hospitals.filter(x => String(x.name).toLowerCase().includes(text.toLowerCase()));
        }
        this.setState({ query: text, filterHospitals: data })
    }

    getHospitals = () => {
        ServiceApi.getHospitals(this.state.token, (response) => {
            console.log(response)
            this.setState({ hospitals: response, isLoading: false })
        }, (error) => {
            this.setState({
                isLoading: false, modalError: true, messageInfo: error
            })
        }, null);
    }

    getToken = async () => {
        this.setState({ isLoading: true })

        const idToken = await ManagerAuth.getToken();

        this.setState({ token: idToken.toString() })

        this.getHospitals();
    }

    componentDidMount() {
        this.getToken();
    }

    render() {
        return (
            <MenuProvider>
                <View style={styles.container}>
                    <View style={styles.navigation_bar}>
                        <IconBack width="32" height="32" onPress={() => this.onClose()}></IconBack>
                        <Text style={[styles.text_title]}>Hospitais</Text>
                        <View style={{ width: 30, height: 30 }}></View>
                    </View>

                    {this.state.isLoading ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size="large" color="#4493FF" />
                        </View>
                        :
                        <View style={{ flex: 1 }}>
                            <View style={styles.page}>
                                <ScrollView>
                                    <TouchableOpacity style={styles.search_bar} onPress={() => this.setState({ modalSearch: true })}>
                                        <TextInput
                                            style={styles.search_bar_input}
                                            placeholder="Buscar Hospital Cadastrado"
                                            placeholderTextColor="#8189A1"
                                            editable={false}
                                        />
                                        <Icon name="search" color={variables.primary} style={styles.search_bar_icon} />
                                    </TouchableOpacity>
                                    {this.state.hospitals.length > 0 && <>
                                        <Text style={[styles.title, { marginTop: variables.paddingHorizontal }]}>Hospitais Adicionados</Text>
                                        {this.state.hospitals.map((item, index) => (
                                            <Card key={index} containerStyle={{ borderWidth: 0, borderRadius: 6, elevation: 0, marginLeft: 0, marginRight: 0, paddingVertical: 0 }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <CheckBox
                                                        containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, paddingHorizontal: 0, marginLeft: 0, flexGrow: 1 }}
                                                        textStyle={{ color: variables.gray2, fontWeight: this.state.check == 1 ? 'bold' : 'normal' }}
                                                        checkedIcon={<IconRadioActive />}
                                                        uncheckedIcon={<IconRadioInactive />}
                                                        checked={this.state.selected ? this.state.selected.name == item.name : false}
                                                        onPress={() => { this.setState({ check: index + 1, selected: item }) }}
                                                        title={
                                                            <View style={{ paddingLeft: variables.paddingHorizontal }}>
                                                                <Text style={styles.title}>{item.name}</Text>
                                                                <Text style={{ color: variables.gray2, fontSize: 13 }}>{item.local}</Text>
                                                            </View>
                                                        }
                                                    />
                                                    <Menu onSelect={(func) => func()}>
                                                        <MenuTrigger>
                                                            <View style={{ width: 35, alignItems: 'center' }}>
                                                                <IconMenu />
                                                            </View>
                                                        </MenuTrigger>
                                                        <MenuOptions optionsContainerStyle={{ marginTop: 30 }} customStyles={menu2}>
                                                            <MenuOption value={() => this.edit(item)}>
                                                                <View style={styles.dropdown_item}>
                                                                    <Text style={styles.dropdown_item_txt}>Editar</Text>
                                                                </View>
                                                            </MenuOption>
                                                            <MenuOption value={() => this.delete(item)}>
                                                                <View style={styles.dropdown_item}>
                                                                    <Text style={styles.dropdown_item_txt}>Excluir</Text>
                                                                </View>
                                                            </MenuOption>
                                                        </MenuOptions>
                                                    </Menu>
                                                </View>
                                            </Card>
                                        ))}
                                        <Button
                                            title="Selecionar"
                                            disabled={!this.state.selected}
                                            buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight, marginTop: variables.paddingHorizontal }}
                                            titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                            disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                            disabledTitleStyle={{ color: variables.light }}
                                            onPress={() => this.onSelect(this.state.selected)}
                                        />
                                    </>}
                                </ScrollView>
                                {this.state.hospitals.length == 0 && <>
                                    <View style={{ alignItems: 'center', flex: 1 }}>
                                        <IconHospital width="50" height="44" style={{ marginBottom: 19 }} />
                                        <Text style={{ color: variables.gray3, fontSize: 14, textAlign: 'center' }}>{'Você não tem nenhum \nhospital salvo'}</Text>
                                    </View>
                                    <View style={[theme.fab_button_container, { backgroundColor: 'transparent' }]}>
                                        <TouchableOpacity style={theme.fab_button} onPress={() => this.add()}>
                                            <Icon name="plus" type="feather" color="#ffffff" />
                                        </TouchableOpacity>
                                    </View>
                                </>}
                            </View>
                            <Modal visible={this.state.modalForm} animationType="slide">
                                <View style={{ flex: 1 }}>
                                    <View style={styles.navigation_bar}>
                                        <TouchableOpacity onPress={() => this.setState({ modalForm: false })}>
                                            <IconBack width="32" height="32" />
                                        </TouchableOpacity>
                                        <Text style={styles.text_title}>{this.state.hospital || this.state.cidade ? 'Editar Hospital' : 'Adicionar Hospital'}</Text>
                                        <View style={{ width: 30, height: 30 }}></View>
                                    </View>
                                    <View style={{ paddingHorizontal: variables.paddingHorizontal, paddingVertical: 32 }}>
                                        <View style={{ position: 'relative' }}>
                                            <FloatingInput
                                                label="Hospital"
                                                isMask={false}
                                                value={this.state.hospital}
                                                onChangeText={(text) => this.setState({ hospital: text })}
                                                onFocus={() => this.setState({ isFocused: true })}
                                                onBlur={() => this.setState({ isFocused: false })}
                                            />
                                            {this.state.isFocused && <View style={styles.dropdown}>
                                                <Text style={[styles.item]}>São Paulo - SP</Text>
                                                <Text style={[styles.item]}>São João da Boa vista - SP</Text>
                                                <Text style={[styles.item]}>São Carlos - SP</Text>
                                            </View>}
                                        </View>
                                        <FloatingInput
                                            label="Cidade"
                                            isMask={false}
                                            value={this.state.cidade}
                                            onChangeText={(text) => this.setState({ cidade: text })}
                                        />
                                        <FloatingInput
                                            label="Alíquota de ISS"
                                            isMask={false}
                                            keyboardType="numeric"
                                            value={this.state.aliquota}
                                            onChangeText={(text) => this.handleISSNumber(text)}
                                            icon={<Icon name="info" type="feather" color={variables.gray3} onPress={() => this.setState({ modalInfo: true })} />}
                                        />
                                        <Button
                                            title="Salvar"
                                            disabled={false}
                                            buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                                            titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                            disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                            disabledTitleStyle={{ color: variables.light }}
                                            onPress={() => this.onPressSaveHospital()}
                                        />
                                    </View>
                                </View>
                            </Modal>
                            <Modal
                                visible={this.state.modalSearch}
                                onShow={() => {
                                    //this.state.inputH.current.blur()
                                    //this.state.inputH.current.focus()
                                }}
                            >
                                <View style={{ flex: 1 }}>
                                    <View style={styles.navigation_bar}>
                                        <TouchableOpacity onPress={() => this.setState({ modalSearch: false })}>
                                            <IconBack width="32" height="32" />
                                        </TouchableOpacity>
                                        <TextInput
                                            style={{ flexGrow: 1, height: 40, top: 3, position: 'relative', color: variables.light }}
                                            placeholder="Pesquisar Hospital..."
                                            placeholderTextColor="#E0E1E8"
                                            onChangeText={(text) => this.onChangeQuery(text)}
                                            value={this.state.query}
                                            autoFocus={true}

                                        />
                                        {this.state.query.length > 0 && <Icon name="close" color={variables.light} onPress={() => this.setState({ query: '', filterHospitals: [] })} />}
                                    </View>
                                    {this.state.filterHospitals.length > 0 && this.state.filterHospitals.map((item, index) => {
                                        return (
                                            <View key={index + '_p2'} style={{ paddingHorizontal: variables.paddingHorizontal }}>
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: variables.paddingHorizontal }}
                                                 onPress={() => this.onSelect(item)}>
                                                    <IconMapMaker />
                                                    <View style={{ flexGrow: 1, marginHorizontal: variables.paddingVertical }}>
                                                        <Text style={styles.title}>{item.name}</Text>
                                                        <Text style={{ color: variables.gray2, fontSize: 13 }}>{item.local}</Text>
                                                    </View>
                                                    <IconExternalLink />
                                                </TouchableOpacity>
                                                <Divider />
                                            </View>
                                        )
                                    })}
                                </View>
                                {this.state.filterHospitals.length == 0 && <>
                                    <View style={{ alignItems: 'center', flexGrow: 1 }}>
                                        <IconHospitalNotFound width="50" height="44" style={{ marginBottom: 19 }} />
                                        <Text style={styles.title}>Ops, desculpe</Text>
                                        <Text style={{ color: variables.gray3, fontSize: 14, textAlign: 'center', marginTop: 8 }}>{'Não encontramos nenhum hospital \ncadastrado em nosso banco de dados.'}</Text>
                                    </View>
                                    <View style={[theme.fab_button_container]}>
                                        <Text style={theme.fab_button_hint}>{'Você pode adicionar um hospital \nmanualmente clicando aqui'}</Text>
                                        <TouchableOpacity style={theme.fab_button} onPress={() => this.add()}>
                                            <Icon name="plus" type="feather" color="#ffffff" />
                                        </TouchableOpacity>
                                    </View>
                                </>}
                            </Modal>
                            <SimpleBottomSheet
                                visible={this.state.modalSuccess}
                                onClose={() => this.setState({ modalSuccess: false })}
                            >
                                <View style={{ alignItems: 'center' }}>
                                    <IconHospitalSuccess />
                                    <Text style={{ marginVertical: variables.paddingHorizontal, color: variables.gray2, fontSize: 16, textAlign: 'center' }}>{'Hospistal salvo à sua lista \ncom sucesso!'}</Text>
                                </View>
                                <Button
                                    title="OK"
                                    disabled={false}
                                    buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                                    titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                    disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                    disabledTitleStyle={{ color: variables.light }}
                                    onPress={() => this.setState({ modalSuccess: false })}
                                />
                            </SimpleBottomSheet>
                            <SimpleBottomSheet visible={this.state.modalConfirm} onClose={() => this.setState({ modalConfirm: false })}>
                                <View style={{ alignItems: 'center', marginHorizontal: variables.paddingHorizontal }}>
                                    <IconTrash />
                                    <Text style={{ fontSize: 16, color: variables.gray2, marginVertical: variables.paddingHorizontal, textAlign: 'center' }}>Você tem certeza que desenha exluir este local da sua lista?</Text>
                                </View>
                                <Button
                                    title="Sim, excluir"
                                    disabled={false}
                                    buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                                    titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                    disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                    disabledTitleStyle={{ color: variables.light }}
                                    onPress={() => this.removeItem()}
                                />
                                <View style={{ height: variables.paddingHorizontal }} />
                                <Button
                                    title="Não, voltar"
                                    disabled={false}
                                    buttonStyle={{ backgroundColor: variables.light, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight, borderWidth: 1 }}
                                    titleStyle={{ color: variables.primary, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                    disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                    disabledTitleStyle={{ color: variables.light }}
                                    onPress={() => this.setState({ modalConfirm: false })}
                                />
                            </SimpleBottomSheet>
                            <SimpleBottomSheet visible={this.state.modalDelete} onClose={() => this.setState({ modalDelete: false })}>
                                <View style={{ alignItems: 'center', marginHorizontal: variables.paddingHorizontal }}>
                                    <IconCheckSuccess />
                                    <Text style={{ fontSize: 16, color: variables.gray2, marginVertical: variables.paddingHorizontal, textAlign: 'center' }}>Local excluído com sucesso!</Text>
                                </View>
                                <Button
                                    title="Ok"
                                    disabled={false}
                                    buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                                    titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                    disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                    disabledTitleStyle={{ color: variables.light }}
                                    onPress={() => this.setState({ modalDelete: false })}
                                />
                            </SimpleBottomSheet>
                            <SimpleModal visible={this.state.modalInfo} onClose={() => this.setState({ modalInfo: false })}>
                                <Text style={styles.paragraph}>Favor confirmar com a sua contabilidade o ISS da cidade que você presta o plantão.</Text>
                            </SimpleModal>

                            <BottomSheetFailure
                                visible={this.state.modalError}
                                text={this.state.messageInfo}
                                onClose={() => { this.setState({ modalError: false }); }}
                            />
                        </View>
                    }
                </View>
            </MenuProvider >

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    navigation_bar: {
        height: 96,
        backgroundColor: '#202945',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    text_title: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center'
    },
    //meus estilos
    page: {
        flex: 1,
        backgroundColor: '#F5F6FA',
        padding: 16,
        paddingBottom: 76,
        justifyContent: 'center'
    },
    title: {
        color: variables.secondary,
        fontFamily: variables.fontBold,
        fontSize: variables.fontSize,
        margin: 0,
    },
    text: {
        color: variables.secondary,
        fontSize: 14,
    },
    search_bar: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 12,
        borderRadius: 6,
        backgroundColor: '#ffffff',
    },
    search_bar_input: {
        height: 48,
        flexGrow: 1,
    },
    search_bar_icon: {
        flexShrink: 0,
    },
    paragraph: {
        fontSize: variables.fontSize,
        color: variables.gray2,
        marginVertical: 8,
    },
    dropdown: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 48,
        backgroundColor: variables.light,
        shadowColor: variables.secondary_dark,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.19,
        shadowRadius: 0,

        elevation: 3,
        zIndex: 20,
    },
    dropdown_item: {
        padding: variables.paddingHorizontal,
        backgroundColor: variables.light,
    },
    dropdown_item_txt: {
        fontSize: variables.fontSize,
        color: variables.secondary,
    },
    item: {
        paddingHorizontal: 24,
        paddingVertical: variables.paddingVertical,
        color: variables.gray2,
    },
});

const menu2 = {
    optionsContainer: {
        padding: 0,
    },
    optionWrapper: {
        margin: 0,
        padding: 0,
    },
}