import React, { createContext, useContext, useState } from 'react';

export const AuthContext = createContext({});


//Player
export const PlayerContext = createContext({
    fileDescription: {},
    updateFile: () => { },
    clearFile: () => { }
});
export function PlayerContextProvider({
    children,
}) {
    const [fileDescription, setFile] = useState({
        path: '',
        name: '',
        size: '',
        type: ''
    })

    function updateFile(file) {
        setFile(file)
    }

    function clearFile() {
        setFile({
            path: '',
            name: '',
            size: '',
            type: ''
        })
    }

    return (
        <PlayerContext.Provider
            value={{
                fileDescription,
                updateFile,
                clearFile
            }}
        >
            {children}
        </PlayerContext.Provider>
    );
}

export const usePlayer = () => {
    return useContext(PlayerContext);
};