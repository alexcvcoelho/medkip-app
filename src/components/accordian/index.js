import React, {Component} from 'react';
import { View, TouchableOpacity, Text, StyleSheet, LayoutAnimation, Platform, UIManager} from "react-native";
import { Icon, Divider } from 'react-native-elements';

import { theme, variables } from '~/theme';

export default class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = { 
          data: props.data,
          expanded : false,
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
  
  render() {

    return (
       <View>
            <TouchableOpacity style={[styles.row, this.props.headerStyles]} onPress={()=>this.toggleExpand()}>
                <Text style={[theme.title]}>{this.props.title}</Text>
                <Icon name={this.state.expanded ? 'chevron-up' : 'chevron-down'} type="feather" style={{ width: 20}} color="#364574" />
            </TouchableOpacity>
            {
                this.state.expanded &&
                <View>
                    {/* <Divider /> */}
                    {/* <View style={{ height: variables.paddingHorizontal/ 2 }}></View> */}
                    {this.props.children}
                </View>
            }
       </View>
    )
  }

  onClick=(index)=>{
    const temp = this.state.data.slice()
    temp[index].value = !temp[index].value
    this.setState({data: temp})
  }

  toggleExpand=()=>{
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded : !this.state.expanded})
  }

}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 52,
    }
});