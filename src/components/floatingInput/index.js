import React, { Component } from 'react';
import {
    View,
    TextInput,
    Animated,
} from 'react-native';

import { theme, variables, hexToRGB } from '~/theme';
import { TextInputMask } from 'react-native-masked-text'


export default class FloatingInput extends Component {
    state = {
        isFocused: false,
    };

    handleFocus = () => this.setState({ isFocused: true });
    handleBlur = () => this.setState({ isFocused: false });


    render() {
        const { label, icon, isMask, maskType, ...props } = this.props;
        const labelStyle = {
            position: 'absolute',
            left: 0,
            top: (this.state.isFocused || this.props.value !== '') ? -12 : 14,
            color: (this.state.isFocused) ? variables.primary : variables.gray3,
            fontSize: (this.state.isFocused || this.props.value !== '') ? 12 : 14,
            marginHorizontal: variables.paddingHorizontal,
            paddingHorizontal: 8,
            backgroundColor: variables.light,
        };

        const textfield = {
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: variables.paddingHorizontal,
            borderRadius: variables.borderRadius,
            backgroundColor: variables.light,
            borderWidth: 1,
            borderColor: (this.state.isFocused) ? variables.primary : '#C0C4D0',
            marginBottom: variables.paddingHorizontal,
        };

        const textfield_input = {
            height: 48,
            width: '100%',
            //flexGrow: 1,
            color: variables.secondary,
            fontSize: variables.fontSize,
        };
        return (
            <View style={textfield}>
                <Animated.Text style={labelStyle}>
                    {label}
                </Animated.Text>
                {isMask
                    ? <TextInputMask
                        {...props}
                        style={textfield_input}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        type={maskType}
                        blurOnSubmit
                    />
                    : <TextInput
                        {...props}
                        style={textfield_input}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        blurOnSubmit
                    />
                }
                {icon}
            </View>
        );
    }
}
