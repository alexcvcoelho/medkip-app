//import { DrawerContentScrollView } from '@react-navigation/drawer';
import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import IconArrowRight from "~/assets/images/arrow-blue.svg";
import IconClose from "~/assets/images/close.svg";

import IconProfile from "~/assets/images/profile-active.svg";
import IconConfig from "~/assets/images/icon-config.svg";
import IconHelp from "~/assets/images/icon-help.svg";
import IconPrivacy from "~/assets/images/icon-privacy.svg";
import IconFiles from "~/assets/images/icon-files.svg";
import IconLogout from "~/assets/images/icon-logout.svg";

import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';


//import { HomeStackNavigator, CasesStackNavigator, FinancialStackNavigator, ShiftsStackNavigator, ProfileStackNavigator } from "~/pages/stack-navigation";
//import { TabComponent } from '~/pages/tab'

//import { AuthContext } from '~/components/context';


export default function Sidebar({ ...props }) {

    //const { signOut } = React.useContext(AuthContext);

    return (
        <View style={styles.container}>
            <DrawerContentScrollView>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
                        <IconClose width="39" height="37"></IconClose>
                    </TouchableOpacity>
                    <Text style={styles.text}>Menu</Text>
                </View>

                <TouchableOpacity style={[styles.line_content, styles.margin_first]}
                    onPress={props.navigation.navigate('Profile')}>
                    <IconProfile width="24" height="24"></IconProfile>
                    <Text style={styles.text}>Conta</Text>
                    <View style={styles.sub_item_arrow}>
                        <IconArrowRight width="5" height="9"></IconArrowRight>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.line_content, styles.margin_sub_items]}>
                    <IconConfig width="24" height="24"></IconConfig>
                    <Text style={styles.text}>Configuração da conta</Text>
                    <View style={styles.sub_item_arrow}>
                        <IconArrowRight width="5" height="9"></IconArrowRight>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.line_content, styles.margin_sub_items]}>
                    <IconHelp width="24" height="24"></IconHelp>
                    <Text style={styles.text}>Ajuda</Text>
                    <View style={styles.sub_item_arrow}>
                        <IconArrowRight width="5" height="9"></IconArrowRight>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.line_content, styles.margin_sub_items]}>
                    <IconPrivacy width="24" height="24"></IconPrivacy>
                    <Text style={styles.text}>Política de privacidade</Text>
                    <View style={styles.sub_item_arrow}>
                        <IconArrowRight width="5" height="9"></IconArrowRight>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.line_content, styles.margin_sub_items]} >
                    <IconFiles width="24" height="24"></IconFiles>
                    <Text style={styles.text}>Arquivos</Text>
                    <View style={styles.sub_item_arrow}>
                        <IconArrowRight width="5" height="9"></IconArrowRight>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.line_content, styles.margin_sub_items]}>
                    <IconLogout width="24" height="24"></IconLogout>
                    <Text style={styles.text}>Logout</Text>
                    <View style={styles.sub_item_arrow}>
                        <IconArrowRight width="5" height="9"></IconArrowRight>
                    </View>
                </TouchableOpacity>
            </DrawerContentScrollView>
            <Text style={styles.text_bottom}>Medkip app ver 0.1</Text>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    margin_first: {
        marginTop: 50
    },
    margin_sub_items: {
        marginTop: 14
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 60,
        marginLeft: 20
    },
    text: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 14,
        color: '#202945',
        marginLeft: 10
    },
    line_content: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 16,
        marginRight: 16,
        height: 56,
    },
    sub_item_arrow: {
        flex: 1,
        alignItems: 'flex-end'
    },
    line: {

    },
    text_bottom: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 33,
        marginLeft: 17,
        color: '#202945',
        opacity: 0.5
    }
});

