import React, { useState } from 'react'
import { View, Modal, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import { Icon, Button, Text } from 'react-native-elements';

import { theme, variables, hexToRGB } from '~/theme';

import IconTrash from "~/assets/images/icon-trash.svg";
import IconCheckSuccess from "~/assets/images/icon-check-success.svg";
import IconCheckFailure from "~/assets/images/info.svg";

const SimpleBottomSheet = ({ visible, onClose = null, onCloseOutside = null, children }) => {

    const [maxHeight, setMaxHeight] = useState(Dimensions.get('window').height - 80);

    Dimensions.addEventListener('change', () => {
        setMaxHeight(Dimensions.get('window').height - 80);
    });

    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}
            // onRequestClose={() => onCloseOutside()}
            >
                <View style={theme.bsOverlay}>
                    <View style={[theme.bsView, { position: 'relative', zIndex: 100, maxHeight: maxHeight }]}>
                        {onClose && <View style={{ alignItems: 'flex-end' }}>
                            <Icon name="close" color={variables.accent} onPress={() => onClose()} />
                        </View>
                        }
                        <View>
                            <ScrollView>
                                {children}
                            </ScrollView>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => onCloseOutside ? onCloseOutside() : onClose()} style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }} />
                </View>
            </Modal>
        </>
    )
}

const SimpleModal = ({ visible, onClose = null, children, title = '' }) => {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
        >
            <View style={theme.modalOverlay}>
                <View style={theme.modalView}>
                    {onClose && <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                        {(title !== '') && <Text style={[theme.title, { flexGrow: 1, paddingHorizontal: 35 - variables.paddingHorizontal }]}>{title}</Text>}
                        <Icon name="close" color={variables.accent} size={20} onPress={() => onClose()} />
                    </View>}
                    <View style={{ paddingHorizontal: 35 - variables.paddingHorizontal }}>
                        {children}
                    </View>
                </View>
            </View>
        </Modal>
    )
}
const BottomSheetConfirm = ({ visible, onClose = null, onConfirm = null, text = '' }) => {
    const textStyles = { fontSize: 16, color: variables.gray2, marginVertical: variables.paddingHorizontal, textAlign: 'center' }
    return (
        <SimpleBottomSheet visible={visible} onClose={() => onClose()} onCloseOutside={() => onClose()}>
            <View style={{ alignItems: 'center', marginHorizontal: variables.paddingHorizontal }}>
                <IconTrash />
                <Text style={textStyles}>{text}</Text>
            </View>
            <Button
                title="Sim, excluir"
                disabled={false}
                buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                disabledTitleStyle={{ color: variables.light }}
                onPress={() => { onClose(); onConfirm() }}
            />
            <View style={{ height: variables.paddingHorizontal }} />
            <Button
                title="Não, voltar"
                disabled={false}
                buttonStyle={{ backgroundColor: variables.light, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight, borderWidth: 1 }}
                titleStyle={{ color: variables.primary, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                disabledTitleStyle={{ color: variables.light }}
                onPress={() => onClose()}
            />
        </SimpleBottomSheet>
    )
}
const BottomSheetSuccess = ({ visible, onClose = null, text = '' }) => {
    const textStyles = { fontSize: 16, color: variables.gray2, marginVertical: variables.paddingHorizontal, textAlign: 'center' }
    return (
        <SimpleBottomSheet visible={visible} onClose={() => onClose}>
            <View style={{ alignItems: 'center', marginHorizontal: variables.paddingHorizontal }}>
                <IconCheckSuccess />
                <Text style={textStyles}>{text}</Text>
            </View>
            <Button
                title="Ok"
                disabled={false}
                buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                disabledTitleStyle={{ color: variables.light }}
                onPress={() => onClose()}
            />
        </SimpleBottomSheet>
    )
}

const BottomSheetFailure = ({ visible, onClose = null, text = '' }) => {
    const textStyles = { fontSize: 16, color: variables.gray2, marginVertical: variables.paddingHorizontal, textAlign: 'center' }
    return (
        <SimpleBottomSheet visible={visible} onClose={() => onClose()}>
            <View style={{ alignItems: 'center', marginHorizontal: variables.paddingHorizontal }}>
                <IconCheckFailure />
                <Text style={textStyles}>{text}</Text>
            </View>
            <Button
                title="Ok"
                disabled={false}
                buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                disabledTitleStyle={{ color: variables.light }}
                onPress={() => onClose()}
            />
        </SimpleBottomSheet>
    )
}
export {
    SimpleBottomSheet,
    SimpleModal,
    BottomSheetConfirm,
    BottomSheetSuccess,
    BottomSheetFailure
}
