import React, { useRef, useState } from 'react'
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, TextInput, Modal } from 'react-native';
import { Card, ListItem, Button, Icon } from 'react-native-elements';


import CalendarPicker from 'react-native-calendar-picker';

import { theme, variables, hexToRGB } from '~/theme';

import IconCalendar from "~/assets/images/icon-calendar.svg";
import IconHospital from "~/assets/images/icon-hospital.svg";
import moment from 'moment';

const filter = ({ visible, onClose, onOpen, onFilter, hospitals }) => {
    const [selected, setSelected] = useState(null);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [bsFilterDate, setBsFilterDate] = useState(false);
    const [bsFilterHospital, setBsFilterHospital] = useState(false);

    const [isFocused, setIsFocused] = useState(false);

    const [query, setQuery] = useState('');

    const [filterHospitals, setFilterHospitals] = useState([]);

    const onChangeQuery = (text) => {
        setQuery(text)

        const result = hospitals.filter(x => String(x.name).toLowerCase().includes(String(text).toLowerCase()))
        setFilterHospitals(result)
    }

    const [filters, setFilters] = useState({
        periodo: '',
        hospital: ''
    })

    const inputH = useRef(null);

    const changeValue = (key, value) => {
        const data = { ...filters };
        data[key] = value;
        //console.log(data)
        setFilters(data);
    };

    const onDateChange = (date, type) => {
        if (type === 'END_DATE') {
            setEndDate(date)
        } else if (type === 'START_DATE') {
            setStartDate(date)
        }
        console.log(date, type)
    };

    const filterDate = () => {
        setBsFilterDate(false);
        onOpen();
        changeValue('periodo', `${moment(startDate).format("DD/MM/YY")} até ${moment(endDate).format("DD/MM/YY")}`);
        // changeValue('startDate', moment(startDate).format("DD/MM/YY"))
        // changeValue('endDate', moment(endDate).format("DD/MM/YY"))
    }

    const customDayHeaderStylesCallback = ({ dayOfWeek, month, year }) => {
        return {
            textStyle: {
                fontFamily: variables.fontBold,
            }
        };
    }

    //const hospitals = ['Samaritano, São Paulo', 'Israelita Albert Einstein, São Paulo', 'Samaritano, São Paulo', 'Universitário, São Paulo']

    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}
            >
                <View style={theme.bsOverlay}>
                    <View style={theme.bsView}>
                        <View>
                            <View style={styles.header}>
                                <Text style={styles.title}>Filtrar por</Text>
                                <TouchableOpacity onPress={() => onClose()}>
                                    <Icon name="close" color={variables.primary} />
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity style={styles.textfield} onPress={() => {
                                onClose(); setBsFilterDate(true); changeValue('periodo', '')
                            }}>
                                <TextInput
                                    style={styles.textfield_input}
                                    placeholder="Filtrar por período"
                                    placeholderTextColor={variables.gray3}
                                    editable={false}
                                    value={filters.periodo}
                                />
                                <IconCalendar width="20" height="20" style={styles.textfield_icon} />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.textfield} onPress={() => { onClose(); setBsFilterHospital(true); changeValue('hospital', ''); }}>
                                <TextInput
                                    style={styles.textfield_input}
                                    placeholder="Filtrar por hospital"
                                    placeholderTextColor={variables.gray3}
                                    editable={false}
                                    value={filters.hospital}
                                />
                                <IconHospital width="24" height="20" style={styles.textfield_icon} />
                            </TouchableOpacity>
                            <Button
                                title="Aplicar"
                                disabled={!filters.hospital && !filters.periodo}
                                buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                                titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                disabledTitleStyle={{ color: variables.light }}
                                onPress={() => onFilter(filters)}
                            />
                        </View>
                    </View>
                </View>
            </Modal>

            <Modal
                animationType="slide"
                transparent={true}
                visible={bsFilterDate}
            >
                <View style={theme.bsOverlay}>
                    <View style={theme.bsView}>
                        <View>
                            <View style={styles.header}>
                                <Text style={styles.title}>Selecione um período</Text>
                                <TouchableOpacity onPress={() => { setBsFilterDate(false); onOpen() }}>
                                    <Icon name="close" color={variables.primary} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginVertical: 8 }}>
                                <CalendarPicker
                                    onDateChange={onDateChange}
                                    allowRangeSelection={true}
                                    weekdays={['S', 'T', 'Q', 'Q', 'S', 'S', 'D']}
                                    months={['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']}
                                    headerWrapperStyle={{ backgroundColor: '#f7f7f9', width: 'auto', alignSelf: 'stretch', borderRadius: variables.borderRadius, padding: variables.paddingHorizontal, paddingBottom: variables.paddingHorizontal }}
                                    previousComponent={<Icon name="chevron-left" color={variables.primary} />}
                                    nextComponent={<Icon name="chevron-right" color={variables.primary} />}
                                    todayBackgroundColor={'transparent'}
                                    selectedDayColor={'#E8F2FF'}
                                    selectedDayTextColor={variables.secondary}
                                    selectedRangeStartStyle={styles.selectedRangeStartStyle}
                                    selectedRangeEndStyle={[styles.selectedRangeStartStyle, styles.selectedRangeEndStyle]}
                                    selectedRangeStartTextStyle={{ color: variables.light }}
                                    selectedRangeEndTextStyle={{ color: variables.light }}
                                    dayLabelsWrapper={{ borderTopWidth: 0, borderBottomWidth: 0 }}
                                    customDayHeaderStyles={customDayHeaderStylesCallback}
                                    textStyle={{
                                        fontFamily: variables.fontFamily,
                                        color: variables.secondary,
                                    }}
                                    monthTitleStyle={{ fontFamily: variables.fontBold }}
                                    yearTitleStyle={{ fontFamily: variables.fontBold }} />
                            </View>
                            <Button
                                title="Selecionar"
                                disabled={startDate != null && endDate != null ? false : true}
                                buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                                titleStyle={{ color: variables.light, fontSize: variables.fontSize, fontFamily: variables.fontBold }}
                                disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                disabledTitleStyle={{ color: variables.light }}
                                onPress={() => filterDate()}
                            />
                        </View>
                    </View>
                </View>
            </Modal>
            <Modal
                animationType="slide"
                transparent={true}
                visible={bsFilterHospital}
                onShow={() => {
                    inputH.current.blur()
                    inputH.current.focus()
                }}
            >
                <View style={theme.bsOverlay}>
                    <View style={theme.bsView}>
                        <View>
                            <View style={styles.header}>
                                <Text style={styles.title}>Selecione um hospital</Text>
                                <TouchableOpacity onPress={() => { setBsFilterHospital(false); onOpen() }}>
                                    <Icon name="close" color={variables.primary} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <View style={[styles.textfield, { marginBottom: 0, borderColor: variables.primary }]}>
                                    <TextInput
                                        style={styles.textfield_input}
                                        placeholder="Filtrar por hospital"
                                        placeholderTextColor={variables.gray3}
                                        ref={inputH}
                                        onChangeText={(text) => onChangeQuery(text)}
                                        onFocus={() => setIsFocused(true)}
                                        onBlur={() => setIsFocused(false)}
                                    />
                                    <IconHospital width="24" height="20" style={styles.textfield_icon} />
                                </View>
                                <View style={{ backgroundColor: '#f7f7f9', borderRadius: 4, marginBottom: variables.paddingHorizontal }}>
                                    {query.length > 1 && isFocused &&
                                        filterHospitals.map((item, i) => (
                                            <Text key={item.id} onPress={() => setSelected(i)} style={[styles.item, selected === i ? styles.item_selected : {}]}>{item.name}</Text>
                                        ))
                                    }
                                </View>
                            </View>
                            <Button
                                title="Selecionar"
                                buttonStyle={{ backgroundColor: variables.primary, borderColor: variables.primary, borderRadius: variables.borderRadius, height: variables.inputHeight }}
                                titleStyle={{ color: variables.light, fontFamily: variables.fontBold, fontSize: variables.fontSize }}
                                disabledStyle={{ backgroundColor: hexToRGB(variables.primary, 0.3), borderColor: variables.primary }}
                                disabledTitleStyle={{ color: variables.light }}
                                onPress={() => { setBsFilterHospital(false); onOpen(); selected != null ? changeValue('hospital', hospitals[selected].name) : null }}
                            />
                        </View>
                    </View>
                </View>
            </Modal>
        </>
    )
}
const styles = StyleSheet.create({
    page: {
        padding: variables.paddingHorizontal,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 12
    },
    title: {
        color: variables.secondary,
        fontFamily: variables.fontBold,
        fontSize: 14,
        margin: 0,
    },
    textfield: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: variables.paddingHorizontal,
        borderRadius: variables.borderRadius,
        backgroundColor: variables.light,
        borderWidth: 1,
        borderColor: '#C0C4D0',
        marginBottom: variables.paddingHorizontal,
    },
    textfield_input: {
        height: 48,
        flexGrow: 1,
        color: variables.secondary,
        fontSize: variables.fontSize,
    },
    textfield_icon: {
        flexShrink: 0,
        width: 24,
        height: 20
    },
    item: {
        paddingHorizontal: variables.paddingHorizontal * 2,
        paddingVertical: variables.paddingVertical,
        color: variables.secondary,
    },
    item_selected: {
        backgroundColor: '#e5edfa',
        fontWeight: "bold",
    },
    selectedRangeStartStyle: {
        backgroundColor: variables.primary,
        borderTopLeftRadius: 2,
        borderBottomLeftRadius: 2,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 4,

        elevation: 3,
    },
    selectedRangeEndStyle: {
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
    }

})

export default filter
