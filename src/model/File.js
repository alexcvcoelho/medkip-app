import JSModel from 'react-native-jsmodel';

export class ResponseProfile extends JSModel {
    constructor(json) {
        super(json);
        this.id = "";
        this.name = "";
        this.type = "";
        this.size = 0;
        this.url = "";
        this.uploaded = false;
    }
}