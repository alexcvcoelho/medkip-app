import JSModel from 'react-native-jsmodel';

export class ResponseProfile extends JSModel {
    constructor(json) {
        super(json);

        if (this.validate(json)) {
            this.id = json.id;
            this.profile = new Profile(json.profile);
        } else {
            console.log('Erro serializar model ResponseProfile')
        }
    }
}

export class Profile extends JSModel {
    constructor(json) {
        super(json);
        if (this.validate(json)) {
            this.bio = json.bio;
            this.city = json.city;
            this.college = json.college;
            this.crm = json.crm;
            this.expertise = json.expertise;
            this.name = json.name;
            this.state = json.state;
            this.avatar = json.avatar;
        } else {
            console.log('Erro serializar model Profile')
        }
    }
}
/* Response Model
            {
                "id": "117463040776664226989",
                "profile": {
                    "bio": "Pequeno grande homem",
                    "city": "Sorocaba",
                    "college": "Engenheiro de Computação",
                    "crm": "1234",
                    "expertise": "Analista de Sistemas",
                    "name": "Juan",
                    "state": "SP",
                    "avatar": null
                }
            }
*/